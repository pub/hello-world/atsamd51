#define RED_LED 25
#define GREEN_LED 9

void setup() {
    pinMode(RED_LED, OUTPUT);
    pinMode(GREEN_LED, OUTPUT);

    // This turns off the LEDs.
    digitalWrite(RED_LED, HIGH);
    digitalWrite(GREEN_LED, HIGH);

    Serial.begin(9600);
}

void loop() {
    digitalWrite(RED_LED, LOW);
    digitalWrite(GREEN_LED, LOW);
    Serial.println("hello world");
    delay(500);
    digitalWrite(RED_LED, HIGH);
    digitalWrite(GREEN_LED, HIGH);
    delay(500);
}
