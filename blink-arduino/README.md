# Programming with the Arduino IDE

This doc assumes you've already burned an Arduino compatible bootloader on your board, and set up
the Arduino IDE to talk to it. If you still need to do this, check out the README one level up.

After that, just select the right settings in the Arduino IDE tools menu and you're good to go:

- Board: Adafruit Feather M4 Express (SAMD51)
- Port: depends on your OS, but it should say Adafruit Feather M4 Express (SAMD51) somewhere
- Programmer: USBtinyISP

The other settings are up to you. Defaults are fine.

![Arduino IDE](img/arduino_ide.jpg)

Note that this blinky sketch is fancy, and prints "hello world" over USB serial. To see it, you can
use the Arduino IDE's serial monitor. Getting this to work would be a lot more difficult without the
Arduino libraries.
