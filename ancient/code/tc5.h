
void startup_step_operator(void){
  // some pin fiddling,
  GPIO_PORT.DIRSET.reg = DIR_BM;
  GPIO_PORT.OUTCLR.reg = DIR_BM;
  GPIO_PORT.DIRSET.reg = STEP_BM;
  /*
  GPIO_PORT.PINCFG[STEP_PIN].bit.PMUXEN = 1;
  if(STEP_PIN % 2){ // true when odd,
    GPIO_PORT.PMUX[STEP_PIN >> 1].reg |= PORT_PMUX_PMUXO(STEP_TIMER_PERIPHERAL);
  } else {
    GPIO_PORT.PMUX[STEP_PIN >> 1].reg |= PORT_PMUX_PMUXE(STEP_TIMER_PERIPHERAL);
  }
  */
  // our pin is on PB00 / TC7-WO[0]
  // which is something of a welp, it should be on a timer pin that is defined...
  // live and learn, we'll use the interrupt to step, kind of a bummer, but here we are
  // a handful of these can only be written to when the thing is turnt off
  TC5->COUNT16.CTRLA.bit.SWRST = 1;
  //TC5->COUNT16.CTRLA.bit.ENABLE = 0;
  // unmask clocks
  MCLK->APBCMASK.reg |= MCLK_APBCMASK_TC5;
  // make one clk,
  GCLK->GENCTRL[TIMER_B_GCLK_NUM].reg = GCLK_GENCTRL_SRC(GCLK_GENCTRL_SRC_DFLL) | GCLK_GENCTRL_GENEN;
  while(GCLK->SYNCBUSY.reg & GCLK_SYNCBUSY_GENCTRL(TIMER_B_GCLK_NUM));
  // ok, clock to these channels...
  GCLK->PCHCTRL[TC5_GCLK_ID].reg = GCLK_PCHCTRL_CHEN | GCLK_PCHCTRL_GEN(TIMER_B_GCLK_NUM);
  // turn them ooon...
  TC5->COUNT16.CTRLA.reg = TC_CTRLA_MODE_COUNT16 | TC_CTRLA_PRESCSYNC_PRESC | TC_CTRLA_PRESCALER_DIV16 | TC_CTRLA_CAPTEN0;
  // going to set this up to count at some time, we will tune
  TC5->COUNT16.WAVE.reg = TC_WAVE_WAVEGEN_MFRQ;
  // allow interrupt to trigger on this event (overflow)
	TC5->COUNT16.INTENSET.bit.MC0 = 1;
  TC5->COUNT16.INTENSET.bit.MC1 = 1;
  // set the period, should show a lower bound of ~ 200 counts (that's 400 cpu counts, for interrupt of... 800ns)
  while(TC5->COUNT16.SYNCBUSY.bit.CC0);
	TC5->COUNT16.CC[0].reg = STEP_TICKER_B_FULL_COUNT;
	// enable, sync for enable write
  while(TC5->COUNT16.SYNCBUSY.bit.ENABLE);
	TC5->COUNT16.CTRLA.bit.ENABLE = 1;
	// enable the IRQ
	NVIC_EnableIRQ(TC5_IRQn);
  // do step pin stuff ?
}
