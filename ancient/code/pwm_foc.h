/*
 * pwm_foc.h
 *
 * Created: 2/17/2018 10:22:37 PM
 *  Author: Jake
 */ 


#ifndef PWM_FOC_H_
#define PWM_FOC_H_

#include "sam.h"

void pwmsetup_foc(void);

// 0 -> 255 (but count deadtime?) midpoint is 126
void pwmupdate_foc(uint32_t one, uint32_t two, uint32_t three); 

// asserts min and max for pwm
void pwm_bounds(uint32_t *val);

#endif /* PWM_FOC_H_ */