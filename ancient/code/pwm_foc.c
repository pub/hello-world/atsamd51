/*
 * pwm.c
 *
 * Created: 2/17/2018 10:22:16 PM
 *  Author: Jake
 */ 
#include "pwm_foc.h"
#include "hardware.h"

void pwmsetup_foc(void){
	/* TCC SETUP */
	// from 49.6.2.1
	// a few registers are protected - and can only be updated when
	// TCCn.CTRLA.ENABLE = 0
	// FCTRLA and FCTRLB, WEXCTRL, DRVCTRL, and EVCTRL
	
	// (4) Configure Output Pin with PORT->Group[n].DIRSET.reg
	// PA8 PA9 PA10 PA12, PB10 PB11
	// 32.9.13
	PORT->Group[0].DIRSET.reg |= (uint32_t)(1 << 8) | (uint32_t)(1 << 9) | (uint32_t)(1 << 10) | (uint32_t)(1 << 12);
	PORT->Group[1].DIRSET.reg |= (uint32_t)(1 << 10) | (uint32_t)(1 << 11);
	
	// 1 lo / hi
	PORT->Group[0].PINCFG[10].bit.PMUXEN = 1;
	PORT->Group[0].PMUX[10>>1].reg |= PORT_PMUX_PMUXE(0x5);  // on peripheral F
	PORT->Group[0].PINCFG[12].bit.PMUXEN = 1;
	PORT->Group[0].PMUX[12>>1].reg |= PORT_PMUX_PMUXE(0x5);
	
	// 2 lo / hi
	PORT->Group[0].PINCFG[9].bit.PMUXEN = 1;
	PORT->Group[0].PMUX[9>>1].reg |= PORT_PMUX_PMUXO(0x5);  // on peripheral F
	PORT->Group[1].PINCFG[11].bit.PMUXEN = 1;
	PORT->Group[1].PMUX[11>>1].reg |= PORT_PMUX_PMUXO(0x5);
	
	// 3 lo / hi
	PORT->Group[0].PINCFG[8].bit.PMUXEN = 1;
	PORT->Group[0].PMUX[8>>1].reg |= PORT_PMUX_PMUXE(0x5);  // on peripheral F
	PORT->Group[1].PINCFG[10].bit.PMUXEN = 1;
	PORT->Group[1].PMUX[10>>1].reg |= PORT_PMUX_PMUXE(0x5);
	
	// (1) enable the TCC Bus Clock - CLK_TCCn_APB
	// https://www.eevblog.com/forum/microcontrollers/atmel-sam-d-tc-and-tcc-(no-asf)/
	
	TCC0->CTRLA.bit.ENABLE = 0;
	
	MCLK->APBBMASK.reg |= MCLK_APBBMASK_TCC0; // at 15.8.9
	
	GCLK->GENCTRL[5].reg = GCLK_GENCTRL_SRC(GCLK_GENCTRL_SRC_DFLL) | GCLK_GENCTRL_GENEN;
	while(GCLK->SYNCBUSY.reg & GCLK_SYNCBUSY_GENCTRL5);
	
	GCLK->PCHCTRL[TCC0_GCLK_ID].reg = GCLK_PCHCTRL_CHEN | GCLK_PCHCTRL_GEN_GCLK5;
	
	TCC0->CTRLA.reg |= TCC_CTRLA_PRESCALER_DIV4 | TCC_CTRLA_PRESCSYNC_PRESC |TCC_CTRLA_RESOLUTION(0);
	
	// (2) Select Waveform Generation operation in the WAVE register WAVE.WAVEGEN
	// we want dual slope pwm
	
	TCC0->WAVE.reg = TCC_WAVE_WAVEGEN_DSBOTH; // 'dual slope both' - updates on both hi and lo of slope ?
	
	// (3) We want OTMX - Output Matrix Channel Pin Routing Configuration - at 0x0
	
	TCC0->WEXCTRL.reg = TCC_WEXCTRL_DTHS(1) | TCC_WEXCTRL_DTLS(1) |
	TCC_WEXCTRL_DTIEN1 | TCC_WEXCTRL_DTIEN2 | TCC_WEXCTRL_DTIEN3 | TCC_WEXCTRL_DTIEN0 |
	TCC_WEXCTRL_OTMX(0);
	
	TCC0->PER.reg = TCC_PER_PER(256); 
	
	TCC0->COUNT.reg = 0;
	
	TCC0->CCBUF[0].reg = 0; // '3'
	TCC0->CCBUF[1].reg = 0; // '2'
	TCC0->CCBUF[2].reg = 0; // '1'
	TCC0->CCBUF[3].reg = 0;
	
	// (4) Enable with CTRLA.ENABLE
	
	TCC0->CTRLA.bit.ENABLE = 1;
	while(TCC0->SYNCBUSY.bit.ENABLE);
}

#define PWMFOC_MIN_PWM 3
#define PWMFOC_MAX_PWM 253

void pwmupdate_foc(uint32_t one, uint32_t two, uint32_t three){
	pwm_bounds(&three);
	pwm_bounds(&two);
	pwm_bounds(&one);
	/*
	uart_sendchar_buffered(&up1, one);
	uart_sendchar_buffered(&up1, two);
	uart_sendchar_buffered(&up1, three);
	*/
	TCC0->CCBUF[0].reg = three; // '3'
	TCC0->CCBUF[1].reg = two; // '2'
	TCC0->CCBUF[2].reg = one; // '1'
}

void pwm_bounds(uint32_t *val){
	if(*val > PWMFOC_MAX_PWM){
		*val = PWMFOC_MAX_PWM;
	} else if(*val < PWMFOC_MIN_PWM){
		*val = PWMFOC_MIN_PWM;
	}
}