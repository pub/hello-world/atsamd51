<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.0.0">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="no" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="no" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="no" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="no" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="no" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="no" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="no" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="microcontrollers">
<packages>
<package name="QFN-64-9X9MM">
<description>&lt;h3&gt;64-pin QFN 9x9mm, 0.5mm pitch&lt;/h3&gt;
&lt;p&gt;Package used by ATmega128RFA1&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.atmel.com/Images/Atmel-8266-MCU_Wireless-ATmega128RFA1_Datasheet.pdf"&gt;Example Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="-4.492" y1="-4.5" x2="4.508" y2="-4.5" width="0.09" layer="51"/>
<wire x1="4.508" y1="-4.5" x2="4.508" y2="4.5" width="0.09" layer="51"/>
<wire x1="4.508" y1="4.5" x2="-4.492" y2="4.5" width="0.09" layer="51"/>
<wire x1="-4.492" y1="4.5" x2="-4.492" y2="-4.5" width="0.09" layer="51"/>
<wire x1="-4.6" y1="4.6" x2="-4.6" y2="4.1" width="0.2032" layer="21"/>
<wire x1="-4.6" y1="4.6" x2="-4.1" y2="4.6" width="0.2032" layer="21"/>
<wire x1="4.6" y1="4.6" x2="4.1" y2="4.6" width="0.2032" layer="21"/>
<wire x1="4.6" y1="4.6" x2="4.6" y2="4.1" width="0.2032" layer="21"/>
<circle x="-4.842" y="4.85" radius="0.2" width="0" layer="21"/>
<circle x="-3.442" y="3.45" radius="0.2" width="0.09" layer="51"/>
<smd name="26" x="0.75" y="-4.5" dx="0.25" dy="0.6" layer="1" rot="R180"/>
<smd name="25" x="0.25" y="-4.5" dx="0.25" dy="0.6" layer="1" rot="R180"/>
<smd name="24" x="-0.25" y="-4.5" dx="0.25" dy="0.6" layer="1" rot="R180"/>
<smd name="27" x="1.25" y="-4.5" dx="0.25" dy="0.6" layer="1" rot="R180"/>
<smd name="28" x="1.75" y="-4.5" dx="0.25" dy="0.6" layer="1" rot="R180"/>
<smd name="23" x="-0.75" y="-4.5" dx="0.25" dy="0.6" layer="1" rot="R180"/>
<smd name="22" x="-1.25" y="-4.5" dx="0.25" dy="0.6" layer="1" rot="R180"/>
<smd name="21" x="-1.75" y="-4.5" dx="0.25" dy="0.6" layer="1" rot="R180"/>
<smd name="6" x="-4.5" y="1.25" dx="0.25" dy="0.6" layer="1" rot="R270"/>
<smd name="5" x="-4.5" y="1.75" dx="0.25" dy="0.6" layer="1" rot="R270"/>
<smd name="4" x="-4.5" y="2.25" dx="0.25" dy="0.6" layer="1" rot="R270"/>
<smd name="7" x="-4.5" y="0.75" dx="0.25" dy="0.6" layer="1" rot="R270"/>
<smd name="8" x="-4.5" y="0.25" dx="0.25" dy="0.6" layer="1" rot="R270"/>
<smd name="3" x="-4.5" y="2.75" dx="0.25" dy="0.6" layer="1" rot="R270"/>
<smd name="2" x="-4.5" y="3.25" dx="0.25" dy="0.6" layer="1" rot="R270"/>
<smd name="9" x="-4.5" y="-0.25" dx="0.25" dy="0.6" layer="1" rot="R270"/>
<smd name="10" x="-4.5" y="-0.75" dx="0.25" dy="0.6" layer="1" rot="R270"/>
<smd name="1" x="-4.5" y="3.75" dx="0.25" dy="0.6" layer="1" rot="R270"/>
<smd name="16" x="-4.5" y="-3.75" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="15" x="-4.5" y="-3.25" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="14" x="-4.5" y="-2.75" dx="0.25" dy="0.6" layer="1" rot="R270"/>
<smd name="17" x="-3.75" y="-4.5" dx="0.25" dy="0.6" layer="1" rot="R180"/>
<smd name="18" x="-3.25" y="-4.5" dx="0.25" dy="0.6" layer="1" rot="R180"/>
<smd name="13" x="-4.5" y="-2.25" dx="0.25" dy="0.6" layer="1" rot="R270"/>
<smd name="12" x="-4.5" y="-1.75" dx="0.25" dy="0.6" layer="1" rot="R270"/>
<smd name="19" x="-2.75" y="-4.5" dx="0.25" dy="0.6" layer="1" rot="R180"/>
<smd name="20" x="-2.25" y="-4.5" dx="0.25" dy="0.6" layer="1" rot="R180"/>
<smd name="11" x="-4.5" y="-1.25" dx="0.25" dy="0.6" layer="1" rot="R270"/>
<smd name="29" x="2.25" y="-4.5" dx="0.25" dy="0.6" layer="1"/>
<smd name="30" x="2.75" y="-4.5" dx="0.25" dy="0.6" layer="1"/>
<smd name="31" x="3.25" y="-4.5" dx="0.25" dy="0.6" layer="1"/>
<smd name="32" x="3.75" y="-4.5" dx="0.25" dy="0.6" layer="1"/>
<smd name="33" x="4.5" y="-3.75" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="34" x="4.5" y="-3.25" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="35" x="4.5" y="-2.75" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="36" x="4.5" y="-2.25" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="37" x="4.5" y="-1.75" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="38" x="4.5" y="-1.25" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="39" x="4.5" y="-0.75" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="40" x="4.5" y="-0.25" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="41" x="4.5" y="0.25" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="42" x="4.5" y="0.75" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="43" x="4.5" y="1.25" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="44" x="4.5" y="1.75" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="45" x="4.5" y="2.25" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="46" x="4.5" y="2.75" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="47" x="4.5" y="3.25" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="48" x="4.5" y="3.75" dx="0.25" dy="0.6" layer="1" rot="R90"/>
<smd name="49" x="3.75" y="4.5" dx="0.25" dy="0.6" layer="1"/>
<smd name="50" x="3.25" y="4.5" dx="0.25" dy="0.6" layer="1"/>
<smd name="51" x="2.75" y="4.5" dx="0.25" dy="0.6" layer="1"/>
<smd name="52" x="2.25" y="4.5" dx="0.25" dy="0.6" layer="1"/>
<smd name="53" x="1.75" y="4.5" dx="0.25" dy="0.6" layer="1"/>
<smd name="54" x="1.25" y="4.5" dx="0.25" dy="0.6" layer="1"/>
<smd name="55" x="0.75" y="4.5" dx="0.25" dy="0.6" layer="1"/>
<smd name="56" x="0.25" y="4.5" dx="0.25" dy="0.6" layer="1"/>
<smd name="57" x="-0.25" y="4.5" dx="0.25" dy="0.6" layer="1"/>
<smd name="58" x="-0.75" y="4.5" dx="0.25" dy="0.6" layer="1"/>
<smd name="59" x="-1.25" y="4.5" dx="0.25" dy="0.6" layer="1"/>
<smd name="60" x="-1.75" y="4.5" dx="0.25" dy="0.6" layer="1"/>
<smd name="61" x="-2.25" y="4.5" dx="0.25" dy="0.6" layer="1"/>
<smd name="62" x="-2.75" y="4.5" dx="0.25" dy="0.6" layer="1"/>
<smd name="63" x="-3.25" y="4.5" dx="0.25" dy="0.6" layer="1"/>
<smd name="64" x="-3.75" y="4.5" dx="0.25" dy="0.6" layer="1"/>
<text x="0" y="1.27" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.27" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="4.6" y1="-4.6" x2="4.1" y2="-4.6" width="0.2032" layer="21"/>
<wire x1="4.6" y1="-4.6" x2="4.6" y2="-4.1" width="0.2032" layer="21"/>
<wire x1="-4.6" y1="-4.6" x2="-4.6" y2="-4.1" width="0.2032" layer="21"/>
<wire x1="-4.6" y1="-4.6" x2="-4.1" y2="-4.6" width="0.2032" layer="21"/>
<smd name="P$1" x="0" y="0" dx="4.826" dy="4.826" layer="1" cream="no"/>
<polygon width="0.127" layer="31">
<vertex x="-1.27" y="-1.27"/>
<vertex x="-1.27" y="1.27"/>
<vertex x="1.27" y="1.27"/>
<vertex x="1.27" y="-1.27"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="ATSAMD51">
<pin name="GND@5" x="-35.56" y="-48.26" length="middle"/>
<pin name="GND@4" x="-35.56" y="-45.72" length="middle"/>
<pin name="GND@3" x="-35.56" y="-43.18" length="middle"/>
<pin name="GND@2" x="-35.56" y="-40.64" length="middle"/>
<pin name="GND@1" x="-35.56" y="-38.1" length="middle"/>
<pin name="VDDCORE" x="-35.56" y="-17.78" length="middle"/>
<pin name="VDDANA" x="-35.56" y="-12.7" length="middle"/>
<pin name="VDDIO@4" x="-35.56" y="-7.62" length="middle"/>
<pin name="VDDIO@3" x="-35.56" y="-5.08" length="middle"/>
<pin name="VDDIO@2" x="-35.56" y="-2.54" length="middle"/>
<pin name="VDDIO@1" x="-35.56" y="0" length="middle"/>
<pin name="PA00/XIN32/SER1-0/TC2-0" x="43.18" y="0" length="middle" rot="R180"/>
<pin name="PA01/XOUT32/SER1-1/TC2-1" x="43.18" y="-2.54" length="middle" rot="R180"/>
<pin name="PA02/ADC0-1/DAC-0" x="43.18" y="-5.08" length="middle" rot="R180"/>
<pin name="PA03/ANAREF-VREFA/ADC0-1" x="43.18" y="-7.62" length="middle" rot="R180"/>
<pin name="PA04/ANAREF-VREFB/ADC0-4/SER0-0/TC0-0" x="43.18" y="-10.16" length="middle" rot="R180"/>
<pin name="PA05/ADC0-5/DAC-1/SER0-1/TC0-1" x="43.18" y="-12.7" length="middle" rot="R180"/>
<pin name="PA06/ANAREF-VREFC/ADC0-6/SER0-2/TC1-0" x="43.18" y="-15.24" length="middle" rot="R180"/>
<pin name="PA07/ADC0-7/SER0-3/TC1-1" x="43.18" y="-17.78" length="middle" rot="R180"/>
<pin name="PA08/ADC0-8/ADC1-2/SER0-0/SER2-1/TC0-0/TCC0-0" x="43.18" y="-20.32" length="middle" rot="R180"/>
<pin name="PA09/ADC0-9/ADC1-3/SER0-1/SER2-0/TC0-1/TCC0-1" x="43.18" y="-22.86" length="middle" rot="R180"/>
<pin name="PA10/ADC0-10/SER0-2/SER2-2/TC1-0/TCC0-2" x="43.18" y="-25.4" length="middle" rot="R180"/>
<pin name="PA11/ADC0-11/SER0-3/SER2-3/TC1-1/TCC0-3" x="43.18" y="-27.94" length="middle" rot="R180"/>
<pin name="PA12/SER2-0/SER4-1/TC2-0/TCC0-6" x="43.18" y="-30.48" length="middle" rot="R180"/>
<pin name="PA13/SER2-1/SER4-0/TC2-1/TCC0-7" x="43.18" y="-33.02" length="middle" rot="R180"/>
<pin name="PA14/XIN0/SER2-2/SER4-2/TC3-0" x="43.18" y="-35.56" length="middle" rot="R180"/>
<pin name="PA15/XOUT0/SER2-3/SER4-3/TC3-1" x="43.18" y="-38.1" length="middle" rot="R180"/>
<pin name="PA16/SER1-0/SER3-1/TC2-0" x="43.18" y="-40.64" length="middle" rot="R180"/>
<pin name="PA17/SER1-1/SER3-0/TC2-1" x="43.18" y="-43.18" length="middle" rot="R180"/>
<pin name="PA18/SER1-2/SER3-2/TC3-0" x="43.18" y="-45.72" length="middle" rot="R180"/>
<pin name="PA19/SER1-3/SER3-3/TC3-1" x="43.18" y="-48.26" length="middle" rot="R180"/>
<pin name="PA20/SER5-2/SER3-2/TC7-0" x="43.18" y="-50.8" length="middle" rot="R180"/>
<pin name="PA21/SER5-3/SER3-3/TC7-1" x="43.18" y="-53.34" length="middle" rot="R180"/>
<pin name="PA22/SER3-0/SER5-1/TC4-0" x="43.18" y="-55.88" length="middle" rot="R180"/>
<pin name="PA23/SER3-1/SER5-0/TC4-1" x="43.18" y="-58.42" length="middle" rot="R180"/>
<pin name="PA24/SER3-2/SER5-2/TC5-0/PDEC0-0/USBDM" x="43.18" y="-60.96" length="middle" rot="R180"/>
<pin name="PA25/SER3-3/SER5-3/TC5-1/PDEC0-1/USBDP" x="43.18" y="-63.5" length="middle" rot="R180"/>
<pin name="PA27/GCLK-1" x="43.18" y="-66.04" length="middle" rot="R180"/>
<pin name="PA30/SER7-2/SER1-2/TC6-0/SWCLK" x="43.18" y="-68.58" length="middle" rot="R180"/>
<pin name="PA31/SER7-3/SER1-3/TC6-1/SWDIO" x="43.18" y="-71.12" length="middle" rot="R180"/>
<pin name="PB00/ADC0-12/SER5-2/TC7-0" x="43.18" y="-78.74" length="middle" rot="R180"/>
<pin name="PB01/ADC0-13/SER5-3/TC7-1" x="43.18" y="-81.28" length="middle" rot="R180"/>
<pin name="PB03/ADC0/SER5-1/TC6" x="43.18" y="-86.36" length="middle" rot="R180"/>
<pin name="PB04/ADC1-6" x="43.18" y="-88.9" length="middle" rot="R180"/>
<pin name="PB05/ADC1-7" x="43.18" y="-91.44" length="middle" rot="R180"/>
<pin name="PB06/ADC1-8" x="43.18" y="-93.98" length="middle" rot="R180"/>
<pin name="PB07/ADC1-9" x="43.18" y="-96.52" length="middle" rot="R180"/>
<pin name="PB08/ADC0-2/ADC1-9/SER4-0/TC4-0" x="43.18" y="-99.06" length="middle" rot="R180"/>
<pin name="PB09/ADC0-3/ADC1-1/SER4-1/TC4-1" x="43.18" y="-101.6" length="middle" rot="R180"/>
<pin name="PB10/SER4-2/TC5-0/TCC0-4" x="43.18" y="-104.14" length="middle" rot="R180"/>
<pin name="PB11/SER4-3/TC5-1/TCC0-5" x="43.18" y="-106.68" length="middle" rot="R180"/>
<pin name="PB12/SER4-0/TC4-0" x="43.18" y="-109.22" length="middle" rot="R180"/>
<pin name="PB13/SER4-1/TC4-1" x="43.18" y="-111.76" length="middle" rot="R180"/>
<pin name="PB14/SER4-2/TC5-0" x="43.18" y="-114.3" length="middle" rot="R180"/>
<pin name="PB15/SER4-3/TC5-1" x="43.18" y="-116.84" length="middle" rot="R180"/>
<pin name="PB16/SER5-0/TC6-0" x="43.18" y="-119.38" length="middle" rot="R180"/>
<pin name="PB17/SER5-1/TC6-1" x="43.18" y="-121.92" length="middle" rot="R180"/>
<pin name="PB22/XIN1/SER1-2/SER5-2/PDEC0-2/TC7-0" x="43.18" y="-124.46" length="middle" rot="R180"/>
<pin name="PB23/XOUT1/SER1-3/SER5-3/TC7-1" x="43.18" y="-127" length="middle" rot="R180"/>
<pin name="PB30/SER7-0/SER5-1/TC0-0/SWO" x="43.18" y="-129.54" length="middle" rot="R180"/>
<pin name="RESETN" x="-35.56" y="-55.88" length="middle"/>
<pin name="PB31/SER7-1/SER5-0/TC0-1" x="43.18" y="-132.08" length="middle" rot="R180"/>
<pin name="PB02/ADC0-14/SER5-0/TC6-0" x="43.18" y="-83.82" length="middle" rot="R180"/>
<wire x1="-30.48" y1="5.08" x2="38.1" y2="5.08" width="0.254" layer="94"/>
<wire x1="38.1" y1="5.08" x2="38.1" y2="-137.16" width="0.254" layer="94"/>
<wire x1="38.1" y1="-137.16" x2="-30.48" y2="-137.16" width="0.254" layer="94"/>
<wire x1="-30.48" y1="-137.16" x2="-30.48" y2="5.08" width="0.254" layer="94"/>
<text x="-5.08" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-142.24" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VSW" x="-35.56" y="-22.86" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ATSAMD51" prefix="U">
<gates>
<gate name="G$1" symbol="ATSAMD51" x="0" y="0"/>
</gates>
<devices>
<device name="QFN64" package="QFN-64-9X9MM">
<connects>
<connect gate="G$1" pin="GND@1" pad="7"/>
<connect gate="G$1" pin="GND@2" pad="22"/>
<connect gate="G$1" pin="GND@3" pad="33"/>
<connect gate="G$1" pin="GND@4" pad="47"/>
<connect gate="G$1" pin="GND@5" pad="54"/>
<connect gate="G$1" pin="PA00/XIN32/SER1-0/TC2-0" pad="1"/>
<connect gate="G$1" pin="PA01/XOUT32/SER1-1/TC2-1" pad="2"/>
<connect gate="G$1" pin="PA02/ADC0-1/DAC-0" pad="3"/>
<connect gate="G$1" pin="PA03/ANAREF-VREFA/ADC0-1" pad="4"/>
<connect gate="G$1" pin="PA04/ANAREF-VREFB/ADC0-4/SER0-0/TC0-0" pad="13"/>
<connect gate="G$1" pin="PA05/ADC0-5/DAC-1/SER0-1/TC0-1" pad="14"/>
<connect gate="G$1" pin="PA06/ANAREF-VREFC/ADC0-6/SER0-2/TC1-0" pad="15"/>
<connect gate="G$1" pin="PA07/ADC0-7/SER0-3/TC1-1" pad="16"/>
<connect gate="G$1" pin="PA08/ADC0-8/ADC1-2/SER0-0/SER2-1/TC0-0/TCC0-0" pad="17"/>
<connect gate="G$1" pin="PA09/ADC0-9/ADC1-3/SER0-1/SER2-0/TC0-1/TCC0-1" pad="18"/>
<connect gate="G$1" pin="PA10/ADC0-10/SER0-2/SER2-2/TC1-0/TCC0-2" pad="19"/>
<connect gate="G$1" pin="PA11/ADC0-11/SER0-3/SER2-3/TC1-1/TCC0-3" pad="20"/>
<connect gate="G$1" pin="PA12/SER2-0/SER4-1/TC2-0/TCC0-6" pad="29"/>
<connect gate="G$1" pin="PA13/SER2-1/SER4-0/TC2-1/TCC0-7" pad="30"/>
<connect gate="G$1" pin="PA14/XIN0/SER2-2/SER4-2/TC3-0" pad="31"/>
<connect gate="G$1" pin="PA15/XOUT0/SER2-3/SER4-3/TC3-1" pad="32"/>
<connect gate="G$1" pin="PA16/SER1-0/SER3-1/TC2-0" pad="35"/>
<connect gate="G$1" pin="PA17/SER1-1/SER3-0/TC2-1" pad="36"/>
<connect gate="G$1" pin="PA18/SER1-2/SER3-2/TC3-0" pad="37"/>
<connect gate="G$1" pin="PA19/SER1-3/SER3-3/TC3-1" pad="38"/>
<connect gate="G$1" pin="PA20/SER5-2/SER3-2/TC7-0" pad="41"/>
<connect gate="G$1" pin="PA21/SER5-3/SER3-3/TC7-1" pad="42"/>
<connect gate="G$1" pin="PA22/SER3-0/SER5-1/TC4-0" pad="43"/>
<connect gate="G$1" pin="PA23/SER3-1/SER5-0/TC4-1" pad="44"/>
<connect gate="G$1" pin="PA24/SER3-2/SER5-2/TC5-0/PDEC0-0/USBDM" pad="45"/>
<connect gate="G$1" pin="PA25/SER3-3/SER5-3/TC5-1/PDEC0-1/USBDP" pad="46"/>
<connect gate="G$1" pin="PA27/GCLK-1" pad="51"/>
<connect gate="G$1" pin="PA30/SER7-2/SER1-2/TC6-0/SWCLK" pad="57"/>
<connect gate="G$1" pin="PA31/SER7-3/SER1-3/TC6-1/SWDIO" pad="58"/>
<connect gate="G$1" pin="PB00/ADC0-12/SER5-2/TC7-0" pad="61"/>
<connect gate="G$1" pin="PB01/ADC0-13/SER5-3/TC7-1" pad="62"/>
<connect gate="G$1" pin="PB02/ADC0-14/SER5-0/TC6-0" pad="63"/>
<connect gate="G$1" pin="PB03/ADC0/SER5-1/TC6" pad="64"/>
<connect gate="G$1" pin="PB04/ADC1-6" pad="5"/>
<connect gate="G$1" pin="PB05/ADC1-7" pad="6"/>
<connect gate="G$1" pin="PB06/ADC1-8" pad="9"/>
<connect gate="G$1" pin="PB07/ADC1-9" pad="10"/>
<connect gate="G$1" pin="PB08/ADC0-2/ADC1-9/SER4-0/TC4-0" pad="11"/>
<connect gate="G$1" pin="PB09/ADC0-3/ADC1-1/SER4-1/TC4-1" pad="12"/>
<connect gate="G$1" pin="PB10/SER4-2/TC5-0/TCC0-4" pad="23"/>
<connect gate="G$1" pin="PB11/SER4-3/TC5-1/TCC0-5" pad="24"/>
<connect gate="G$1" pin="PB12/SER4-0/TC4-0" pad="25"/>
<connect gate="G$1" pin="PB13/SER4-1/TC4-1" pad="26"/>
<connect gate="G$1" pin="PB14/SER4-2/TC5-0" pad="27"/>
<connect gate="G$1" pin="PB15/SER4-3/TC5-1" pad="28"/>
<connect gate="G$1" pin="PB16/SER5-0/TC6-0" pad="39"/>
<connect gate="G$1" pin="PB17/SER5-1/TC6-1" pad="40"/>
<connect gate="G$1" pin="PB22/XIN1/SER1-2/SER5-2/PDEC0-2/TC7-0" pad="49"/>
<connect gate="G$1" pin="PB23/XOUT1/SER1-3/SER5-3/TC7-1" pad="50"/>
<connect gate="G$1" pin="PB30/SER7-0/SER5-1/TC0-0/SWO" pad="59"/>
<connect gate="G$1" pin="PB31/SER7-1/SER5-0/TC0-1" pad="60"/>
<connect gate="G$1" pin="RESETN" pad="52"/>
<connect gate="G$1" pin="VDDANA" pad="8"/>
<connect gate="G$1" pin="VDDCORE" pad="53"/>
<connect gate="G$1" pin="VDDIO@1" pad="21"/>
<connect gate="G$1" pin="VDDIO@2" pad="34"/>
<connect gate="G$1" pin="VDDIO@3" pad="48"/>
<connect gate="G$1" pin="VDDIO@4" pad="56"/>
<connect gate="G$1" pin="VSW" pad="55"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="borkedlabs-passives">
<packages>
<package name="0805">
<smd name="1" x="-0.95" y="0" dx="0.7" dy="1.2" layer="1"/>
<smd name="2" x="0.95" y="0" dx="0.7" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.032" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="0603-CAP">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.332" x2="0.356" y2="0.332" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.319" x2="0.356" y2="-0.319" width="0.1016" layer="51"/>
<smd name="1" x="-0.8" y="0" dx="0.96" dy="0.8" layer="1"/>
<smd name="2" x="0.8" y="0" dx="0.96" dy="0.8" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4" x2="-0.3381" y2="0.4" layer="51"/>
<rectangle x1="0.3302" y1="-0.4" x2="0.8303" y2="0.4" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="0402-CAP">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="0" y1="0.0305" x2="0" y2="-0.0305" width="0.4064" layer="21"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-2.413" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="1210">
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.3" x2="1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="-1.3" x2="-1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-1.3" x2="-1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.2032" layer="21"/>
<wire x1="-1.6" y1="-1.3" x2="1.6" y2="-1.3" width="0.2032" layer="21"/>
<smd name="1" x="-1.6" y="0" dx="1.2" dy="2" layer="1"/>
<smd name="2" x="1.6" y="0" dx="1.2" dy="2" layer="1"/>
<text x="-2.07" y="1.77" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.17" y="-3.24" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.794" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.027" y1="1.245" x2="1.027" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.002" y1="-1.245" x2="1.016" y2="-1.245" width="0.1524" layer="21"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-2.54" y="1.5875" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.302" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="0603-RES">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2286" y1="-0.381" x2="0.2286" y2="0.381" layer="21"/>
</package>
<package name="0402-RES">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.778" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2032" y1="-0.3556" x2="0.2032" y2="0.3556" layer="21"/>
</package>
<package name="R2512">
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
</package>
<package name="TO220ACS">
<description>&lt;B&gt;DIODE&lt;/B&gt;&lt;p&gt;
2-lead molded, vertical</description>
<wire x1="5.08" y1="-1.143" x2="4.953" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="4.953" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="-4.699" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-4.699" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-5.08" y2="-1.143" width="0.1524" layer="21"/>
<circle x="-4.4958" y="-3.7084" radius="0.254" width="0" layer="21"/>
<pad name="C" x="-2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="A" x="2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<text x="-5.08" y="-6.0452" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-7.62" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.334" y1="-0.762" x2="5.334" y2="0" layer="21"/>
<rectangle x1="-5.334" y1="-1.27" x2="-3.429" y2="-0.762" layer="21"/>
<rectangle x1="-3.429" y1="-1.27" x2="-1.651" y2="-0.762" layer="51"/>
<rectangle x1="3.429" y1="-1.27" x2="5.334" y2="-0.762" layer="21"/>
<rectangle x1="1.651" y1="-1.27" x2="3.429" y2="-0.762" layer="51"/>
<rectangle x1="-1.651" y1="-1.27" x2="1.651" y2="-0.762" layer="21"/>
</package>
<package name="744777920-INDUCTOR">
<smd name="P$1" x="0" y="3" dx="1.7" dy="2" layer="1"/>
<smd name="P$2" x="0" y="-3" dx="1.7" dy="2" layer="1"/>
<wire x1="-4" y1="0" x2="-4" y2="3" width="0.127" layer="21"/>
<wire x1="-4" y1="3" x2="-3" y2="4" width="0.127" layer="21" curve="-90"/>
<wire x1="-3" y1="4" x2="3" y2="4" width="0.127" layer="21"/>
<wire x1="3" y1="4" x2="4" y2="3" width="0.127" layer="21" curve="-90"/>
<wire x1="4" y1="3" x2="4" y2="-3" width="0.127" layer="21"/>
<wire x1="4" y1="-3" x2="3" y2="-4" width="0.127" layer="21" curve="-90"/>
<wire x1="3" y1="-4" x2="-3" y2="-4" width="0.127" layer="21"/>
<wire x1="-3" y1="-4" x2="-4" y2="-3" width="0.127" layer="21" curve="-90"/>
<wire x1="-4" y1="-3" x2="-4" y2="0" width="0.127" layer="21"/>
<rectangle x1="-4" y1="-4" x2="4" y2="4" layer="39"/>
<text x="5.08" y="2.54" size="1.016" layer="25">&gt;NAME</text>
<text x="5.08" y="1.27" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="SPM6530-IND">
<smd name="1" x="0" y="2.775" dx="3.4" dy="1.85" layer="1"/>
<smd name="2" x="0" y="-2.775" dx="3.4" dy="1.85" layer="1"/>
<wire x1="-3.25" y1="3.85" x2="-3.25" y2="-3.85" width="0.127" layer="21"/>
<wire x1="-3.25" y1="-3.85" x2="3.25" y2="-3.85" width="0.127" layer="21"/>
<wire x1="3.25" y1="-3.85" x2="3.25" y2="3.85" width="0.127" layer="21"/>
<wire x1="3.25" y1="3.85" x2="-3.25" y2="3.85" width="0.127" layer="21"/>
<text x="3.81" y="2.54" size="1.016" layer="25">&gt;NAME</text>
<text x="3.81" y="-3.81" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="IHLP-5050FD-01-IND">
<smd name="1" x="0" y="5.4102" dx="4.953" dy="2.9464" layer="1"/>
<smd name="2" x="0" y="-5.4102" dx="4.953" dy="2.9464" layer="1"/>
<wire x1="6.4516" y1="6.604" x2="6.4516" y2="-6.604" width="0.127" layer="21"/>
<wire x1="3.81" y1="-6.604" x2="6.4516" y2="-6.604" width="0.127" layer="21"/>
<wire x1="6.4516" y1="6.604" x2="3.81" y2="6.604" width="0.127" layer="21"/>
<wire x1="-3.81" y1="6.604" x2="-6.4516" y2="6.604" width="0.127" layer="21"/>
<wire x1="-6.4516" y1="6.604" x2="-6.4516" y2="-6.604" width="0.127" layer="21"/>
<wire x1="-6.4516" y1="-6.604" x2="-3.81" y2="-6.604" width="0.127" layer="21"/>
<text x="5.08" y="7.62" size="1.016" layer="25">&gt;NAME</text>
<text x="5.08" y="-8.89" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="7443340330-IND">
<smd name="P$1" x="0" y="3.35" dx="3" dy="2.3" layer="1"/>
<smd name="P$2" x="0" y="-3.35" dx="3" dy="2.3" layer="1"/>
<wire x1="-2" y1="4" x2="-4" y2="4" width="0.127" layer="21"/>
<wire x1="-4" y1="4" x2="-4" y2="-4" width="0.127" layer="21"/>
<wire x1="-4" y1="-4" x2="-2" y2="-4" width="0.127" layer="21"/>
<wire x1="2" y1="-4" x2="4" y2="-4" width="0.127" layer="21"/>
<wire x1="4" y1="-4" x2="4" y2="4" width="0.127" layer="21"/>
<wire x1="4" y1="4" x2="2" y2="4" width="0.127" layer="21"/>
<text x="3" y="5" size="1.016" layer="25">&gt;NAME</text>
<text x="3" y="-6" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.15" y1="2.95" x2="1.15" y2="4.45" layer="51"/>
<rectangle x1="-1.15" y1="-4.45" x2="1.15" y2="-2.95" layer="51"/>
</package>
<package name="8X8-IND">
<smd name="1" x="0" y="3.2" dx="2.2" dy="1.6" layer="1"/>
<smd name="2" x="0" y="-3.2" dx="2.2" dy="1.6" layer="1"/>
<wire x1="2" y1="-4" x2="4" y2="-4" width="0.127" layer="21"/>
<wire x1="4" y1="-4" x2="4" y2="4" width="0.127" layer="21"/>
<wire x1="4" y1="4" x2="2" y2="4" width="0.127" layer="21"/>
<wire x1="-2" y1="4" x2="-4" y2="4" width="0.127" layer="21"/>
<wire x1="-4" y1="4" x2="-4" y2="-4" width="0.127" layer="21"/>
<wire x1="-4" y1="-4" x2="-2" y2="-4" width="0.127" layer="21"/>
<text x="-5" y="5" size="1.27" layer="25">&gt;NAME</text>
<text x="-5" y="-6" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.81" y1="-3.81" x2="3.81" y2="3.81" layer="39"/>
</package>
<package name="744029100-IND">
<smd name="1" x="0" y="1.1" dx="3.2" dy="1" layer="1"/>
<smd name="2" x="0" y="-1.1" dx="3.2" dy="1" layer="1"/>
<wire x1="-2" y1="2" x2="-2" y2="-2" width="0.127" layer="21"/>
<wire x1="-2" y1="-2" x2="2" y2="-2" width="0.127" layer="21"/>
<wire x1="2" y1="-2" x2="2" y2="2" width="0.127" layer="21"/>
<wire x1="2" y1="2" x2="-2" y2="2" width="0.127" layer="21"/>
<text x="-3" y="2.3" size="1.27" layer="25">&gt;NAME</text>
<text x="-3" y="-3.6" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="7447709470-IND">
<smd name="1" x="0" y="4.95" dx="5.4" dy="2.9" layer="1"/>
<smd name="2" x="0" y="-4.95" dx="5.4" dy="2.9" layer="1"/>
<wire x1="-3" y1="6" x2="-6" y2="6" width="0.127" layer="21"/>
<wire x1="-6" y1="6" x2="-6" y2="-6" width="0.127" layer="21"/>
<wire x1="-6" y1="-6" x2="-3" y2="-6" width="0.127" layer="21"/>
<wire x1="3" y1="-6" x2="6" y2="-6" width="0.127" layer="21"/>
<wire x1="6" y1="-6" x2="6" y2="6" width="0.127" layer="21"/>
<wire x1="6" y1="6" x2="3" y2="6" width="0.127" layer="21"/>
<text x="-7" y="8" size="1.27" layer="25">&gt;NAME</text>
<text x="-7" y="-9" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="7447789002-IND">
<smd name="1" x="0" y="3" dx="1.7" dy="2" layer="1"/>
<smd name="2" x="0" y="-3" dx="1.7" dy="2" layer="1"/>
<wire x1="2" y1="-4" x2="4" y2="-4" width="0.127" layer="21"/>
<wire x1="4" y1="-4" x2="4" y2="4" width="0.127" layer="21"/>
<wire x1="4" y1="4" x2="2" y2="4" width="0.127" layer="21"/>
<wire x1="-2" y1="4" x2="-4" y2="4" width="0.127" layer="21"/>
<wire x1="-4" y1="4" x2="-4" y2="-4" width="0.127" layer="21"/>
<wire x1="-4" y1="-4" x2="-2" y2="-4" width="0.127" layer="21"/>
<text x="-5" y="5" size="1.27" layer="25">&gt;NAME</text>
<text x="-5" y="-6" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.81" y1="-3.81" x2="3.81" y2="3.81" layer="39"/>
</package>
</packages>
<symbols>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<text x="1.524" y="-4.064" size="1.27" layer="97">&gt;PACKAGE</text>
<text x="1.524" y="-5.842" size="1.27" layer="97">&gt;VOLTAGE</text>
<text x="1.524" y="-7.62" size="1.27" layer="97">&gt;TYPE</text>
</symbol>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<text x="-3.81" y="-6.858" size="1.27" layer="97">&gt;PRECISION</text>
<text x="-3.81" y="-5.08" size="1.27" layer="97">&gt;PACKAGE</text>
</symbol>
<symbol name="INDUCTOR">
<wire x1="0" y1="5.08" x2="1.27" y2="3.81" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="2.54" x2="1.27" y2="3.81" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="2.54" x2="1.27" y2="1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="0" x2="1.27" y2="1.27" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-1.27" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-3.81" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="-5.08" x2="1.27" y2="-3.81" width="0.254" layer="94" curve="90" cap="flat"/>
<text x="-1.27" y="-5.08" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="3.81" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="2" x="0" y="-7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="1" x="0" y="7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<text x="6.35" y="-5.08" size="1.27" layer="97" rot="R90">&gt;PACKAGE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="CAP" prefix="C" uservalue="yes">
<description>&lt;b&gt;Capacitor&lt;/b&gt;
Standard 0603 ceramic capacitor, and 0.1" leaded capacitor.</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0805"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0603-CAP" package="0603-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0603"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0402-CAP" package="0402-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0402"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1210" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1206" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;b&gt;Resistor&lt;/b&gt;
Basic schematic elements and footprints for 0603, 1206, and PTH resistors.</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1206" constant="no"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="2010"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0805-RES" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0805"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0603-RES" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0603"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0402-RES" package="0402-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0402"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="2512"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TO220ACS" package="TO220ACS">
<connects>
<connect gate="G$1" pin="1" pad="A"/>
<connect gate="G$1" pin="2" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="INDUCTOR" prefix="L" uservalue="yes">
<gates>
<gate name="G$1" symbol="INDUCTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-744777920" package="744777920-INDUCTOR">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0805"/>
</technology>
</technologies>
</device>
<device name="-0603" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0603"/>
</technology>
</technologies>
</device>
<device name="-SPM6530" package="SPM6530-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-IHLP-5050FD-01" package="IHLP-5050FD-01-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-7443340330" package="7443340330-IND">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="7443340330"/>
</technology>
</technologies>
</device>
<device name="-0402" package="0402-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0402"/>
</technology>
</technologies>
</device>
<device name="-744778002" package="8X8-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-744029100" package="744029100-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-7447709470" package="7447709470-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-7447789002" package="7447789002-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="lights">
<packages>
<package name="LED1206">
<description>LED 1206 pads (standard pattern)</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
</package>
<package name="LED1206FAB">
<description>LED1206 FAB style (smaller pads to allow trace between)</description>
<wire x1="-2.032" y1="1.016" x2="2.032" y2="1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="1.016" x2="2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="-1.016" x2="-2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-1.016" x2="-2.032" y2="1.016" width="0.127" layer="21"/>
<smd name="1" x="-1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<smd name="2" x="1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<text x="-1.778" y="1.27" size="1.016" layer="25" ratio="15">&gt;NAME</text>
<text x="-1.778" y="-2.286" size="1.016" layer="27" ratio="15">&gt;VALUE</text>
</package>
<package name="5MM">
<description>5mm round through hole part.</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="IN" x="-1.27" y="0" drill="0.8128" diameter="1.4224"/>
<pad name="OUT" x="1.27" y="0" drill="0.8128" diameter="1.4224"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED0805">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-0.1778" y1="0.4318" x2="0.1778" y2="0" width="0.127" layer="21"/>
<wire x1="0.1778" y1="0" x2="-0.1778" y2="-0.4318" width="0.127" layer="21"/>
<wire x1="-0.1778" y1="0.4318" x2="-0.1778" y2="-0.4318" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="LED">
<description>LED</description>
<wire x1="1.27" y1="2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="1.778" x2="-3.429" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="0.635" x2="-3.302" y2="-0.762" width="0.1524" layer="94"/>
<text x="3.556" y="-2.032" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-2.032" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="0.381"/>
<vertex x="-3.048" y="1.27"/>
<vertex x="-2.54" y="0.762"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-0.762"/>
<vertex x="-2.921" y="0.127"/>
<vertex x="-2.413" y="-0.381"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED" prefix="D">
<description>LED</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LED1206">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FAB1206" package="LED1206FAB">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="5MM">
<connects>
<connect gate="G$1" pin="A" pad="IN"/>
<connect gate="G$1" pin="C" pad="OUT"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="LED0805">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="fab">
<packages>
<package name="TACT-SWITCH-KMR6">
<smd name="P$1" x="-2.05" y="0.8" dx="0.9" dy="1" layer="1" rot="R180"/>
<smd name="P$2" x="2.05" y="0.8" dx="0.9" dy="1" layer="1" rot="R180"/>
<smd name="P$3" x="-2.05" y="-0.8" dx="0.9" dy="1" layer="1" rot="R180"/>
<smd name="P$4" x="2.05" y="-0.8" dx="0.9" dy="1" layer="1" rot="R180"/>
<smd name="P$5" x="0" y="1.5" dx="1.7" dy="0.55" layer="1" rot="R180"/>
<wire x1="-1.4" y1="0.8" x2="0" y2="0.8" width="0.127" layer="51"/>
<wire x1="0" y1="0.8" x2="1.4" y2="0.8" width="0.127" layer="51"/>
<wire x1="-1.4" y1="-0.8" x2="0" y2="-0.8" width="0.127" layer="51"/>
<wire x1="0" y1="-0.8" x2="1.4" y2="-0.8" width="0.127" layer="51"/>
<wire x1="0" y1="0.8" x2="0" y2="0.6" width="0.127" layer="51"/>
<wire x1="0" y1="0.6" x2="0.4" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0" y1="-0.8" x2="0" y2="-0.5" width="0.127" layer="51"/>
<wire x1="-2.1" y1="0.2" x2="-2.1" y2="-0.2" width="0.127" layer="51"/>
<wire x1="2.1" y1="-0.2" x2="2.1" y2="0.2" width="0.127" layer="51"/>
<wire x1="2.1" y1="1.4" x2="2.1" y2="1.5" width="0.127" layer="51"/>
<wire x1="2.1" y1="1.5" x2="1" y2="1.5" width="0.127" layer="51"/>
<wire x1="-1" y1="1.5" x2="-2.1" y2="1.5" width="0.127" layer="51"/>
<wire x1="-2.1" y1="1.5" x2="-2.1" y2="1.4" width="0.127" layer="51"/>
<wire x1="-2.1" y1="-1.4" x2="-2.1" y2="-1.5" width="0.127" layer="51"/>
<wire x1="-2.1" y1="-1.5" x2="2.1" y2="-1.5" width="0.127" layer="51"/>
<wire x1="2.1" y1="-1.5" x2="2.1" y2="-1.4" width="0.127" layer="51"/>
</package>
<package name="SOT-23">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt;</description>
<wire x1="-1.4224" y1="0.381" x2="1.4732" y2="0.381" width="0.1524" layer="21"/>
<wire x1="1.4732" y1="0.381" x2="1.4732" y2="-0.381" width="0.1524" layer="21"/>
<wire x1="1.4732" y1="-0.381" x2="-1.4224" y2="-0.381" width="0.1524" layer="21"/>
<wire x1="-1.4224" y1="-0.381" x2="-1.4224" y2="0.381" width="0.1524" layer="21"/>
<smd name="3" x="0.9906" y="1.016" dx="0.7874" dy="0.889" layer="1"/>
<smd name="2" x="-0.9398" y="1.016" dx="0.7874" dy="0.889" layer="1"/>
<smd name="1" x="0.0254" y="-1.016" dx="0.7874" dy="0.889" layer="1"/>
<text x="-1.397" y="1.778" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-1.397" y="3.302" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<rectangle x1="0.7874" y1="0.4318" x2="1.1684" y2="0.9398" layer="51"/>
<rectangle x1="-1.143" y1="0.4318" x2="-0.762" y2="0.9398" layer="51"/>
<rectangle x1="-0.1778" y1="-0.9398" x2="0.2032" y2="-0.4318" layer="51"/>
</package>
<package name="TO252">
<description>&lt;b&gt;SMALL OUTLINE TRANSISTOR&lt;/b&gt;&lt;p&gt;
TS-003</description>
<wire x1="3.2766" y1="3.8354" x2="3.277" y2="-2.159" width="0.2032" layer="21"/>
<wire x1="3.277" y1="-2.159" x2="-3.277" y2="-2.159" width="0.2032" layer="21"/>
<wire x1="-3.277" y1="-2.159" x2="-3.2766" y2="3.8354" width="0.2032" layer="21"/>
<wire x1="-3.277" y1="3.835" x2="3.2774" y2="3.8346" width="0.2032" layer="51"/>
<wire x1="-3.973" y1="5.983" x2="3.973" y2="5.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-5.983" x2="-3.973" y2="-5.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-5.983" x2="-3.973" y2="5.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="5.983" x2="3.973" y2="-5.983" width="0.0508" layer="39"/>
<wire x1="-2.5654" y1="3.937" x2="-2.5654" y2="4.6482" width="0.2032" layer="51"/>
<wire x1="-2.5654" y1="4.6482" x2="-2.1082" y2="5.1054" width="0.2032" layer="51"/>
<wire x1="-2.1082" y1="5.1054" x2="2.1082" y2="5.1054" width="0.2032" layer="51"/>
<wire x1="2.1082" y1="5.1054" x2="2.5654" y2="4.6482" width="0.2032" layer="51"/>
<wire x1="2.5654" y1="4.6482" x2="2.5654" y2="3.937" width="0.2032" layer="51"/>
<wire x1="2.5654" y1="3.937" x2="-2.5654" y2="3.937" width="0.2032" layer="51"/>
<smd name="3" x="0" y="2.5" dx="5.4" dy="6.2" layer="1"/>
<smd name="1" x="-2.28" y="-4.8" dx="1" dy="1.6" layer="1"/>
<smd name="2" x="2.28" y="-4.8" dx="1" dy="1.6" layer="1"/>
<text x="-3.81" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="5.08" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.7178" y1="-5.1562" x2="-1.8542" y2="-2.2606" layer="51"/>
<rectangle x1="1.8542" y1="-5.1562" x2="2.7178" y2="-2.2606" layer="51"/>
<rectangle x1="-0.4318" y1="-3.0226" x2="0.4318" y2="-2.2606" layer="21"/>
<polygon width="0.1998" layer="51">
<vertex x="-2.5654" y="3.937"/>
<vertex x="-2.5654" y="4.6482"/>
<vertex x="-2.1082" y="5.1054"/>
<vertex x="2.1082" y="5.1054"/>
<vertex x="2.5654" y="4.6482"/>
<vertex x="2.5654" y="3.937"/>
</polygon>
</package>
<package name="SOT223">
<description>&lt;b&gt;SOT-223&lt;/b&gt;</description>
<wire x1="3.2766" y1="1.651" x2="3.2766" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="3.2766" y1="-1.651" x2="-3.2766" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="-3.2766" y1="-1.651" x2="-3.2766" y2="1.651" width="0.2032" layer="21"/>
<wire x1="-3.2766" y1="1.651" x2="3.2766" y2="1.651" width="0.2032" layer="21"/>
<smd name="1" x="-2.3114" y="-3.0988" dx="1.2192" dy="2.2352" layer="1"/>
<smd name="2" x="0" y="-3.0988" dx="1.2192" dy="2.2352" layer="1"/>
<smd name="3" x="2.3114" y="-3.0988" dx="1.2192" dy="2.2352" layer="1"/>
<smd name="4" x="0" y="3.099" dx="3.6" dy="2.2" layer="1" thermals="no"/>
<text x="-0.8255" y="4.5085" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-0.1905" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6002" y1="1.8034" x2="1.6002" y2="3.6576" layer="51"/>
<rectangle x1="-0.4318" y1="-3.6576" x2="0.4318" y2="-1.8034" layer="51"/>
<rectangle x1="-2.7432" y1="-3.6576" x2="-1.8796" y2="-1.8034" layer="51"/>
<rectangle x1="1.8796" y1="-3.6576" x2="2.7432" y2="-1.8034" layer="51"/>
<rectangle x1="-1.6002" y1="1.8034" x2="1.6002" y2="3.6576" layer="51"/>
<rectangle x1="-0.4318" y1="-3.6576" x2="0.4318" y2="-1.8034" layer="51"/>
<rectangle x1="-2.7432" y1="-3.6576" x2="-1.8796" y2="-1.8034" layer="51"/>
<rectangle x1="1.8796" y1="-3.6576" x2="2.7432" y2="-1.8034" layer="51"/>
</package>
<package name="CBA-SILK-LOGO">
<circle x="0" y="0" radius="0.254" width="0.127" layer="21"/>
<circle x="-0.762" y="0.762" radius="0.254" width="0.127" layer="21"/>
<wire x1="-0.254" y1="1.016" x2="0.254" y2="1.016" width="0.127" layer="21"/>
<wire x1="0.254" y1="1.016" x2="0.254" y2="0.508" width="0.127" layer="21"/>
<wire x1="0.254" y1="0.508" x2="-0.254" y2="0.508" width="0.127" layer="21"/>
<wire x1="-0.254" y1="0.508" x2="-0.254" y2="1.016" width="0.127" layer="21"/>
<wire x1="-1.016" y1="0.254" x2="-0.508" y2="0.254" width="0.127" layer="21"/>
<wire x1="-0.508" y1="0.254" x2="-0.508" y2="-0.254" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-0.254" x2="-1.016" y2="-0.254" width="0.127" layer="21"/>
<wire x1="-1.016" y1="-0.254" x2="-1.016" y2="0.254" width="0.127" layer="21"/>
<wire x1="0.508" y1="0.508" x2="1.016" y2="0.508" width="0.127" layer="21"/>
<wire x1="1.016" y1="0.508" x2="1.016" y2="1.016" width="0.127" layer="21"/>
<wire x1="1.016" y1="1.016" x2="0.508" y2="1.016" width="0.127" layer="21"/>
<wire x1="0.508" y1="1.016" x2="0.508" y2="0.508" width="0.127" layer="21"/>
<wire x1="0.508" y1="0.254" x2="1.016" y2="0.254" width="0.127" layer="21"/>
<wire x1="1.016" y1="0.254" x2="1.016" y2="-0.254" width="0.127" layer="21"/>
<wire x1="1.016" y1="-0.254" x2="0.508" y2="-0.254" width="0.127" layer="21"/>
<wire x1="0.508" y1="-0.254" x2="0.508" y2="0.254" width="0.127" layer="21"/>
<wire x1="0.508" y1="-0.508" x2="1.016" y2="-0.508" width="0.127" layer="21"/>
<wire x1="1.016" y1="-0.508" x2="1.016" y2="-1.016" width="0.127" layer="21"/>
<wire x1="1.016" y1="-1.016" x2="0.508" y2="-1.016" width="0.127" layer="21"/>
<wire x1="0.508" y1="-1.016" x2="0.508" y2="-0.508" width="0.127" layer="21"/>
<wire x1="0.254" y1="-0.508" x2="-0.254" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-0.254" y1="-0.508" x2="-0.254" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-0.254" y1="-1.016" x2="0.254" y2="-1.016" width="0.127" layer="21"/>
<wire x1="0.254" y1="-1.016" x2="0.254" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-0.508" x2="-1.016" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-1.016" y1="-0.508" x2="-1.016" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-1.016" y1="-1.016" x2="-0.508" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-1.016" x2="-0.508" y2="-0.508" width="0.127" layer="21"/>
</package>
<package name="MK-LOGO-SILK">
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="1.27" width="0.127" layer="21"/>
<wire x1="1.27" y1="1.27" x2="-1.27" y2="1.27" width="0.127" layer="21"/>
<wire x1="-0.9525" y1="-1.016" x2="-0.9525" y2="1.016" width="0.127" layer="21"/>
<wire x1="-0.5715" y1="0" x2="-0.9525" y2="1.016" width="0.127" layer="21"/>
<wire x1="-0.1905" y1="1.016" x2="-0.1905" y2="-1.016" width="0.127" layer="21"/>
<wire x1="0.1905" y1="-1.016" x2="0.1905" y2="0" width="0.127" layer="21"/>
<wire x1="0.1905" y1="0" x2="0.1905" y2="1.016" width="0.127" layer="21"/>
<wire x1="0.1905" y1="0" x2="0.9525" y2="1.016" width="0.127" layer="21"/>
<wire x1="0.1905" y1="0" x2="0.9525" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-0.5715" y1="0" x2="-0.1905" y2="1.016" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="TS2">
<wire x1="0" y1="1.905" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-3.175" y2="1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-1.905" x2="-3.175" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-4.445" y2="0" width="0.254" layer="94"/>
<wire x1="-4.445" y1="0" x2="-4.445" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.905" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="0" x2="-3.175" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="1.905" width="0.254" layer="94"/>
<circle x="0" y="-2.54" radius="0.127" width="0.4064" layer="94"/>
<circle x="0" y="2.54" radius="0.127" width="0.4064" layer="94"/>
<text x="-6.35" y="-2.54" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-3.81" y="3.175" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="P" x="0" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
<pin name="S" x="0" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="S1" x="2.54" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="P1" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
</symbol>
<symbol name="N_MOSFET">
<wire x1="-0.508" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-2.159" x2="0" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="1.397" y1="-0.254" x2="1.397" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="1.397" y1="-3.048" x2="0" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="2.159" x2="0" y2="2.159" width="0.1524" layer="94"/>
<wire x1="0" y1="2.159" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="3.048" x2="1.397" y2="3.048" width="0.1524" layer="94"/>
<wire x1="1.397" y1="3.048" x2="1.397" y2="0.762" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.159" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<text x="2.54" y="0" size="1.778" layer="96">&gt;VALUE</text>
<text x="2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.27" layer="95">G</text>
<text x="0" y="-5.08" size="1.27" layer="95">S</text>
<text x="0" y="5.08" size="1.27" layer="95" rot="MR180">D</text>
<rectangle x1="-2.032" y1="1.397" x2="-1.524" y2="2.921" layer="94"/>
<rectangle x1="-2.032" y1="-0.762" x2="-1.524" y2="0.762" layer="94"/>
<rectangle x1="-2.032" y1="-2.921" x2="-1.524" y2="-1.397" layer="94"/>
<pin name="D" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="S" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="G" x="-5.08" y="-2.54" visible="off" length="short" direction="pas"/>
<polygon width="0.1016" layer="94">
<vertex x="-1.524" y="0"/>
<vertex x="-0.508" y="0.635"/>
<vertex x="-0.508" y="-0.635"/>
</polygon>
<polygon width="0.1016" layer="94">
<vertex x="1.397" y="0.762"/>
<vertex x="2.032" y="-0.254"/>
<vertex x="0.762" y="-0.254"/>
</polygon>
</symbol>
<symbol name="REGULATOR_SOT223">
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="2.54" x2="-6.35" y2="-1.27" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="-1.27" x2="0" y2="-1.27" width="0.4064" layer="94"/>
<wire x1="0" y1="-1.27" x2="6.35" y2="-1.27" width="0.4064" layer="94"/>
<wire x1="6.35" y1="-1.27" x2="6.35" y2="2.54" width="0.4064" layer="94"/>
<wire x1="6.35" y1="2.54" x2="6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="6.35" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="2.54" x2="-6.35" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="6.35" y1="2.54" x2="7.62" y2="2.54" width="0.254" layer="94"/>
<text x="-6.35" y="-3.81" size="1.27" layer="95">&gt;NAME</text>
<text x="1.27" y="-3.81" size="1.27" layer="96">&gt;VALUE</text>
<pin name="IN" x="-7.62" y="2.54" length="point"/>
<pin name="GND" x="0" y="-2.54" length="point" rot="R90"/>
<pin name="OUT1" x="7.62" y="2.54" length="point" rot="R180"/>
<pin name="OUT2" x="7.62" y="0" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="2-8X4-5_SWITCH" prefix="S">
<gates>
<gate name="G$1" symbol="TS2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TACT-SWITCH-KMR6">
<connects>
<connect gate="G$1" pin="P" pad="P$1"/>
<connect gate="G$1" pin="P1" pad="P$2"/>
<connect gate="G$1" pin="S" pad="P$3"/>
<connect gate="G$1" pin="S1" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NMOSFET" prefix="T" uservalue="yes">
<description>&lt;b&gt;MOS FET&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="N_MOSFET" x="0" y="0"/>
</gates>
<devices>
<device name="SOT23" package="SOT-23">
<connects>
<connect gate="A" pin="D" pad="1"/>
<connect gate="A" pin="G" pad="3"/>
<connect gate="A" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TO252" package="TO252">
<connects>
<connect gate="A" pin="D" pad="3"/>
<connect gate="A" pin="G" pad="1"/>
<connect gate="A" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="REGULATOR_SOT223" prefix="U">
<gates>
<gate name="G$1" symbol="REGULATOR_SOT223" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT223">
<connects>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="IN" pad="3"/>
<connect gate="G$1" pin="OUT1" pad="2"/>
<connect gate="G$1" pin="OUT2" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CBA-LOGO">
<gates>
</gates>
<devices>
<device name="" package="CBA-SILK-LOGO">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MK-LOGO">
<gates>
</gates>
<devices>
<device name="" package="MK-LOGO-SILK">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+3V3" urn="urn:adsk.eagle:symbol:26950/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="VCC" urn="urn:adsk.eagle:symbol:26928/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+5V" urn="urn:adsk.eagle:symbol:26929/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+3V3" urn="urn:adsk.eagle:component:26981/1" prefix="+3V3" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC" urn="urn:adsk.eagle:component:26957/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" urn="urn:adsk.eagle:component:26963/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors">
<description>&lt;h3&gt;SparkFun Connectors&lt;/h3&gt;
This library contains electrically-functional connectors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="SAMTECH_FTSH-105-01">
<description>&lt;h3&gt;ARM Cortex Debug Connector (10-pin)&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.05"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.samtec.com/ftppub/cpdf/FTSH-1XX-XX-XXX-DV-XXX-MKT.pdf"&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CORTEX_DEBUG&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<rectangle x1="-0.2032" y1="1.7145" x2="0.2032" y2="2.921" layer="51"/>
<rectangle x1="1.0668" y1="1.7145" x2="1.4732" y2="2.921" layer="51"/>
<rectangle x1="2.3368" y1="1.7145" x2="2.7432" y2="2.921" layer="51"/>
<rectangle x1="-1.4732" y1="1.7145" x2="-1.0668" y2="2.921" layer="51"/>
<rectangle x1="-2.7432" y1="1.7145" x2="-2.3368" y2="2.921" layer="51"/>
<rectangle x1="-0.2032" y1="-2.921" x2="0.2032" y2="-1.7145" layer="51" rot="R180"/>
<rectangle x1="-1.4732" y1="-2.921" x2="-1.0668" y2="-1.7145" layer="51" rot="R180"/>
<rectangle x1="-2.7432" y1="-2.921" x2="-2.3368" y2="-1.7145" layer="51" rot="R180"/>
<rectangle x1="1.0668" y1="-2.921" x2="1.4732" y2="-1.7145" layer="51" rot="R180"/>
<rectangle x1="2.3368" y1="-2.921" x2="2.7432" y2="-1.7145" layer="51" rot="R180"/>
<smd name="6" x="0" y="2.413" dx="0.508" dy="1.27" layer="1"/>
<smd name="8" x="1.27" y="2.413" dx="0.508" dy="1.27" layer="1"/>
<smd name="10" x="2.54" y="2.413" dx="0.508" dy="1.27" layer="1"/>
<smd name="4" x="-1.27" y="2.413" dx="0.508" dy="1.27" layer="1"/>
<smd name="2" x="-2.54" y="2.413" dx="0.508" dy="1.27" layer="1"/>
<smd name="1" x="-2.54" y="-2.413" dx="0.508" dy="1.27" layer="1"/>
<smd name="3" x="-1.27" y="-2.413" dx="0.508" dy="1.27" layer="1"/>
<smd name="5" x="0" y="-2.413" dx="0.508" dy="1.27" layer="1"/>
<smd name="7" x="1.27" y="-2.413" dx="0.508" dy="1.27" layer="1"/>
<smd name="9" x="2.54" y="-2.413" dx="0.508" dy="1.27" layer="1"/>
<text x="-1.3462" y="0.4572" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.7018" y="-0.9652" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-0.8" y1="1.24" x2="0.8" y2="1.24" width="0.127" layer="21"/>
<wire x1="-6.3" y1="2.5" x2="-6.3" y2="-2.5" width="0.127" layer="51"/>
<wire x1="-6.3" y1="-2.5" x2="6.3" y2="-2.5" width="0.127" layer="51"/>
<wire x1="6.3" y1="-2.5" x2="6.3" y2="2.5" width="0.127" layer="51"/>
<wire x1="6.3" y1="2.5" x2="-6.3" y2="2.5" width="0.127" layer="51"/>
<wire x1="-5.3" y1="1.6" x2="-5.3" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-5.3" y1="-1.6" x2="5.3" y2="-1.6" width="0.127" layer="51"/>
<wire x1="5.3" y1="-1.6" x2="5.3" y2="1.6" width="0.127" layer="51"/>
<wire x1="5.3" y1="1.6" x2="-5.3" y2="1.6" width="0.127" layer="51"/>
<wire x1="-3.2" y1="1.7" x2="-3.2" y2="-1.7" width="0.127" layer="51"/>
<wire x1="-3.2" y1="1.7" x2="3.2" y2="1.7" width="0.127" layer="51"/>
<wire x1="3.2" y1="-1.7" x2="-3.2" y2="-1.7" width="0.127" layer="51"/>
<wire x1="3.2" y1="1.7" x2="3.2" y2="-1.7" width="0.127" layer="51"/>
</package>
<package name="2X5-PTH-1.27MM">
<description>&lt;h3&gt;Plated Through Hole - 2x5 ARM Cortex Debug Connector (10-pin)&lt;/h3&gt;
&lt;p&gt;tDoc (51) layer border represents maximum dimensions of plastic housing.&lt;/p&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:1.27mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”http://portal.fciconnect.com/Comergent//fci/drawing/20021111.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="8" x="1.27" y="0.635" drill="0.508" diameter="1"/>
<pad name="6" x="0" y="0.635" drill="0.508" diameter="1"/>
<pad name="4" x="-1.27" y="0.635" drill="0.508" diameter="1"/>
<pad name="2" x="-2.54" y="0.635" drill="0.508" diameter="1"/>
<pad name="10" x="2.54" y="0.635" drill="0.508" diameter="1"/>
<pad name="7" x="1.27" y="-0.635" drill="0.508" diameter="1"/>
<pad name="5" x="0" y="-0.635" drill="0.508" diameter="1"/>
<pad name="3" x="-1.27" y="-0.635" drill="0.508" diameter="1"/>
<pad name="1" x="-2.54" y="-0.635" drill="0.508" diameter="1"/>
<pad name="9" x="2.54" y="-0.635" drill="0.508" diameter="1"/>
<wire x1="-3.403" y1="-1.021" x2="-3.403" y2="-0.259" width="0.254" layer="21"/>
<wire x1="3.175" y1="1.715" x2="-3.175" y2="1.715" width="0.127" layer="51"/>
<wire x1="-3.175" y1="1.715" x2="-3.175" y2="-1.715" width="0.127" layer="51"/>
<wire x1="-3.175" y1="-1.715" x2="3.175" y2="-1.715" width="0.127" layer="51"/>
<wire x1="3.175" y1="-1.715" x2="3.175" y2="1.715" width="0.127" layer="51"/>
<text x="-1.5748" y="1.9304" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.8288" y="-2.4638" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="2X6_SMD">
<description>&lt;h3&gt;Surface Mount - 2x6&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:12&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_06x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<circle x="-3.81" y="1.27" radius="0.7" width="0.127" layer="51"/>
<circle x="-6.35" y="1.27" radius="0.7" width="0.127" layer="51"/>
<circle x="-6.35" y="-1.27" radius="0.7" width="0.127" layer="51"/>
<circle x="-3.81" y="-1.27" radius="0.7" width="0.127" layer="51"/>
<circle x="-1.27" y="-1.27" radius="0.7" width="0.127" layer="51"/>
<circle x="-1.27" y="1.27" radius="0.7" width="0.127" layer="51"/>
<wire x1="-7.62" y1="-2.5" x2="-7.62" y2="2.5" width="0.127" layer="51"/>
<wire x1="-7.62" y1="2.5" x2="7.62" y2="2.5" width="0.127" layer="51"/>
<wire x1="7.62" y1="2.5" x2="7.62" y2="-2.5" width="0.127" layer="51"/>
<wire x1="7.62" y1="-2.5" x2="-7.62" y2="-2.5" width="0.127" layer="51"/>
<rectangle x1="-4.11" y1="2.55" x2="-3.51" y2="3.35" layer="51"/>
<rectangle x1="-6.65" y1="2.55" x2="-6.05" y2="3.35" layer="51"/>
<rectangle x1="-1.57" y1="2.55" x2="-0.97" y2="3.35" layer="51"/>
<rectangle x1="-6.65" y1="-3.35" x2="-6.05" y2="-2.55" layer="51" rot="R180"/>
<rectangle x1="-4.11" y1="-3.35" x2="-3.51" y2="-2.55" layer="51" rot="R180"/>
<rectangle x1="-1.57" y1="-3.35" x2="-0.97" y2="-2.55" layer="51" rot="R180"/>
<smd name="1" x="-6.35" y="-2.85" dx="1.02" dy="1.9" layer="1"/>
<smd name="2" x="-6.35" y="2.85" dx="1.02" dy="1.9" layer="1"/>
<smd name="3" x="-3.81" y="-2.85" dx="1.02" dy="1.9" layer="1"/>
<smd name="4" x="-3.81" y="2.85" dx="1.02" dy="1.9" layer="1"/>
<smd name="5" x="-1.27" y="-2.85" dx="1.02" dy="1.9" layer="1"/>
<smd name="6" x="-1.27" y="2.85" dx="1.02" dy="1.9" layer="1"/>
<circle x="1.27" y="-1.27" radius="0.7" width="0.127" layer="51"/>
<circle x="1.27" y="1.27" radius="0.7" width="0.127" layer="51"/>
<rectangle x1="0.97" y1="2.55" x2="1.57" y2="3.35" layer="51"/>
<rectangle x1="0.97" y1="-3.35" x2="1.57" y2="-2.55" layer="51" rot="R180"/>
<smd name="7" x="1.27" y="-2.85" dx="1.02" dy="1.9" layer="1"/>
<smd name="8" x="1.27" y="2.85" dx="1.02" dy="1.9" layer="1"/>
<circle x="3.81" y="-1.27" radius="0.7" width="0.127" layer="51"/>
<circle x="3.81" y="1.27" radius="0.7" width="0.127" layer="51"/>
<rectangle x1="3.51" y1="2.55" x2="4.11" y2="3.35" layer="51"/>
<rectangle x1="3.51" y1="-3.35" x2="4.11" y2="-2.55" layer="51" rot="R180"/>
<smd name="9" x="3.81" y="-2.85" dx="1.02" dy="1.9" layer="1"/>
<smd name="10" x="3.81" y="2.85" dx="1.02" dy="1.9" layer="1"/>
<circle x="6.35" y="-1.27" radius="0.7" width="0.127" layer="51"/>
<circle x="6.35" y="1.27" radius="0.7" width="0.127" layer="51"/>
<rectangle x1="6.05" y1="2.55" x2="6.65" y2="3.35" layer="51"/>
<rectangle x1="6.05" y1="-3.35" x2="6.65" y2="-2.55" layer="51" rot="R180"/>
<smd name="11" x="6.35" y="-2.85" dx="1.02" dy="1.9" layer="1"/>
<smd name="12" x="6.35" y="2.85" dx="1.02" dy="1.9" layer="1"/>
<text x="-7.112" y="4.191" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-6.858" y="-4.699" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-7.239" y1="-3.81" x2="-7.239" y2="-2.794" width="0.2032" layer="21"/>
</package>
<package name="2X6">
<description>&lt;h3&gt;Plated Through Hole - 2x6&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:12&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_06x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.635" x2="12.065" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.27" x2="13.97" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.81" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.2032" layer="21"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.905" y2="3.81" width="0.2032" layer="21"/>
<wire x1="1.905" y1="3.81" x2="3.175" y2="3.81" width="0.2032" layer="21"/>
<wire x1="3.175" y1="3.81" x2="3.81" y2="3.175" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.175" x2="4.445" y2="3.81" width="0.2032" layer="21"/>
<wire x1="4.445" y1="3.81" x2="5.715" y2="3.81" width="0.2032" layer="21"/>
<wire x1="5.715" y1="3.81" x2="6.35" y2="3.175" width="0.2032" layer="21"/>
<wire x1="6.35" y1="3.175" x2="6.985" y2="3.81" width="0.2032" layer="21"/>
<wire x1="6.985" y1="3.81" x2="8.255" y2="3.81" width="0.2032" layer="21"/>
<wire x1="8.255" y1="3.81" x2="8.89" y2="3.175" width="0.2032" layer="21"/>
<wire x1="8.89" y1="3.175" x2="9.525" y2="3.81" width="0.2032" layer="21"/>
<wire x1="9.525" y1="3.81" x2="10.795" y2="3.81" width="0.2032" layer="21"/>
<wire x1="10.795" y1="3.81" x2="11.43" y2="3.175" width="0.2032" layer="21"/>
<wire x1="11.43" y1="3.175" x2="12.065" y2="3.81" width="0.2032" layer="21"/>
<wire x1="12.065" y1="3.81" x2="13.335" y2="3.81" width="0.2032" layer="21"/>
<wire x1="13.335" y1="3.81" x2="13.97" y2="3.175" width="0.2032" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.175" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="3.175" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="3.175" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="3.175" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="3.175" x2="13.97" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="13.335" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.985" y1="-1.27" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="2" x="0" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="3" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="2.54" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="5" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="5.08" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="7" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
<pad name="8" x="7.62" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="9" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="10" x="10.16" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="11" x="12.7" y="0" drill="1.016" diameter="1.8796"/>
<pad name="12" x="12.7" y="2.54" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51"/>
<rectangle x1="2.286" y1="2.286" x2="2.794" y2="2.794" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="2.286" x2="5.334" y2="2.794" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="2.286" x2="7.874" y2="2.794" layer="51"/>
<rectangle x1="9.906" y1="2.286" x2="10.414" y2="2.794" layer="51"/>
<rectangle x1="12.446" y1="2.286" x2="12.954" y2="2.794" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<wire x1="-0.635" y1="-1.651" x2="0.635" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.651" x2="-0.635" y2="-1.651" width="0.2032" layer="22"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.81" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.81" width="0.2032" layer="21"/>
<text x="-1.27" y="4.064" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.413" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="2X6_NOSILK">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="2" x="0" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="3" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="2.54" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="5" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="5.08" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="7" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
<pad name="8" x="7.62" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="9" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="10" x="10.16" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="11" x="12.7" y="0" drill="1.016" diameter="1.8796"/>
<pad name="12" x="12.7" y="2.54" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51"/>
<rectangle x1="2.286" y1="2.286" x2="2.794" y2="2.794" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="2.286" x2="5.334" y2="2.794" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="2.286" x2="7.874" y2="2.794" layer="51"/>
<rectangle x1="9.906" y1="2.286" x2="10.414" y2="2.794" layer="51"/>
<rectangle x1="12.446" y1="2.286" x2="12.954" y2="2.794" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<text x="-1.27" y="4.064" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.413" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X24">
<description>&lt;h3&gt;Plated Through Hole -24 Pin&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:24&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_24&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="2" x="-26.67" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="-24.13" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="-21.59" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="-19.05" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="-16.51" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="7" x="-13.97" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="8" x="-11.43" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="9" x="-8.89" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="10" x="-6.35" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="11" x="-3.81" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="12" x="-1.27" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="13" x="1.27" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="14" x="3.81" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="15" x="6.35" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="16" x="8.89" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="17" x="11.43" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="18" x="13.97" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="19" x="16.51" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="20" x="19.05" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="21" x="21.59" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="22" x="24.13" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="23" x="26.67" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="24" x="29.21" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="25" x="31.75" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="-14.224" y1="-0.254" x2="-13.716" y2="0.254" layer="51"/>
<rectangle x1="-16.764" y1="-0.254" x2="-16.256" y2="0.254" layer="51"/>
<rectangle x1="-19.304" y1="-0.254" x2="-18.796" y2="0.254" layer="51"/>
<rectangle x1="-21.844" y1="-0.254" x2="-21.336" y2="0.254" layer="51"/>
<rectangle x1="-24.384" y1="-0.254" x2="-23.876" y2="0.254" layer="51"/>
<rectangle x1="-26.924" y1="-0.254" x2="-26.416" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="51"/>
<rectangle x1="-9.144" y1="-0.254" x2="-8.636" y2="0.254" layer="51"/>
<rectangle x1="-11.684" y1="-0.254" x2="-11.176" y2="0.254" layer="51"/>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51"/>
<rectangle x1="8.636" y1="-0.254" x2="9.144" y2="0.254" layer="51"/>
<rectangle x1="11.176" y1="-0.254" x2="11.684" y2="0.254" layer="51"/>
<rectangle x1="13.716" y1="-0.254" x2="14.224" y2="0.254" layer="51"/>
<rectangle x1="16.256" y1="-0.254" x2="16.764" y2="0.254" layer="51"/>
<rectangle x1="18.796" y1="-0.254" x2="19.304" y2="0.254" layer="51"/>
<rectangle x1="21.336" y1="-0.254" x2="21.844" y2="0.254" layer="51"/>
<rectangle x1="23.876" y1="-0.254" x2="24.384" y2="0.254" layer="51"/>
<rectangle x1="26.416" y1="-0.254" x2="26.924" y2="0.254" layer="51"/>
<rectangle x1="28.956" y1="-0.254" x2="29.464" y2="0.254" layer="51"/>
<rectangle x1="31.496" y1="-0.254" x2="32.004" y2="0.254" layer="51"/>
<text x="-27.94" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-27.94" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X24_LONGPADS">
<description>&lt;h3&gt;Plated Through Hole -24 Pin Long Pads&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:24&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_24&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="33.02" y1="0.635" x2="33.02" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-27.94" y1="0.635" x2="-27.94" y2="-0.635" width="0.2032" layer="21"/>
<pad name="2" x="-26.67" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="3" x="-24.13" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="4" x="-21.59" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="5" x="-19.05" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="6" x="-16.51" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="7" x="-13.97" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="8" x="-11.43" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="9" x="-8.89" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="10" x="-6.35" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="11" x="-3.81" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="12" x="-1.27" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="13" x="1.27" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="14" x="3.81" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="15" x="6.35" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="16" x="8.89" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="17" x="11.43" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="18" x="13.97" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="19" x="16.51" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="20" x="19.05" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="21" x="21.59" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="22" x="24.13" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="23" x="26.67" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="24" x="29.21" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="25" x="31.75" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<rectangle x1="-14.224" y1="-0.254" x2="-13.716" y2="0.254" layer="51"/>
<rectangle x1="-16.764" y1="-0.254" x2="-16.256" y2="0.254" layer="51"/>
<rectangle x1="-19.304" y1="-0.254" x2="-18.796" y2="0.254" layer="51"/>
<rectangle x1="-21.844" y1="-0.254" x2="-21.336" y2="0.254" layer="51"/>
<rectangle x1="-24.384" y1="-0.254" x2="-23.876" y2="0.254" layer="51"/>
<rectangle x1="-26.924" y1="-0.254" x2="-26.416" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="51"/>
<rectangle x1="-9.144" y1="-0.254" x2="-8.636" y2="0.254" layer="51"/>
<rectangle x1="-11.684" y1="-0.254" x2="-11.176" y2="0.254" layer="51"/>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51"/>
<rectangle x1="8.636" y1="-0.254" x2="9.144" y2="0.254" layer="51"/>
<rectangle x1="11.176" y1="-0.254" x2="11.684" y2="0.254" layer="51"/>
<rectangle x1="13.716" y1="-0.254" x2="14.224" y2="0.254" layer="51"/>
<rectangle x1="16.256" y1="-0.254" x2="16.764" y2="0.254" layer="51"/>
<rectangle x1="18.796" y1="-0.254" x2="19.304" y2="0.254" layer="51"/>
<rectangle x1="21.336" y1="-0.254" x2="21.844" y2="0.254" layer="51"/>
<rectangle x1="23.876" y1="-0.254" x2="24.384" y2="0.254" layer="51"/>
<rectangle x1="26.416" y1="-0.254" x2="26.924" y2="0.254" layer="51"/>
<rectangle x1="28.956" y1="-0.254" x2="29.464" y2="0.254" layer="51"/>
<rectangle x1="31.496" y1="-0.254" x2="32.004" y2="0.254" layer="51"/>
<text x="-27.94" y="2.159" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-27.94" y="-2.794" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X24_LOCK">
<description>&lt;h3&gt;Plated Through Hole -24 Pin Locking Footprint&lt;/h3&gt;
Holes are offset 0.005" from center, to hold pins in place while soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:24&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_24&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-14.605" y1="1.27" x2="-13.335" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-13.335" y1="1.27" x2="-12.7" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-12.7" y1="-0.635" x2="-13.335" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-17.78" y1="0.635" x2="-17.145" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-17.145" y1="1.27" x2="-15.875" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-15.875" y1="1.27" x2="-15.24" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-15.24" y1="-0.635" x2="-15.875" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-15.875" y1="-1.27" x2="-17.145" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-17.145" y1="-1.27" x2="-17.78" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-14.605" y1="1.27" x2="-15.24" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-15.24" y1="-0.635" x2="-14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-13.335" y1="-1.27" x2="-14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-22.225" y1="1.27" x2="-20.955" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-20.955" y1="1.27" x2="-20.32" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-20.32" y1="-0.635" x2="-20.955" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-20.32" y1="0.635" x2="-19.685" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-19.685" y1="1.27" x2="-18.415" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-18.415" y1="1.27" x2="-17.78" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-17.78" y1="-0.635" x2="-18.415" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-18.415" y1="-1.27" x2="-19.685" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-19.685" y1="-1.27" x2="-20.32" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-25.4" y1="0.635" x2="-24.765" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-24.765" y1="1.27" x2="-23.495" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-23.495" y1="1.27" x2="-22.86" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-22.86" y1="-0.635" x2="-23.495" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-23.495" y1="-1.27" x2="-24.765" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-24.765" y1="-1.27" x2="-25.4" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-22.225" y1="1.27" x2="-22.86" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-22.86" y1="-0.635" x2="-22.225" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-20.955" y1="-1.27" x2="-22.225" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-27.94" y1="0.635" x2="-27.305" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-27.305" y1="1.27" x2="-26.035" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-26.035" y1="1.27" x2="-25.4" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-25.4" y1="-0.635" x2="-26.035" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-26.035" y1="-1.27" x2="-27.305" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-27.305" y1="-1.27" x2="-27.94" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.08" y2="0.635" width="0.2032" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="2.54" y2="0.635" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-2.54" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.2032" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-2.54" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-6.985" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-6.985" y1="1.27" x2="-5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-5.715" y1="1.27" x2="-5.08" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-5.715" y1="-1.27" x2="-6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-6.985" y1="-1.27" x2="-7.62" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-5.08" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-12.065" y1="1.27" x2="-10.795" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-10.795" y1="1.27" x2="-10.16" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-10.16" y1="-0.635" x2="-10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-10.16" y1="0.635" x2="-9.525" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-9.525" y1="1.27" x2="-8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-8.255" y1="1.27" x2="-7.62" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-7.62" y1="-0.635" x2="-8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-8.255" y1="-1.27" x2="-9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-9.525" y1="-1.27" x2="-10.16" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-12.065" y1="1.27" x2="-12.7" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-12.7" y1="-0.635" x2="-12.065" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-10.795" y1="-1.27" x2="-12.065" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="5.08" y2="0.635" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.985" y2="1.27" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="7.62" y2="0.635" width="0.2032" layer="21"/>
<wire x1="7.62" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.985" y1="-1.27" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="7.62" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="9.525" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.16" y2="0.635" width="0.2032" layer="21"/>
<wire x1="10.16" y1="-0.635" x2="9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="7.62" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.27" x2="10.16" y2="0.635" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.27" x2="12.065" y2="1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.27" x2="12.7" y2="0.635" width="0.2032" layer="21"/>
<wire x1="12.7" y1="-0.635" x2="12.065" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="10.16" y1="-0.635" x2="10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.27" x2="12.7" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.27" x2="14.605" y2="1.27" width="0.2032" layer="21"/>
<wire x1="14.605" y1="1.27" x2="15.24" y2="0.635" width="0.2032" layer="21"/>
<wire x1="15.24" y1="-0.635" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="14.605" y1="-1.27" x2="13.335" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="12.7" y1="-0.635" x2="13.335" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="1.27" x2="15.24" y2="0.635" width="0.2032" layer="21"/>
<wire x1="15.875" y1="1.27" x2="17.145" y2="1.27" width="0.2032" layer="21"/>
<wire x1="17.145" y1="1.27" x2="17.78" y2="0.635" width="0.2032" layer="21"/>
<wire x1="17.78" y1="-0.635" x2="17.145" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="17.145" y1="-1.27" x2="15.875" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="15.24" y1="-0.635" x2="15.875" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="18.415" y1="1.27" x2="17.78" y2="0.635" width="0.2032" layer="21"/>
<wire x1="18.415" y1="1.27" x2="19.685" y2="1.27" width="0.2032" layer="21"/>
<wire x1="19.685" y1="1.27" x2="20.32" y2="0.635" width="0.2032" layer="21"/>
<wire x1="20.32" y1="-0.635" x2="19.685" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="19.685" y1="-1.27" x2="18.415" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="17.78" y1="-0.635" x2="18.415" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="20.955" y1="1.27" x2="20.32" y2="0.635" width="0.2032" layer="21"/>
<wire x1="20.955" y1="1.27" x2="22.225" y2="1.27" width="0.2032" layer="21"/>
<wire x1="22.225" y1="1.27" x2="22.86" y2="0.635" width="0.2032" layer="21"/>
<wire x1="22.86" y1="-0.635" x2="22.225" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="22.225" y1="-1.27" x2="20.955" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="20.32" y1="-0.635" x2="20.955" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="23.495" y1="1.27" x2="22.86" y2="0.635" width="0.2032" layer="21"/>
<wire x1="23.495" y1="1.27" x2="24.765" y2="1.27" width="0.2032" layer="21"/>
<wire x1="24.765" y1="1.27" x2="25.4" y2="0.635" width="0.2032" layer="21"/>
<wire x1="25.4" y1="-0.635" x2="24.765" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="24.765" y1="-1.27" x2="23.495" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="22.86" y1="-0.635" x2="23.495" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="26.035" y1="1.27" x2="25.4" y2="0.635" width="0.2032" layer="21"/>
<wire x1="26.035" y1="1.27" x2="27.305" y2="1.27" width="0.2032" layer="21"/>
<wire x1="27.305" y1="1.27" x2="27.94" y2="0.635" width="0.2032" layer="21"/>
<wire x1="27.94" y1="-0.635" x2="27.305" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="27.305" y1="-1.27" x2="26.035" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="25.4" y1="-0.635" x2="26.035" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="28.575" y1="1.27" x2="27.94" y2="0.635" width="0.2032" layer="21"/>
<wire x1="28.575" y1="1.27" x2="29.845" y2="1.27" width="0.2032" layer="21"/>
<wire x1="29.845" y1="1.27" x2="30.48" y2="0.635" width="0.2032" layer="21"/>
<wire x1="30.48" y1="-0.635" x2="29.845" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="29.845" y1="-1.27" x2="28.575" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="27.94" y1="-0.635" x2="28.575" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="31.115" y1="1.27" x2="30.48" y2="0.635" width="0.2032" layer="21"/>
<wire x1="31.115" y1="1.27" x2="32.385" y2="1.27" width="0.2032" layer="21"/>
<wire x1="32.385" y1="1.27" x2="33.02" y2="0.635" width="0.2032" layer="21"/>
<wire x1="33.02" y1="0.635" x2="33.02" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="33.02" y1="-0.635" x2="32.385" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="32.385" y1="-1.27" x2="31.115" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="30.48" y1="-0.635" x2="31.115" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-27.94" y1="0.635" x2="-27.94" y2="-0.635" width="0.2032" layer="21"/>
<pad name="2" x="-26.67" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="-24.13" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="-21.59" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="-19.05" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="-16.51" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="7" x="-13.97" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="8" x="-11.43" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="9" x="-8.89" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="10" x="-6.35" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="11" x="-3.81" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="12" x="-1.27" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="13" x="1.27" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="14" x="3.81" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="15" x="6.35" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="16" x="8.89" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="17" x="11.43" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="18" x="13.97" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="19" x="16.51" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="20" x="19.05" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="21" x="21.59" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="22" x="24.13" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="23" x="26.67" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="24" x="29.21" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="25" x="31.75" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="-14.224" y1="-0.254" x2="-13.716" y2="0.254" layer="51"/>
<rectangle x1="-16.764" y1="-0.254" x2="-16.256" y2="0.254" layer="51"/>
<rectangle x1="-19.304" y1="-0.254" x2="-18.796" y2="0.254" layer="51"/>
<rectangle x1="-21.844" y1="-0.254" x2="-21.336" y2="0.254" layer="51"/>
<rectangle x1="-24.384" y1="-0.254" x2="-23.876" y2="0.254" layer="51"/>
<rectangle x1="-26.924" y1="-0.254" x2="-26.416" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="51"/>
<rectangle x1="-9.144" y1="-0.254" x2="-8.636" y2="0.254" layer="51"/>
<rectangle x1="-11.684" y1="-0.254" x2="-11.176" y2="0.254" layer="51"/>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51"/>
<rectangle x1="8.636" y1="-0.254" x2="9.144" y2="0.254" layer="51"/>
<rectangle x1="11.176" y1="-0.254" x2="11.684" y2="0.254" layer="51"/>
<rectangle x1="13.716" y1="-0.254" x2="14.224" y2="0.254" layer="51"/>
<rectangle x1="16.256" y1="-0.254" x2="16.764" y2="0.254" layer="51"/>
<rectangle x1="18.796" y1="-0.254" x2="19.304" y2="0.254" layer="51"/>
<rectangle x1="21.336" y1="-0.254" x2="21.844" y2="0.254" layer="51"/>
<rectangle x1="23.876" y1="-0.254" x2="24.384" y2="0.254" layer="51"/>
<rectangle x1="26.416" y1="-0.254" x2="26.924" y2="0.254" layer="51"/>
<rectangle x1="28.956" y1="-0.254" x2="29.464" y2="0.254" layer="51"/>
<rectangle x1="31.496" y1="-0.254" x2="32.004" y2="0.254" layer="51"/>
<text x="-27.94" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-27.94" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="2X10">
<description>&lt;h3&gt;Plated Through Hole - 2x10&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:20&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_10x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="0.635" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.635" x2="12.065" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.27" x2="13.97" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.2032" layer="21"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.905" y2="3.81" width="0.2032" layer="21"/>
<wire x1="1.905" y1="3.81" x2="3.175" y2="3.81" width="0.2032" layer="21"/>
<wire x1="3.175" y1="3.81" x2="3.81" y2="3.175" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.175" x2="4.445" y2="3.81" width="0.2032" layer="21"/>
<wire x1="4.445" y1="3.81" x2="5.715" y2="3.81" width="0.2032" layer="21"/>
<wire x1="5.715" y1="3.81" x2="6.35" y2="3.175" width="0.2032" layer="21"/>
<wire x1="6.35" y1="3.175" x2="6.985" y2="3.81" width="0.2032" layer="21"/>
<wire x1="6.985" y1="3.81" x2="8.255" y2="3.81" width="0.2032" layer="21"/>
<wire x1="8.255" y1="3.81" x2="8.89" y2="3.175" width="0.2032" layer="21"/>
<wire x1="8.89" y1="3.175" x2="9.525" y2="3.81" width="0.2032" layer="21"/>
<wire x1="9.525" y1="3.81" x2="10.795" y2="3.81" width="0.2032" layer="21"/>
<wire x1="10.795" y1="3.81" x2="11.43" y2="3.175" width="0.2032" layer="21"/>
<wire x1="11.43" y1="3.175" x2="12.065" y2="3.81" width="0.2032" layer="21"/>
<wire x1="12.065" y1="3.81" x2="13.335" y2="3.81" width="0.2032" layer="21"/>
<wire x1="13.335" y1="3.81" x2="13.97" y2="3.175" width="0.2032" layer="21"/>
<wire x1="13.97" y1="3.175" x2="14.605" y2="3.81" width="0.2032" layer="21"/>
<wire x1="14.605" y1="3.81" x2="15.875" y2="3.81" width="0.2032" layer="21"/>
<wire x1="15.875" y1="3.81" x2="16.51" y2="3.175" width="0.2032" layer="21"/>
<wire x1="16.51" y1="3.175" x2="17.145" y2="3.81" width="0.2032" layer="21"/>
<wire x1="17.145" y1="3.81" x2="18.415" y2="3.81" width="0.2032" layer="21"/>
<wire x1="18.415" y1="3.81" x2="19.05" y2="3.175" width="0.2032" layer="21"/>
<wire x1="19.05" y1="3.175" x2="19.685" y2="3.81" width="0.2032" layer="21"/>
<wire x1="19.685" y1="3.81" x2="20.955" y2="3.81" width="0.2032" layer="21"/>
<wire x1="20.955" y1="3.81" x2="21.59" y2="3.175" width="0.2032" layer="21"/>
<wire x1="21.59" y1="-0.635" x2="20.955" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="19.05" y1="-0.635" x2="19.685" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="19.05" y1="-0.635" x2="18.415" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.635" x2="17.145" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.635" x2="15.875" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.175" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="3.175" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="3.175" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="3.175" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="3.175" x2="13.97" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="3.175" x2="16.51" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="19.05" y1="3.175" x2="19.05" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="21.59" y1="3.175" x2="21.59" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="19.685" y1="-1.27" x2="20.955" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="17.145" y1="-1.27" x2="18.415" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="14.605" y1="-1.27" x2="15.875" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="13.335" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.985" y1="-1.27" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="21.59" y1="3.175" x2="22.225" y2="3.81" width="0.2032" layer="21"/>
<wire x1="22.225" y1="3.81" x2="23.495" y2="3.81" width="0.2032" layer="21"/>
<wire x1="23.495" y1="3.81" x2="24.13" y2="3.175" width="0.2032" layer="21"/>
<wire x1="24.13" y1="-0.635" x2="23.495" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="21.59" y1="-0.635" x2="22.225" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="24.13" y1="3.175" x2="24.13" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="22.225" y1="-1.27" x2="23.495" y2="-1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="2" x="0" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="3" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="2.54" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="5" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="5.08" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="7" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
<pad name="8" x="7.62" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="9" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="10" x="10.16" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="11" x="12.7" y="0" drill="1.016" diameter="1.8796"/>
<pad name="12" x="12.7" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="13" x="15.24" y="0" drill="1.016" diameter="1.8796"/>
<pad name="14" x="15.24" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="15" x="17.78" y="0" drill="1.016" diameter="1.8796"/>
<pad name="16" x="17.78" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="17" x="20.32" y="0" drill="1.016" diameter="1.8796"/>
<pad name="18" x="20.32" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="19" x="22.86" y="0" drill="1.016" diameter="1.8796"/>
<pad name="20" x="22.86" y="2.54" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51"/>
<rectangle x1="2.286" y1="2.286" x2="2.794" y2="2.794" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="2.286" x2="5.334" y2="2.794" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="2.286" x2="7.874" y2="2.794" layer="51"/>
<rectangle x1="9.906" y1="2.286" x2="10.414" y2="2.794" layer="51"/>
<rectangle x1="12.446" y1="2.286" x2="12.954" y2="2.794" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="14.986" y1="2.286" x2="15.494" y2="2.794" layer="51"/>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="17.526" y1="2.286" x2="18.034" y2="2.794" layer="51"/>
<rectangle x1="17.526" y1="-0.254" x2="18.034" y2="0.254" layer="51"/>
<rectangle x1="20.066" y1="2.286" x2="20.574" y2="2.794" layer="51"/>
<rectangle x1="20.066" y1="-0.254" x2="20.574" y2="0.254" layer="51"/>
<rectangle x1="22.606" y1="2.286" x2="23.114" y2="2.794" layer="51"/>
<rectangle x1="22.606" y1="-0.254" x2="23.114" y2="0.254" layer="51"/>
<wire x1="-0.635" y1="-1.651" x2="0.635" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.651" x2="0.635" y2="-1.651" width="0.2032" layer="22"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.81" width="0.2032" layer="21"/>
<text x="-1.27" y="4.064" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.413" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="2X10_LOCK">
<description>&lt;h3&gt;Plated Through Hole - 2x10 Locking Footprint&lt;/h3&gt;
Holes are offset 0.005", to hold pins in place while soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:20&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_10x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="0.635" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.635" x2="12.065" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.27" x2="13.97" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.2032" layer="21"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.905" y2="3.81" width="0.2032" layer="21"/>
<wire x1="1.905" y1="3.81" x2="3.175" y2="3.81" width="0.2032" layer="21"/>
<wire x1="3.175" y1="3.81" x2="3.81" y2="3.175" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.175" x2="4.445" y2="3.81" width="0.2032" layer="21"/>
<wire x1="4.445" y1="3.81" x2="5.715" y2="3.81" width="0.2032" layer="21"/>
<wire x1="5.715" y1="3.81" x2="6.35" y2="3.175" width="0.2032" layer="21"/>
<wire x1="6.35" y1="3.175" x2="6.985" y2="3.81" width="0.2032" layer="21"/>
<wire x1="6.985" y1="3.81" x2="8.255" y2="3.81" width="0.2032" layer="21"/>
<wire x1="8.255" y1="3.81" x2="8.89" y2="3.175" width="0.2032" layer="21"/>
<wire x1="8.89" y1="3.175" x2="9.525" y2="3.81" width="0.2032" layer="21"/>
<wire x1="9.525" y1="3.81" x2="10.795" y2="3.81" width="0.2032" layer="21"/>
<wire x1="10.795" y1="3.81" x2="11.43" y2="3.175" width="0.2032" layer="21"/>
<wire x1="11.43" y1="3.175" x2="12.065" y2="3.81" width="0.2032" layer="21"/>
<wire x1="12.065" y1="3.81" x2="13.335" y2="3.81" width="0.2032" layer="21"/>
<wire x1="13.335" y1="3.81" x2="13.97" y2="3.175" width="0.2032" layer="21"/>
<wire x1="13.97" y1="3.175" x2="14.605" y2="3.81" width="0.2032" layer="21"/>
<wire x1="14.605" y1="3.81" x2="15.875" y2="3.81" width="0.2032" layer="21"/>
<wire x1="15.875" y1="3.81" x2="16.51" y2="3.175" width="0.2032" layer="21"/>
<wire x1="16.51" y1="3.175" x2="17.145" y2="3.81" width="0.2032" layer="21"/>
<wire x1="17.145" y1="3.81" x2="18.415" y2="3.81" width="0.2032" layer="21"/>
<wire x1="18.415" y1="3.81" x2="19.05" y2="3.175" width="0.2032" layer="21"/>
<wire x1="19.05" y1="3.175" x2="19.685" y2="3.81" width="0.2032" layer="21"/>
<wire x1="19.685" y1="3.81" x2="20.955" y2="3.81" width="0.2032" layer="21"/>
<wire x1="20.955" y1="3.81" x2="21.59" y2="3.175" width="0.2032" layer="21"/>
<wire x1="21.59" y1="-0.635" x2="20.955" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="19.05" y1="-0.635" x2="19.685" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="19.05" y1="-0.635" x2="18.415" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.635" x2="17.145" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.635" x2="15.875" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.175" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="3.175" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="3.175" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="3.175" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="3.175" x2="13.97" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="3.175" x2="16.51" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="19.05" y1="3.175" x2="19.05" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="21.59" y1="3.175" x2="21.59" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="19.685" y1="-1.27" x2="20.955" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="17.145" y1="-1.27" x2="18.415" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="14.605" y1="-1.27" x2="15.875" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="13.335" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.985" y1="-1.27" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="21.59" y1="3.175" x2="22.225" y2="3.81" width="0.2032" layer="21"/>
<wire x1="22.225" y1="3.81" x2="23.495" y2="3.81" width="0.2032" layer="21"/>
<wire x1="23.495" y1="3.81" x2="24.13" y2="3.175" width="0.2032" layer="21"/>
<wire x1="24.13" y1="-0.635" x2="23.495" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="21.59" y1="-0.635" x2="22.225" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="24.13" y1="3.175" x2="24.13" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="22.225" y1="-1.27" x2="23.495" y2="-1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796"/>
<pad name="2" x="0" y="2.667" drill="1.016" diameter="1.8796"/>
<pad name="3" x="2.54" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="4" x="2.54" y="2.413" drill="1.016" diameter="1.8796"/>
<pad name="5" x="5.08" y="0.127" drill="1.016" diameter="1.8796"/>
<pad name="6" x="5.08" y="2.667" drill="1.016" diameter="1.8796"/>
<pad name="7" x="7.62" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="8" x="7.62" y="2.413" drill="1.016" diameter="1.8796"/>
<pad name="9" x="10.16" y="0.127" drill="1.016" diameter="1.8796"/>
<pad name="10" x="10.16" y="2.667" drill="1.016" diameter="1.8796"/>
<pad name="11" x="12.7" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="12" x="12.7" y="2.413" drill="1.016" diameter="1.8796"/>
<pad name="13" x="15.24" y="0.127" drill="1.016" diameter="1.8796"/>
<pad name="14" x="15.24" y="2.667" drill="1.016" diameter="1.8796"/>
<pad name="15" x="17.78" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="16" x="17.78" y="2.413" drill="1.016" diameter="1.8796"/>
<pad name="17" x="20.32" y="0.127" drill="1.016" diameter="1.8796"/>
<pad name="18" x="20.32" y="2.667" drill="1.016" diameter="1.8796"/>
<pad name="19" x="22.86" y="-0.127" drill="1.016" diameter="1.8796"/>
<pad name="20" x="22.86" y="2.413" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51"/>
<rectangle x1="2.286" y1="2.286" x2="2.794" y2="2.794" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="2.286" x2="5.334" y2="2.794" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="2.286" x2="7.874" y2="2.794" layer="51"/>
<rectangle x1="9.906" y1="2.286" x2="10.414" y2="2.794" layer="51"/>
<rectangle x1="12.446" y1="2.286" x2="12.954" y2="2.794" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="14.986" y1="2.286" x2="15.494" y2="2.794" layer="51"/>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="17.526" y1="2.286" x2="18.034" y2="2.794" layer="51"/>
<rectangle x1="17.526" y1="-0.254" x2="18.034" y2="0.254" layer="51"/>
<rectangle x1="20.066" y1="2.286" x2="20.574" y2="2.794" layer="51"/>
<rectangle x1="20.066" y1="-0.254" x2="20.574" y2="0.254" layer="51"/>
<rectangle x1="22.606" y1="2.286" x2="23.114" y2="2.794" layer="51"/>
<rectangle x1="22.606" y1="-0.254" x2="23.114" y2="0.254" layer="51"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.81" width="0.2032" layer="21"/>
<text x="-1.27" y="4.064" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.413" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-0.762" y1="-1.651" x2="0.635" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.651" x2="-0.762" y2="-1.651" width="0.2032" layer="22"/>
</package>
<package name="2X10_LOCK_SPECIAL">
<description>&lt;h3&gt;Plated Through Hole - 2x10 Locking Footprint Special&lt;/h3&gt;
Holes are offset 0.005", to hold pins in place while soldering. 
Pin rows are offset 5mm as well
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:20&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_10x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="-0.7874" x2="-0.635" y2="-1.4224" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.4224" x2="1.27" y2="-0.7874" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.7874" x2="1.905" y2="-1.4224" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.4224" x2="3.81" y2="-0.7874" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.7874" x2="4.445" y2="-1.4224" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.4224" x2="6.35" y2="-0.7874" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.7874" x2="6.985" y2="-1.4224" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.4224" x2="8.89" y2="-0.7874" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.7874" x2="9.525" y2="-1.4224" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.4224" x2="11.43" y2="-0.7874" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.7874" x2="12.065" y2="-1.4224" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.4224" x2="13.97" y2="-0.7874" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.7874" x2="-1.27" y2="3.3274" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="3.3274" x2="-0.635" y2="3.9624" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="3.9624" x2="0.635" y2="3.9624" width="0.2032" layer="21"/>
<wire x1="0.635" y1="3.9624" x2="1.27" y2="3.3274" width="0.2032" layer="21"/>
<wire x1="1.27" y1="3.3274" x2="1.905" y2="3.9624" width="0.2032" layer="21"/>
<wire x1="1.905" y1="3.9624" x2="3.175" y2="3.9624" width="0.2032" layer="21"/>
<wire x1="3.175" y1="3.9624" x2="3.81" y2="3.3274" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.3274" x2="4.445" y2="3.9624" width="0.2032" layer="21"/>
<wire x1="4.445" y1="3.9624" x2="5.715" y2="3.9624" width="0.2032" layer="21"/>
<wire x1="5.715" y1="3.9624" x2="6.35" y2="3.3274" width="0.2032" layer="21"/>
<wire x1="6.35" y1="3.3274" x2="6.985" y2="3.9624" width="0.2032" layer="21"/>
<wire x1="6.985" y1="3.9624" x2="8.255" y2="3.9624" width="0.2032" layer="21"/>
<wire x1="8.255" y1="3.9624" x2="8.89" y2="3.3274" width="0.2032" layer="21"/>
<wire x1="8.89" y1="3.3274" x2="9.525" y2="3.9624" width="0.2032" layer="21"/>
<wire x1="9.525" y1="3.9624" x2="10.795" y2="3.9624" width="0.2032" layer="21"/>
<wire x1="10.795" y1="3.9624" x2="11.43" y2="3.3274" width="0.2032" layer="21"/>
<wire x1="11.43" y1="3.3274" x2="12.065" y2="3.9624" width="0.2032" layer="21"/>
<wire x1="12.065" y1="3.9624" x2="13.335" y2="3.9624" width="0.2032" layer="21"/>
<wire x1="13.335" y1="3.9624" x2="13.97" y2="3.3274" width="0.2032" layer="21"/>
<wire x1="13.97" y1="3.3274" x2="14.605" y2="3.9624" width="0.2032" layer="21"/>
<wire x1="14.605" y1="3.9624" x2="15.875" y2="3.9624" width="0.2032" layer="21"/>
<wire x1="15.875" y1="3.9624" x2="16.51" y2="3.3274" width="0.2032" layer="21"/>
<wire x1="16.51" y1="3.3274" x2="17.145" y2="3.9624" width="0.2032" layer="21"/>
<wire x1="17.145" y1="3.9624" x2="18.415" y2="3.9624" width="0.2032" layer="21"/>
<wire x1="18.415" y1="3.9624" x2="19.05" y2="3.3274" width="0.2032" layer="21"/>
<wire x1="19.05" y1="3.3274" x2="19.685" y2="3.9624" width="0.2032" layer="21"/>
<wire x1="19.685" y1="3.9624" x2="20.955" y2="3.9624" width="0.2032" layer="21"/>
<wire x1="20.955" y1="3.9624" x2="21.59" y2="3.3274" width="0.2032" layer="21"/>
<wire x1="21.59" y1="-0.7874" x2="20.955" y2="-1.4224" width="0.2032" layer="21"/>
<wire x1="19.05" y1="-0.7874" x2="19.685" y2="-1.4224" width="0.2032" layer="21"/>
<wire x1="19.05" y1="-0.7874" x2="18.415" y2="-1.4224" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.7874" x2="17.145" y2="-1.4224" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.7874" x2="15.875" y2="-1.4224" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.7874" x2="14.605" y2="-1.4224" width="0.2032" layer="21"/>
<wire x1="1.27" y1="3.3274" x2="1.27" y2="-0.7874" width="0.2032" layer="21"/>
<wire x1="3.81" y1="3.3274" x2="3.81" y2="-0.7874" width="0.2032" layer="21"/>
<wire x1="6.35" y1="3.3274" x2="6.35" y2="-0.7874" width="0.2032" layer="21"/>
<wire x1="8.89" y1="3.3274" x2="8.89" y2="-0.7874" width="0.2032" layer="21"/>
<wire x1="11.43" y1="3.3274" x2="11.43" y2="-0.7874" width="0.2032" layer="21"/>
<wire x1="13.97" y1="3.3274" x2="13.97" y2="-0.7874" width="0.2032" layer="21"/>
<wire x1="16.51" y1="3.3274" x2="16.51" y2="-0.7874" width="0.2032" layer="21"/>
<wire x1="19.05" y1="3.3274" x2="19.05" y2="-0.7874" width="0.2032" layer="21"/>
<wire x1="21.59" y1="3.3274" x2="21.59" y2="-0.7874" width="0.2032" layer="21"/>
<wire x1="19.685" y1="-1.4224" x2="20.955" y2="-1.4224" width="0.2032" layer="21"/>
<wire x1="17.145" y1="-1.4224" x2="18.415" y2="-1.4224" width="0.2032" layer="21"/>
<wire x1="14.605" y1="-1.4224" x2="15.875" y2="-1.4224" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.4224" x2="13.335" y2="-1.4224" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.4224" x2="10.795" y2="-1.4224" width="0.2032" layer="21"/>
<wire x1="6.985" y1="-1.4224" x2="8.255" y2="-1.4224" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.4224" x2="5.715" y2="-1.4224" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.4224" x2="3.175" y2="-1.4224" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.4224" x2="0.635" y2="-1.4224" width="0.2032" layer="21"/>
<wire x1="21.59" y1="3.3274" x2="22.225" y2="3.9624" width="0.2032" layer="21"/>
<wire x1="22.225" y1="3.9624" x2="23.495" y2="3.9624" width="0.2032" layer="21"/>
<wire x1="23.495" y1="3.9624" x2="24.13" y2="3.3274" width="0.2032" layer="21"/>
<wire x1="24.13" y1="-0.7874" x2="23.495" y2="-1.4224" width="0.2032" layer="21"/>
<wire x1="21.59" y1="-0.7874" x2="22.225" y2="-1.4224" width="0.2032" layer="21"/>
<wire x1="24.13" y1="3.3274" x2="24.13" y2="-0.7874" width="0.2032" layer="21"/>
<wire x1="22.225" y1="-1.4224" x2="23.495" y2="-1.4224" width="0.2032" layer="21"/>
<pad name="1" x="0" y="-0.1524" drill="1.016" diameter="1.8796"/>
<pad name="2" x="0" y="2.9464" drill="1.016" diameter="1.8796"/>
<pad name="3" x="2.54" y="-0.4064" drill="1.016" diameter="1.8796"/>
<pad name="4" x="2.54" y="2.6924" drill="1.016" diameter="1.8796"/>
<pad name="5" x="5.08" y="-0.1524" drill="1.016" diameter="1.8796"/>
<pad name="6" x="5.08" y="2.9464" drill="1.016" diameter="1.8796"/>
<pad name="7" x="7.62" y="-0.4064" drill="1.016" diameter="1.8796"/>
<pad name="8" x="7.62" y="2.6924" drill="1.016" diameter="1.8796"/>
<pad name="9" x="10.16" y="-0.1524" drill="1.016" diameter="1.8796"/>
<pad name="10" x="10.16" y="2.9464" drill="1.016" diameter="1.8796"/>
<pad name="11" x="12.7" y="-0.4064" drill="1.016" diameter="1.8796"/>
<pad name="12" x="12.7" y="2.6924" drill="1.016" diameter="1.8796"/>
<pad name="13" x="15.24" y="-0.1524" drill="1.016" diameter="1.8796"/>
<pad name="14" x="15.24" y="2.9464" drill="1.016" diameter="1.8796"/>
<pad name="15" x="17.78" y="-0.4064" drill="1.016" diameter="1.8796"/>
<pad name="16" x="17.78" y="2.6924" drill="1.016" diameter="1.8796"/>
<pad name="17" x="20.32" y="-0.1524" drill="1.016" diameter="1.8796"/>
<pad name="18" x="20.32" y="2.9464" drill="1.016" diameter="1.8796"/>
<pad name="19" x="22.86" y="-0.4064" drill="1.016" diameter="1.8796"/>
<pad name="20" x="22.86" y="2.6924" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.254" y1="-0.5334" x2="0.254" y2="-0.0254" layer="51"/>
<rectangle x1="-0.254" y1="2.5654" x2="0.254" y2="3.0734" layer="51"/>
<rectangle x1="2.286" y1="2.5654" x2="2.794" y2="3.0734" layer="51"/>
<rectangle x1="2.286" y1="-0.5334" x2="2.794" y2="-0.0254" layer="51"/>
<rectangle x1="4.826" y1="2.5654" x2="5.334" y2="3.0734" layer="51"/>
<rectangle x1="4.826" y1="-0.5334" x2="5.334" y2="-0.0254" layer="51"/>
<rectangle x1="7.366" y1="2.5654" x2="7.874" y2="3.0734" layer="51"/>
<rectangle x1="9.906" y1="2.5654" x2="10.414" y2="3.0734" layer="51"/>
<rectangle x1="12.446" y1="2.5654" x2="12.954" y2="3.0734" layer="51"/>
<rectangle x1="7.366" y1="-0.5334" x2="7.874" y2="-0.0254" layer="51"/>
<rectangle x1="9.906" y1="-0.5334" x2="10.414" y2="-0.0254" layer="51"/>
<rectangle x1="12.446" y1="-0.5334" x2="12.954" y2="-0.0254" layer="51"/>
<rectangle x1="14.986" y1="2.5654" x2="15.494" y2="3.0734" layer="51"/>
<rectangle x1="14.986" y1="-0.5334" x2="15.494" y2="-0.0254" layer="51"/>
<rectangle x1="17.526" y1="2.5654" x2="18.034" y2="3.0734" layer="51"/>
<rectangle x1="17.526" y1="-0.5334" x2="18.034" y2="-0.0254" layer="51"/>
<rectangle x1="20.066" y1="2.5654" x2="20.574" y2="3.0734" layer="51"/>
<rectangle x1="20.066" y1="-0.5334" x2="20.574" y2="-0.0254" layer="51"/>
<rectangle x1="22.606" y1="2.5654" x2="23.114" y2="3.0734" layer="51"/>
<rectangle x1="22.606" y1="-0.5334" x2="23.114" y2="-0.0254" layer="51"/>
<rectangle x1="-2.794" y1="2.5654" x2="-2.286" y2="3.0734" layer="51"/>
<rectangle x1="-2.794" y1="-0.5334" x2="-2.286" y2="-0.0254" layer="51"/>
<rectangle x1="25.146" y1="2.5654" x2="25.654" y2="3.0734" layer="51"/>
<rectangle x1="25.146" y1="-0.5334" x2="25.654" y2="-0.0254" layer="51"/>
<text x="-1.27" y="4.191" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-0.762" y1="-1.778" x2="0.635" y2="-1.778" width="0.2032" layer="22"/>
<wire x1="0.635" y1="-1.778" x2="-0.762" y2="-1.778" width="0.2032" layer="21"/>
</package>
<package name="2X10_NOSILK">
<description>&lt;h3&gt;Plated Through Hole - 2x10 No Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:20&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_10x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="2" x="0" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="3" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="2.54" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="5" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="5.08" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="7" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
<pad name="8" x="7.62" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="9" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="10" x="10.16" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="11" x="12.7" y="0" drill="1.016" diameter="1.8796"/>
<pad name="12" x="12.7" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="13" x="15.24" y="0" drill="1.016" diameter="1.8796"/>
<pad name="14" x="15.24" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="15" x="17.78" y="0" drill="1.016" diameter="1.8796"/>
<pad name="16" x="17.78" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="17" x="20.32" y="0" drill="1.016" diameter="1.8796"/>
<pad name="18" x="20.32" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="19" x="22.86" y="0" drill="1.016" diameter="1.8796"/>
<pad name="20" x="22.86" y="2.54" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51"/>
<rectangle x1="2.286" y1="2.286" x2="2.794" y2="2.794" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="2.286" x2="5.334" y2="2.794" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="2.286" x2="7.874" y2="2.794" layer="51"/>
<rectangle x1="9.906" y1="2.286" x2="10.414" y2="2.794" layer="51"/>
<rectangle x1="12.446" y1="2.286" x2="12.954" y2="2.794" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="14.986" y1="2.286" x2="15.494" y2="2.794" layer="51"/>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="17.526" y1="2.286" x2="18.034" y2="2.794" layer="51"/>
<rectangle x1="17.526" y1="-0.254" x2="18.034" y2="0.254" layer="51"/>
<rectangle x1="20.066" y1="2.286" x2="20.574" y2="2.794" layer="51"/>
<rectangle x1="20.066" y1="-0.254" x2="20.574" y2="0.254" layer="51"/>
<rectangle x1="22.606" y1="2.286" x2="23.114" y2="2.794" layer="51"/>
<rectangle x1="22.606" y1="-0.254" x2="23.114" y2="0.254" layer="51"/>
<text x="-1.27" y="4.064" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.413" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-0.889" y1="-1.397" x2="0.762" y2="-1.397" width="0.2032" layer="51"/>
<wire x1="0.762" y1="-1.397" x2="-0.889" y2="-1.397" width="0.2032" layer="52"/>
</package>
<package name="2X4">
<description>&lt;h3&gt;Plated Through Hole - 2x4&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:8&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_04x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="-3.81" y="-1.27" drill="1.016" diameter="1.8796"/>
<pad name="2" x="-3.81" y="1.27" drill="1.016" diameter="1.8796"/>
<pad name="3" x="-1.27" y="-1.27" drill="1.016" diameter="1.8796"/>
<pad name="4" x="-1.27" y="1.27" drill="1.016" diameter="1.8796"/>
<pad name="5" x="1.27" y="-1.27" drill="1.016" diameter="1.8796"/>
<pad name="6" x="1.27" y="1.27" drill="1.016" diameter="1.8796"/>
<pad name="7" x="3.81" y="-1.27" drill="1.016" diameter="1.8796"/>
<pad name="8" x="3.81" y="1.27" drill="1.016" diameter="1.8796"/>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="51"/>
<text x="-5.08" y="2.794" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-5.08" y="-3.683" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="CORTEX_DEBUG">
<description>&lt;h3&gt;Cortex Debug Connector&lt;/h3&gt;
&lt;p&gt;&lt;a href="http://infocenter.arm.com/help/topic/com.arm.doc.faqs/attached/13634/cortex_debug_connectors.pdf"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<pin name="VCC" x="-15.24" y="5.08" length="short"/>
<pin name="GND@3" x="-15.24" y="2.54" length="short"/>
<pin name="GND@5" x="-15.24" y="0" length="short"/>
<pin name="KEY" x="-15.24" y="-2.54" length="short"/>
<pin name="GNDDTCT" x="-15.24" y="-5.08" length="short"/>
<pin name="!RESET" x="17.78" y="-5.08" length="short" rot="R180"/>
<pin name="NC/TDI" x="17.78" y="-2.54" length="short" rot="R180"/>
<pin name="SWO/TDO" x="17.78" y="0" length="short" rot="R180"/>
<pin name="SWDCLK/TCK" x="17.78" y="2.54" length="short" rot="R180"/>
<pin name="SWDIO/TMS" x="17.78" y="5.08" length="short" rot="R180"/>
<wire x1="-12.7" y1="-7.62" x2="-12.7" y2="7.62" width="0.254" layer="94"/>
<wire x1="-12.7" y1="7.62" x2="15.24" y2="7.62" width="0.254" layer="94"/>
<wire x1="15.24" y1="7.62" x2="15.24" y2="-7.62" width="0.254" layer="94"/>
<wire x1="15.24" y1="-7.62" x2="-12.7" y2="-7.62" width="0.254" layer="94"/>
<text x="-12.7" y="7.874" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="-12.7" y="-9.906" size="1.778" layer="96" font="vector">&gt;Value</text>
</symbol>
<symbol name="CONN_06X2">
<description>&lt;h3&gt;12 Pin Connection&lt;/h3&gt;
6x2 pin layout</description>
<wire x1="6.35" y1="-2.54" x2="-1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="7.62" x2="5.08" y2="7.62" width="0.6096" layer="94"/>
<wire x1="3.81" y1="5.08" x2="5.08" y2="5.08" width="0.6096" layer="94"/>
<wire x1="3.81" y1="2.54" x2="5.08" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="15.24" x2="-1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="6.35" y1="-2.54" x2="6.35" y2="15.24" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="15.24" x2="6.35" y2="15.24" width="0.4064" layer="94"/>
<wire x1="3.81" y1="12.7" x2="5.08" y2="12.7" width="0.6096" layer="94"/>
<wire x1="3.81" y1="10.16" x2="5.08" y2="10.16" width="0.6096" layer="94"/>
<wire x1="1.27" y1="7.62" x2="0" y2="7.62" width="0.6096" layer="94"/>
<wire x1="1.27" y1="5.08" x2="0" y2="5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="12.7" x2="0" y2="12.7" width="0.6096" layer="94"/>
<wire x1="1.27" y1="10.16" x2="0" y2="10.16" width="0.6096" layer="94"/>
<text x="-1.27" y="-4.826" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-1.27" y="15.748" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="10" x="10.16" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="8" x="10.16" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="10.16" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="10.16" y="10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="10.16" y="12.7" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="9" x="-5.08" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="7" x="-5.08" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="5" x="-5.08" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="3" x="-5.08" y="10.16" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="1" x="-5.08" y="12.7" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="11" x="-5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="12" x="10.16" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="3.81" y1="0" x2="5.08" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
</symbol>
<symbol name="CONN_24">
<description>&lt;h3&gt; 24 Pin Connection&lt;/h3&gt;</description>
<wire x1="3.81" y1="-30.48" x2="-2.54" y2="-30.48" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-25.4" x2="2.54" y2="-25.4" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-27.94" x2="2.54" y2="-27.94" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="33.02" x2="-2.54" y2="-30.48" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-30.48" x2="3.81" y2="33.02" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="33.02" x2="3.81" y2="33.02" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-20.32" x2="2.54" y2="-20.32" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-22.86" x2="2.54" y2="-22.86" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-17.78" x2="2.54" y2="-17.78" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-15.24" x2="2.54" y2="-15.24" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-12.7" x2="2.54" y2="-12.7" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-10.16" x2="2.54" y2="-10.16" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="2.54" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="7.62" x2="2.54" y2="7.62" width="0.6096" layer="94"/>
<wire x1="1.27" y1="10.16" x2="2.54" y2="10.16" width="0.6096" layer="94"/>
<wire x1="1.27" y1="12.7" x2="2.54" y2="12.7" width="0.6096" layer="94"/>
<wire x1="1.27" y1="15.24" x2="2.54" y2="15.24" width="0.6096" layer="94"/>
<wire x1="1.27" y1="17.78" x2="2.54" y2="17.78" width="0.6096" layer="94"/>
<wire x1="1.27" y1="20.32" x2="2.54" y2="20.32" width="0.6096" layer="94"/>
<wire x1="1.27" y1="22.86" x2="2.54" y2="22.86" width="0.6096" layer="94"/>
<wire x1="1.27" y1="25.4" x2="2.54" y2="25.4" width="0.6096" layer="94"/>
<wire x1="1.27" y1="27.94" x2="2.54" y2="27.94" width="0.6096" layer="94"/>
<wire x1="1.27" y1="30.48" x2="2.54" y2="30.48" width="0.6096" layer="94"/>
<text x="-2.54" y="-32.766" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-2.54" y="33.528" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="2" x="7.62" y="-27.94" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="-25.4" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="7.62" y="-22.86" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="7.62" y="-20.32" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="7.62" y="-17.78" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="7" x="7.62" y="-15.24" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="8" x="7.62" y="-12.7" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="9" x="7.62" y="-10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="10" x="7.62" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="11" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="12" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="13" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="14" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="15" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="16" x="7.62" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="17" x="7.62" y="10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="18" x="7.62" y="12.7" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="19" x="7.62" y="15.24" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="20" x="7.62" y="17.78" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="21" x="7.62" y="20.32" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="22" x="7.62" y="22.86" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="23" x="7.62" y="25.4" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="24" x="7.62" y="27.94" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="25" x="7.62" y="30.48" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="CONN_10X2">
<description>&lt;h3&gt; 20 Pin Connection&lt;/h3&gt;
10x2 pin layout</description>
<wire x1="-1.27" y1="5.08" x2="-2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="-2.54" y2="7.62" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="10.16" x2="-2.54" y2="10.16" width="0.6096" layer="94"/>
<wire x1="-3.81" y1="12.7" x2="-3.81" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="-2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-7.62" x2="-2.54" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-10.16" x2="-2.54" y2="-10.16" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-12.7" x2="-2.54" y2="-12.7" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-15.24" x2="-3.81" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="2.54" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-10.16" x2="2.54" y2="-10.16" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-12.7" x2="2.54" y2="-12.7" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-15.24" x2="3.81" y2="12.7" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="12.7" x2="3.81" y2="12.7" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="7.62" x2="2.54" y2="7.62" width="0.6096" layer="94"/>
<wire x1="1.27" y1="10.16" x2="2.54" y2="10.16" width="0.6096" layer="94"/>
<text x="-3.81" y="-17.526" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-3.81" y="13.208" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="1" x="-7.62" y="10.16" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="2" x="7.62" y="10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="-7.62" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="4" x="7.62" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="6" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="7" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="8" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="9" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="10" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="11" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="12" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="13" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="14" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="15" x="-7.62" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="16" x="7.62" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="17" x="-7.62" y="-10.16" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="18" x="7.62" y="-10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="19" x="-7.62" y="-12.7" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="20" x="7.62" y="-12.7" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="CONN_04X2">
<description>&lt;h3&gt;8 Pin Connection&lt;/h3&gt;
4x2 pin layout</description>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="-3.81" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="-3.81" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<text x="-3.81" y="-7.366" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-4.064" y="8.128" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="1" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="2" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="4" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="6" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="7" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="8" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CORTEX_DEBUG" prefix="J">
<description>&lt;h3&gt;Cortex Debug Connector - 10 pin&lt;/h3&gt;
&lt;p&gt;Supports JTAG debug, Serial Wire debug, and Serial Wire Viewer.
PTH and SMD connector options available.&lt;/p&gt;
&lt;p&gt; &lt;ul&gt;&lt;a href=”http://infocenter.arm.com/help/topic/com.arm.doc.faqs/attached/13634/cortex_debug_connectors.pdf”&gt;General Connector Information&lt;/a&gt;
&lt;p&gt;&lt;b&gt; Products:&lt;/b&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href=”http://www.digikey.com/product-detail/en/cnc-tech/3220-10-0100-00/1175-1627-ND/3883661”&gt;PTH Connector&lt;/a&gt; -via Digi-Key&lt;/li&gt;
&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/13229”&gt;SparkFun PSoc&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/13810”&gt;SparkFun T&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CORTEX_DEBUG" x="0" y="0"/>
</gates>
<devices>
<device name="_SMD" package="SAMTECH_FTSH-105-01">
<connects>
<connect gate="G$1" pin="!RESET" pad="10"/>
<connect gate="G$1" pin="GND@3" pad="3"/>
<connect gate="G$1" pin="GND@5" pad="5"/>
<connect gate="G$1" pin="GNDDTCT" pad="9"/>
<connect gate="G$1" pin="KEY" pad="7"/>
<connect gate="G$1" pin="NC/TDI" pad="8"/>
<connect gate="G$1" pin="SWDCLK/TCK" pad="4"/>
<connect gate="G$1" pin="SWDIO/TMS" pad="2"/>
<connect gate="G$1" pin="SWO/TDO" pad="6"/>
<connect gate="G$1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_PTH" package="2X5-PTH-1.27MM">
<connects>
<connect gate="G$1" pin="!RESET" pad="10"/>
<connect gate="G$1" pin="GND@3" pad="3"/>
<connect gate="G$1" pin="GND@5" pad="5"/>
<connect gate="G$1" pin="GNDDTCT" pad="9"/>
<connect gate="G$1" pin="KEY" pad="7"/>
<connect gate="G$1" pin="NC/TDI" pad="8"/>
<connect gate="G$1" pin="SWDCLK/TCK" pad="4"/>
<connect gate="G$1" pin="SWDIO/TMS" pad="2"/>
<connect gate="G$1" pin="SWO/TDO" pad="6"/>
<connect gate="G$1" pin="VCC" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CONN_06X2" prefix="J" uservalue="yes">
<description>&lt;h3&gt;Multi connection point. Often used as Generic Header-pin footprint for 0.1 inch spaced/style header connections&lt;/h3&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;On any of the 0.1 inch spaced packages, you can populate with these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/116"&gt; Break Away Headers - Straight&lt;/a&gt; (PRT-00116)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/553"&gt; Break Away Male Headers - Right Angle&lt;/a&gt; (PRT-00553)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/115"&gt; Female Headers&lt;/a&gt; (PRT-00115)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/117"&gt; Break Away Headers - Machine Pin&lt;/a&gt; (PRT-00117)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/743"&gt; Break Away Female Headers - Swiss Machine Pin&lt;/a&gt; (PRT-00743)&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="CONN_06X2" x="0" y="0"/>
</gates>
<devices>
<device name="SMD_FEMALE" package="2X6_SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH_FEMALE" package="2X6">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-12423"/>
<attribute name="VALUE" value="2X6 FEMALE"/>
</technology>
</technologies>
</device>
<device name="NO_SILK" package="2X6_NOSILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CONN_24" prefix="J" uservalue="yes">
<description>&lt;h3&gt;Multi connection point. Often used as Generic Header-pin footprint for 0.1 inch spaced/style header connections&lt;/h3&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;On any of the 0.1 inch spaced packages, you can populate with these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/116"&gt; Break Away Headers - Straight&lt;/a&gt; (PRT-00116)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/553"&gt; Break Away Male Headers - Right Angle&lt;/a&gt; (PRT-00553)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/115"&gt; Female Headers&lt;/a&gt; (PRT-00115)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/117"&gt; Break Away Headers - Machine Pin&lt;/a&gt; (PRT-00117)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/743"&gt; Break Away Female Headers - Swiss Machine Pin&lt;/a&gt; (PRT-00743)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt; For SCREWTERMINALS and SPRING TERMINALS visit here:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/search/results?term=Screw+Terminals"&gt; Screw Terimnals on SparkFun.com&lt;/a&gt; (5mm/3.5mm/2.54mm spacing)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;This device is also useful as a general connection point to wire up your design to another part of your project. Our various solder wires solder well into these plated through hole pads.&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11375"&gt; Hook-Up Wire - Assortment (Stranded, 22 AWG)&lt;/a&gt; (PRT-11375)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11367"&gt; Hook-Up Wire - Assortment (Solid Core, 22 AWG)&lt;/a&gt; (PRT-11367)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/categories/141"&gt; View the entire wire category on our website here&lt;/a&gt;&lt;/li&gt;
&lt;p&gt;&lt;/p&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;Special notes:&lt;/b&gt;
&lt;p&gt; &lt;/p&gt; Molex polarized connector foot print use with SKU : PRT-08231 with associated crimp pins and housings. 1MM SMD Version SKU: PRT-10208
&lt;p&gt;&lt;/p&gt;
NOTES ON THE VARIANTS LOCK and LOCK_LONGPADS...
This footprint was designed to help hold the alignment of a through-hole component (i.e.  6-pin header) while soldering it into place. You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is a perfectly straight line).  This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes.  Because they are alternating, it causes a "brace" to hold the component in place.  0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers. Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice.  Also,if you push a header all the way into place, it is covered up entirely on the bottom side.</description>
<gates>
<gate name="G$1" symbol="CONN_24" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X24">
<connects>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20" pad="20"/>
<connect gate="G$1" pin="21" pad="21"/>
<connect gate="G$1" pin="22" pad="22"/>
<connect gate="G$1" pin="23" pad="23"/>
<connect gate="G$1" pin="24" pad="24"/>
<connect gate="G$1" pin="25" pad="25"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LONGPADS" package="1X24_LONGPADS">
<connects>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20" pad="20"/>
<connect gate="G$1" pin="21" pad="21"/>
<connect gate="G$1" pin="22" pad="22"/>
<connect gate="G$1" pin="23" pad="23"/>
<connect gate="G$1" pin="24" pad="24"/>
<connect gate="G$1" pin="25" pad="25"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK" package="1X24_LOCK">
<connects>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20" pad="20"/>
<connect gate="G$1" pin="21" pad="21"/>
<connect gate="G$1" pin="22" pad="22"/>
<connect gate="G$1" pin="23" pad="23"/>
<connect gate="G$1" pin="24" pad="24"/>
<connect gate="G$1" pin="25" pad="25"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CONN_10X2" prefix="J" uservalue="yes">
<description>&lt;h3&gt;Multi connection point. Often used as Generic Header-pin footprint for 0.1 inch spaced/style header connections&lt;/h3&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;On any of the 0.1 inch spaced packages, you can populate with these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/116"&gt; Break Away Headers - Straight&lt;/a&gt; (PRT-00116)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/553"&gt; Break Away Male Headers - Right Angle&lt;/a&gt; (PRT-00553)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/115"&gt; Female Headers&lt;/a&gt; (PRT-00115)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/117"&gt; Break Away Headers - Machine Pin&lt;/a&gt; (PRT-00117)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/743"&gt; Break Away Female Headers - Swiss Machine Pin&lt;/a&gt; (PRT-00743)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;Special note about the "SPECIAL" package:&lt;/b&gt;
&lt;p&gt;&lt;/p&gt;
This was SPECIALLY designed to be used with our Graphic LCD Backpack.  Be sure you want to use this!  It is not only staggered on each line of header holes, but IT IS ALSO offset of the center point of the top and bottom lines by 5 mil.  This causes the headers to lock into place on the "standard" footprint on the LCD screen.  The extra squares on the tdocu layer are there simply to reference other pins (if you were to actually populate a longer header than ten long - this is what we do with the backpacks).</description>
<gates>
<gate name="G$1" symbol="CONN_10X2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2X10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20" pad="20"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2X10_LOCK" package="2X10_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20" pad="20"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SPECIAL" package="2X10_LOCK_SPECIAL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20" pad="20"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NOSILK" package="2X10_NOSILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20" pad="20"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CONN_04X2" prefix="J" uservalue="yes">
<description>&lt;h3&gt;Multi connection point. Often used as Generic Header-pin footprint for 0.1 inch spaced/style header connections&lt;/h3&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;On any of the 0.1 inch spaced packages, you can populate with these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/116"&gt; Break Away Headers - Straight&lt;/a&gt; (PRT-00116)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/553"&gt; Break Away Male Headers - Right Angle&lt;/a&gt; (PRT-00553)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/115"&gt; Female Headers&lt;/a&gt; (PRT-00115)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/117"&gt; Break Away Headers - Machine Pin&lt;/a&gt; (PRT-00117)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/743"&gt; Break Away Female Headers - Swiss Machine Pin&lt;/a&gt; (PRT-00743)&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="CONN_04X2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2X4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="connector">
<packages>
<package name="RJ12-6-SMT">
<smd name="P$1" x="-3.175" y="17.399" dx="2.54" dy="0.635" layer="1" rot="R90"/>
<smd name="P$2" x="-1.905" y="17.399" dx="2.54" dy="0.635" layer="1" rot="R90"/>
<smd name="P$3" x="-0.635" y="17.399" dx="2.54" dy="0.635" layer="1" rot="R90"/>
<smd name="P$4" x="0.635" y="17.399" dx="2.54" dy="0.635" layer="1" rot="R90"/>
<smd name="P$5" x="1.905" y="17.399" dx="2.54" dy="0.635" layer="1" rot="R90"/>
<smd name="P$6" x="3.175" y="17.399" dx="2.54" dy="0.635" layer="1" rot="R90"/>
<smd name="P$7" x="-5.334" y="6.35" dx="5.207" dy="2.54" layer="1" rot="R90"/>
<smd name="P$8" x="5.334" y="6.35" dx="5.207" dy="2.54" layer="1" rot="R90"/>
<wire x1="-6.604" y1="0" x2="-6.604" y2="16.891" width="0.127" layer="51"/>
<wire x1="-6.604" y1="16.891" x2="6.604" y2="16.891" width="0.127" layer="51"/>
<wire x1="6.604" y1="16.891" x2="6.604" y2="0" width="0.127" layer="51"/>
<wire x1="6.604" y1="0" x2="3" y2="0" width="0.127" layer="51"/>
<wire x1="3" y1="0" x2="-3" y2="0" width="0.127" layer="51"/>
<wire x1="-3" y1="0" x2="-6.604" y2="0" width="0.127" layer="51"/>
<wire x1="-2" y1="5" x2="-3" y2="0" width="0.127" layer="51"/>
<wire x1="2" y1="5" x2="3" y2="0" width="0.127" layer="51"/>
<wire x1="-6.5" y1="16.8" x2="-6.5" y2="16.3" width="0.127" layer="21"/>
<wire x1="-6.5" y1="16.3" x2="-6" y2="16.8" width="0.127" layer="21"/>
<wire x1="-6" y1="16.8" x2="-6.5" y2="16.8" width="0.127" layer="21"/>
<wire x1="6.5" y1="16.8" x2="6" y2="16.8" width="0.127" layer="21"/>
<wire x1="6" y1="16.8" x2="6.5" y2="16.3" width="0.127" layer="21"/>
<wire x1="6.5" y1="16.3" x2="6.5" y2="16.8" width="0.127" layer="21"/>
<wire x1="-6.5" y1="0.1" x2="-6.5" y2="0.6" width="0.127" layer="21"/>
<wire x1="-6.5" y1="0.6" x2="-6" y2="0.1" width="0.127" layer="21"/>
<wire x1="-6" y1="0.1" x2="-6.5" y2="0.1" width="0.127" layer="21"/>
<wire x1="6.5" y1="0.1" x2="6.5" y2="0.6" width="0.127" layer="21"/>
<wire x1="6.5" y1="0.6" x2="6" y2="0.1" width="0.127" layer="21"/>
<wire x1="6" y1="0.1" x2="6.5" y2="0.1" width="0.127" layer="21"/>
<text x="-6" y="0.5" size="0.8128" layer="21" font="vector">stlb</text>
<text x="4" y="0.5" size="0.8128" layer="21" font="vector">stlr</text>
<wire x1="-5.25" y1="-0.75" x2="-5.25" y2="0.25" width="0.127" layer="21"/>
<wire x1="5.25" y1="-0.75" x2="5.25" y2="0.25" width="0.127" layer="21"/>
<wire x1="-6.5" y1="-1.5" x2="-3.25" y2="-1.5" width="0.127" layer="51"/>
<wire x1="-3.25" y1="-1.5" x2="-2" y2="5" width="0.127" layer="51"/>
<wire x1="-2" y1="5" x2="2" y2="5" width="0.127" layer="51"/>
<wire x1="2" y1="5" x2="3.25" y2="-1.5" width="0.127" layer="51"/>
<wire x1="3.25" y1="-1.5" x2="6.5" y2="-1.5" width="0.127" layer="51"/>
</package>
<package name="RJ12-6-SMT-WIDE">
<smd name="P$1" x="-3.175" y="17.399" dx="2.54" dy="0.635" layer="1" rot="R90"/>
<smd name="P$2" x="-1.905" y="17.399" dx="2.54" dy="0.635" layer="1" rot="R90"/>
<smd name="P$3" x="-0.635" y="17.399" dx="2.54" dy="0.635" layer="1" rot="R90"/>
<smd name="P$4" x="0.635" y="17.399" dx="2.54" dy="0.635" layer="1" rot="R90"/>
<smd name="P$5" x="1.905" y="17.399" dx="2.54" dy="0.635" layer="1" rot="R90"/>
<smd name="P$6" x="3.175" y="17.399" dx="2.54" dy="0.635" layer="1" rot="R90"/>
<smd name="P$7" x="-6.604" y="6.35" dx="5.207" dy="5.08" layer="1" rot="R90"/>
<smd name="P$8" x="6.604" y="6.35" dx="5.207" dy="5.08" layer="1" rot="R90"/>
<wire x1="-6.604" y1="0" x2="-6.604" y2="16.891" width="0.127" layer="51"/>
<wire x1="-6.604" y1="16.891" x2="6.604" y2="16.891" width="0.127" layer="51"/>
<wire x1="6.604" y1="16.891" x2="6.604" y2="0" width="0.127" layer="51"/>
<wire x1="6.604" y1="0" x2="-6.604" y2="0" width="0.127" layer="51"/>
</package>
<package name="RJ12-6-SMT-TOPENTRY">
<smd name="P$1" x="-3.175" y="15.3" dx="5" dy="0.76" layer="1" rot="R90"/>
<smd name="P$2" x="-1.905" y="15.3" dx="5" dy="0.76" layer="1" rot="R90"/>
<smd name="P$3" x="-0.635" y="15.3" dx="5" dy="0.76" layer="1" rot="R90"/>
<smd name="P$4" x="0.635" y="15.3" dx="5" dy="0.76" layer="1" rot="R90"/>
<smd name="P$5" x="1.905" y="15.3" dx="5" dy="0.76" layer="1" rot="R90"/>
<smd name="P$6" x="3.175" y="15.3" dx="5" dy="0.76" layer="1" rot="R90"/>
<smd name="P$7" x="0" y="-0.25" dx="8.8" dy="4.5" layer="1"/>
<wire x1="-6.604" y1="0" x2="-6.604" y2="15.791" width="0.127" layer="51"/>
<wire x1="-6.604" y1="15.791" x2="6.604" y2="15.791" width="0.127" layer="51"/>
<wire x1="6.604" y1="15.791" x2="6.604" y2="0" width="0.127" layer="51"/>
<wire x1="6.604" y1="0" x2="-6.604" y2="0" width="0.127" layer="51"/>
<wire x1="-6.5" y1="15.7" x2="-6.5" y2="15.2" width="0.127" layer="21"/>
<wire x1="-6.5" y1="15.2" x2="-6" y2="15.7" width="0.127" layer="21"/>
<wire x1="-6" y1="15.7" x2="-6.5" y2="15.7" width="0.127" layer="21"/>
<wire x1="6.5" y1="15.7" x2="6" y2="15.7" width="0.127" layer="21"/>
<wire x1="6" y1="15.7" x2="6.5" y2="15.2" width="0.127" layer="21"/>
<wire x1="6.5" y1="15.2" x2="6.5" y2="15.7" width="0.127" layer="21"/>
<wire x1="-6.5" y1="0.1" x2="-6.5" y2="0.6" width="0.127" layer="21"/>
<wire x1="-6.5" y1="0.6" x2="-6" y2="0.1" width="0.127" layer="21"/>
<wire x1="-6" y1="0.1" x2="-6.5" y2="0.1" width="0.127" layer="21"/>
<wire x1="6.5" y1="0.1" x2="6.5" y2="0.6" width="0.127" layer="21"/>
<wire x1="6.5" y1="0.6" x2="6" y2="0.1" width="0.127" layer="21"/>
<wire x1="6" y1="0.1" x2="6.5" y2="0.1" width="0.127" layer="21"/>
<text x="-6" y="14.25" size="0.8128" layer="21" font="vector">stlb</text>
<text x="4.25" y="14.25" size="0.8128" layer="21" font="vector">stlr</text>
<wire x1="-5.25" y1="15.5" x2="-5.25" y2="16.5" width="0.127" layer="21"/>
<wire x1="5.25" y1="15.5" x2="5.25" y2="16.5" width="0.127" layer="21"/>
<wire x1="-5.75" y1="12" x2="5.75" y2="12" width="0.127" layer="51"/>
<wire x1="5.75" y1="12" x2="5.75" y2="4.25" width="0.127" layer="51"/>
<wire x1="5.75" y1="4.25" x2="2" y2="4.25" width="0.127" layer="51"/>
<wire x1="2" y1="4.25" x2="2" y2="2.25" width="0.127" layer="51"/>
<wire x1="2" y1="2.25" x2="-2" y2="2.25" width="0.127" layer="51"/>
<wire x1="-2" y1="2.25" x2="-2" y2="4.25" width="0.127" layer="51"/>
<wire x1="-2" y1="4.25" x2="-5.75" y2="4.25" width="0.127" layer="51"/>
<wire x1="-5.75" y1="4.25" x2="-5.75" y2="12" width="0.127" layer="51"/>
</package>
<package name="RJ12-6-SMT-NOSILK">
<smd name="P$1" x="-3.175" y="17.399" dx="2.54" dy="0.635" layer="1" rot="R90"/>
<smd name="P$2" x="-1.905" y="17.399" dx="2.54" dy="0.635" layer="1" rot="R90"/>
<smd name="P$3" x="-0.635" y="17.399" dx="2.54" dy="0.635" layer="1" rot="R90"/>
<smd name="P$4" x="0.635" y="17.399" dx="2.54" dy="0.635" layer="1" rot="R90"/>
<smd name="P$5" x="1.905" y="17.399" dx="2.54" dy="0.635" layer="1" rot="R90"/>
<smd name="P$6" x="3.175" y="17.399" dx="2.54" dy="0.635" layer="1" rot="R90"/>
<smd name="P$7" x="-5.334" y="6.35" dx="5.207" dy="2.54" layer="1" rot="R90"/>
<smd name="P$8" x="5.334" y="6.35" dx="5.207" dy="2.54" layer="1" rot="R90"/>
<wire x1="-6.604" y1="0" x2="-6.604" y2="16.891" width="0.127" layer="51"/>
<wire x1="-6.604" y1="16.891" x2="6.604" y2="16.891" width="0.127" layer="51"/>
<wire x1="6.604" y1="16.891" x2="6.604" y2="0" width="0.127" layer="51"/>
<wire x1="6.604" y1="0" x2="3" y2="0" width="0.127" layer="51"/>
<wire x1="3" y1="0" x2="-3" y2="0" width="0.127" layer="51"/>
<wire x1="-3" y1="0" x2="-6.604" y2="0" width="0.127" layer="51"/>
<wire x1="-2" y1="5" x2="-3" y2="0" width="0.127" layer="51"/>
<wire x1="2" y1="5" x2="3" y2="0" width="0.127" layer="51"/>
<wire x1="-6.5" y1="16.8" x2="-6.5" y2="16.3" width="0.127" layer="21"/>
<wire x1="-6.5" y1="16.3" x2="-6" y2="16.8" width="0.127" layer="21"/>
<wire x1="-6" y1="16.8" x2="-6.5" y2="16.8" width="0.127" layer="21"/>
<wire x1="6.5" y1="16.8" x2="6" y2="16.8" width="0.127" layer="21"/>
<wire x1="6" y1="16.8" x2="6.5" y2="16.3" width="0.127" layer="21"/>
<wire x1="6.5" y1="16.3" x2="6.5" y2="16.8" width="0.127" layer="21"/>
<wire x1="-6.5" y1="0.1" x2="-6.5" y2="0.6" width="0.127" layer="21"/>
<wire x1="-6.5" y1="0.6" x2="-6" y2="0.1" width="0.127" layer="21"/>
<wire x1="-6" y1="0.1" x2="-6.5" y2="0.1" width="0.127" layer="21"/>
<wire x1="6.5" y1="0.1" x2="6.5" y2="0.6" width="0.127" layer="21"/>
<wire x1="6.5" y1="0.6" x2="6" y2="0.1" width="0.127" layer="21"/>
<wire x1="6" y1="0.1" x2="6.5" y2="0.1" width="0.127" layer="21"/>
<wire x1="-6.5" y1="-1.5" x2="-3.25" y2="-1.5" width="0.127" layer="51"/>
<wire x1="-3.25" y1="-1.5" x2="-2" y2="5" width="0.127" layer="51"/>
<wire x1="-2" y1="5" x2="2" y2="5" width="0.127" layer="51"/>
<wire x1="2" y1="5" x2="3.25" y2="-1.5" width="0.127" layer="51"/>
<wire x1="3.25" y1="-1.5" x2="6.5" y2="-1.5" width="0.127" layer="51"/>
</package>
<package name="JRTOMBSTONE">
<wire x1="-1.1" y1="-1" x2="-1.1" y2="0.7" width="0.127" layer="21"/>
<wire x1="-1.1" y1="0.7" x2="1" y2="0.7" width="0.127" layer="21" curve="-180"/>
<wire x1="1" y1="0.7" x2="1" y2="-1" width="0.127" layer="21"/>
<wire x1="1" y1="-1" x2="-1.1" y2="-1" width="0.127" layer="21"/>
<wire x1="-0.8" y1="-0.5" x2="-0.6" y2="-0.5" width="0.127" layer="21"/>
<wire x1="-0.6" y1="-0.5" x2="-0.3" y2="-0.2" width="0.127" layer="21" curve="90"/>
<wire x1="-0.3" y1="-0.2" x2="-0.3" y2="0.7" width="0.127" layer="21"/>
<wire x1="-0.3" y1="0.7" x2="-0.8" y2="0.7" width="0.127" layer="21"/>
<wire x1="-0.3" y1="0.7" x2="0.1" y2="0.7" width="0.127" layer="21"/>
<wire x1="0.1" y1="0.7" x2="0.1" y2="0.1" width="0.127" layer="21"/>
<wire x1="0.1" y1="0.1" x2="0.1" y2="-0.5" width="0.127" layer="21"/>
<wire x1="0.1" y1="0.7" x2="0.4" y2="0.7" width="0.127" layer="21"/>
<wire x1="0.4" y1="0.7" x2="0.4" y2="0.1" width="0.127" layer="21" curve="-180"/>
<wire x1="0.4" y1="0.1" x2="0.7" y2="-0.5" width="0.127" layer="21"/>
<wire x1="0.4" y1="0.1" x2="0.1" y2="0.1" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="RJ12-6">
<pin name="1" x="-5.08" y="12.7" length="middle"/>
<pin name="3" x="-5.08" y="7.62" length="middle"/>
<pin name="5" x="-5.08" y="2.54" length="middle"/>
<pin name="2" x="-5.08" y="10.16" length="middle"/>
<pin name="4" x="-5.08" y="5.08" length="middle"/>
<pin name="6" x="-5.08" y="0" length="middle"/>
<wire x1="0" y1="15.24" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="7.62" y2="15.24" width="0.254" layer="94"/>
<wire x1="7.62" y1="15.24" x2="0" y2="15.24" width="0.254" layer="94"/>
<text x="0" y="15.24" size="1.778" layer="95">&gt;NAME</text>
<text x="0" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="0" y="17.78" size="1.778" layer="96">RJ12</text>
</symbol>
<symbol name="JRTOMBSTONE">
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="5.08" y2="2.54" width="0.254" layer="94" curve="-180"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-3.81" y1="3.175" x2="-1.27" y2="3.175" width="0.254" layer="94"/>
<wire x1="-1.27" y1="3.175" x2="0" y2="3.175" width="0.254" layer="94"/>
<wire x1="-1.27" y1="3.175" x2="-1.27" y2="-0.635" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-0.635" x2="-3.81" y2="-1.27" width="0.254" layer="94" curve="-143.130069"/>
<wire x1="0.635" y1="3.175" x2="0.635" y2="-1.905" width="0.254" layer="94"/>
<wire x1="0.635" y1="3.175" x2="1.27" y2="3.175" width="0.254" layer="94"/>
<wire x1="1.27" y1="3.175" x2="2.54" y2="0.635" width="0.254" layer="94" curve="-126.869898"/>
<wire x1="2.54" y1="0.635" x2="1.27" y2="0" width="0.254" layer="94" curve="-53.129967"/>
<wire x1="1.27" y1="0" x2="3.175" y2="-1.905" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RJ12-6-SMT" prefix="J">
<gates>
<gate name="G$1" symbol="RJ12-6" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RJ12-6-SMT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
<connect gate="G$1" pin="3" pad="P$3"/>
<connect gate="G$1" pin="4" pad="P$4"/>
<connect gate="G$1" pin="5" pad="P$5"/>
<connect gate="G$1" pin="6" pad="P$6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WIDE" package="RJ12-6-SMT-WIDE">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
<connect gate="G$1" pin="3" pad="P$3"/>
<connect gate="G$1" pin="4" pad="P$4"/>
<connect gate="G$1" pin="5" pad="P$5"/>
<connect gate="G$1" pin="6" pad="P$6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TOP" package="RJ12-6-SMT-TOPENTRY">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
<connect gate="G$1" pin="3" pad="P$3"/>
<connect gate="G$1" pin="4" pad="P$4"/>
<connect gate="G$1" pin="5" pad="P$5"/>
<connect gate="G$1" pin="6" pad="P$6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NOSILK" package="RJ12-6-SMT-NOSILK">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
<connect gate="G$1" pin="3" pad="P$3"/>
<connect gate="G$1" pin="4" pad="P$4"/>
<connect gate="G$1" pin="5" pad="P$5"/>
<connect gate="G$1" pin="6" pad="P$6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JRTOMBSTONE" prefix="MP">
<gates>
<gate name="G$1" symbol="JRTOMBSTONE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="JRTOMBSTONE">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="power">
<packages>
<package name="PWRPAD_SC-02_2-45MM">
<pad name="P$1" x="0" y="0" drill="2.45" diameter="4.24" thermals="no"/>
</package>
<package name="PWRPAD_4MM">
<pad name="P$1" x="0" y="0" drill="3.9878" diameter="6.35" thermals="no"/>
</package>
<package name="PWRPAD_3-25MM">
<pad name="P$1" x="0" y="0" drill="3.25" diameter="5.75" thermals="no"/>
</package>
<package name="PWRPAD_2-65MM">
<pad name="P$1" x="0" y="0" drill="2.65" diameter="4.65" thermals="no"/>
</package>
<package name="PWRPAD_2-05MM">
<pad name="P$1" x="0" y="0" drill="2.05" diameter="3.8" thermals="no"/>
</package>
<package name="PWRPAD_M3-PEM-MOUNT">
<pad name="P$1" x="0" y="0" drill="4.1" diameter="6.2" thermals="no"/>
<polygon width="0.127" layer="31">
<vertex x="-0.6" y="3"/>
<vertex x="0.6" y="3"/>
<vertex x="0.4" y="2.1"/>
<vertex x="-0.4" y="2.1"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="0.6" y="-3"/>
<vertex x="-0.6" y="-3"/>
<vertex x="-0.4" y="-2.1"/>
<vertex x="0.4" y="-2.1"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-3" y="-0.6"/>
<vertex x="-3" y="0.6"/>
<vertex x="-2.1" y="0.4"/>
<vertex x="-2.1" y="-0.4"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="3" y="0.6"/>
<vertex x="3" y="-0.6"/>
<vertex x="2.1" y="-0.4"/>
<vertex x="2.1" y="0.4"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-2.55269375" y="1.73136875"/>
<vertex x="-1.704165625" y="2.579896875"/>
<vertex x="-1.19203125" y="1.784921875"/>
<vertex x="-1.75771875" y="1.2192375"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="2.59705625" y="-1.72131875"/>
<vertex x="1.748528125" y="-2.569846875"/>
<vertex x="1.23639375" y="-1.774871875"/>
<vertex x="1.80208125" y="-1.2091875"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-1.704165625" y="-2.569846875"/>
<vertex x="-2.55269375" y="-1.72131875"/>
<vertex x="-1.75771875" y="-1.2091875"/>
<vertex x="-1.19203125" y="-1.774871875"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="1.748528125" y="2.579896875"/>
<vertex x="2.59705625" y="1.73136875"/>
<vertex x="1.80208125" y="1.2192375"/>
<vertex x="1.23639375" y="1.784921875"/>
</polygon>
<circle x="0" y="0" radius="3" width="0.125" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="PWRPAD">
<pin name="PWRPAD" x="-5.08" y="0" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PWRPAD" prefix="J">
<gates>
<gate name="G$1" symbol="PWRPAD" x="0" y="0"/>
</gates>
<devices>
<device name="SC-02_2-45MM" package="PWRPAD_SC-02_2-45MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4MM" package="PWRPAD_4MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3" package="PWRPAD_3-25MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2.5" package="PWRPAD_2-65MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2" package="PWRPAD_2-05MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="PWRPAD_M3-PEM-MOUNT">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U1" library="microcontrollers" deviceset="ATSAMD51" device="QFN64"/>
<part name="C7" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="D1" library="lights" deviceset="LED" device="0805"/>
<part name="D2" library="lights" deviceset="LED" device="0805"/>
<part name="S1" library="fab" deviceset="2-8X4-5_SWITCH" device=""/>
<part name="S2" library="fab" deviceset="2-8X4-5_SWITCH" device=""/>
<part name="J3" library="SparkFun-Connectors" deviceset="CORTEX_DEBUG" device="_SMD"/>
<part name="T1" library="fab" deviceset="NMOSFET" device="TO252"/>
<part name="T3" library="fab" deviceset="NMOSFET" device="SOT23"/>
<part name="T4" library="fab" deviceset="NMOSFET" device="SOT23"/>
<part name="J4" library="connector" deviceset="RJ12-6-SMT" device=""/>
<part name="R1" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="10k"/>
<part name="GND6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V34" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="J2" library="power" deviceset="PWRPAD" device=""/>
<part name="J1" library="power" deviceset="PWRPAD" device=""/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C6" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C4" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C3" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C2" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C1" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C5" library="borkedlabs-passives" deviceset="CAP" device="0805" value="2.2uF"/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V33" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="C12" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="L1" library="borkedlabs-passives" deviceset="INDUCTOR" device="-0805" value="10uH"/>
<part name="+3V37" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C8" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="+3V31" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="R2" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="R3" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="100R"/>
<part name="+3V35" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="D3" library="lights" deviceset="LED" device="0805"/>
<part name="D4" library="lights" deviceset="LED" device="0805"/>
<part name="J5" library="connector" deviceset="RJ12-6-SMT" device=""/>
<part name="C9" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="+3V32" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="R4" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="R5" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="100R"/>
<part name="+3V36" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="D7" library="lights" deviceset="LED" device="0805"/>
<part name="R10" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="D6" library="lights" deviceset="LED" device="0805"/>
<part name="R9" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="D5" library="lights" deviceset="LED" device="0805"/>
<part name="R8" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="+3V38" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND14" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R6" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="10R"/>
<part name="GND10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND12" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND13" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C10" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C11" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="U2" library="fab" deviceset="REGULATOR_SOT223" device=""/>
<part name="P+2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C14" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C13" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="P+3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="GND15" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="J11" library="SparkFun-Connectors" deviceset="CONN_06X2" device="NO_SILK"/>
<part name="T2" library="fab" deviceset="NMOSFET" device="TO252"/>
<part name="R7" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="10R"/>
<part name="GND11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="J12" library="SparkFun-Connectors" deviceset="CONN_24" device=""/>
<part name="GND16" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="J13" library="SparkFun-Connectors" deviceset="CONN_24" device=""/>
<part name="J15" library="SparkFun-Connectors" deviceset="CONN_24" device=""/>
<part name="J16" library="SparkFun-Connectors" deviceset="CONN_24" device=""/>
<part name="J9" library="power" deviceset="PWRPAD" device="M3"/>
<part name="J10" library="power" deviceset="PWRPAD" device="M3"/>
<part name="J14" library="SparkFun-Connectors" deviceset="CONN_10X2" device="NOSILK"/>
<part name="J7" library="SparkFun-Connectors" deviceset="CONN_04X2" device=""/>
<part name="J8" library="SparkFun-Connectors" deviceset="CONN_04X2" device=""/>
<part name="J6" library="SparkFun-Connectors" deviceset="CONN_04X2" device=""/>
<part name="GND4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND17" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V39" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="MP1" library="connector" deviceset="JRTOMBSTONE" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U1" gate="G$1" x="68.58" y="187.96"/>
<instance part="C7" gate="G$1" x="12.7" y="165.1"/>
<instance part="D1" gate="G$1" x="111.76" y="259.08" rot="R270"/>
<instance part="D2" gate="G$1" x="111.76" y="246.38" rot="R270"/>
<instance part="S1" gate="G$1" x="58.42" y="7.62" rot="R270"/>
<instance part="S2" gate="G$1" x="238.76" y="226.06" rot="R270"/>
<instance part="J3" gate="G$1" x="58.42" y="30.48"/>
<instance part="T1" gate="A" x="185.42" y="139.7"/>
<instance part="T3" gate="A" x="185.42" y="88.9"/>
<instance part="T4" gate="A" x="185.42" y="63.5"/>
<instance part="J4" gate="G$1" x="68.58" y="246.38"/>
<instance part="R1" gate="G$1" x="93.98" y="30.48" rot="R90"/>
<instance part="GND6" gate="1" x="35.56" y="30.48" rot="R270"/>
<instance part="+3V34" gate="G$1" x="22.86" y="38.1" rot="R90"/>
<instance part="J2" gate="G$1" x="48.26" y="276.86" rot="R180"/>
<instance part="J1" gate="G$1" x="48.26" y="284.48" rot="R180"/>
<instance part="P+1" gate="VCC" x="124.46" y="284.48" rot="R270"/>
<instance part="GND7" gate="1" x="96.52" y="276.86" rot="R90"/>
<instance part="C6" gate="G$1" x="12.7" y="182.88"/>
<instance part="C4" gate="G$1" x="2.54" y="182.88"/>
<instance part="C3" gate="G$1" x="-7.62" y="182.88"/>
<instance part="C2" gate="G$1" x="-17.78" y="182.88"/>
<instance part="C1" gate="G$1" x="-27.94" y="182.88"/>
<instance part="C5" gate="G$1" x="2.54" y="165.1"/>
<instance part="GND1" gate="1" x="-38.1" y="180.34" rot="R270"/>
<instance part="GND2" gate="1" x="-7.62" y="162.56" rot="R270"/>
<instance part="GND3" gate="1" x="20.32" y="149.86" rot="R270"/>
<instance part="+3V33" gate="G$1" x="22.86" y="195.58"/>
<instance part="C12" gate="G$1" x="154.94" y="175.26"/>
<instance part="L1" gate="G$1" x="172.72" y="180.34" rot="R90"/>
<instance part="+3V37" gate="G$1" x="187.96" y="180.34" rot="R270"/>
<instance part="GND8" gate="1" x="154.94" y="167.64"/>
<instance part="C8" gate="G$1" x="38.1" y="254"/>
<instance part="+3V31" gate="G$1" x="22.86" y="259.08" rot="R90"/>
<instance part="R2" gate="G$1" x="99.06" y="259.08"/>
<instance part="R3" gate="G$1" x="99.06" y="246.38"/>
<instance part="+3V35" gate="G$1" x="132.08" y="251.46" rot="R270"/>
<instance part="D3" gate="G$1" x="111.76" y="228.6" rot="R270"/>
<instance part="D4" gate="G$1" x="111.76" y="215.9" rot="R270"/>
<instance part="J5" gate="G$1" x="68.58" y="215.9"/>
<instance part="C9" gate="G$1" x="38.1" y="223.52"/>
<instance part="+3V32" gate="G$1" x="22.86" y="228.6" rot="R90"/>
<instance part="R4" gate="G$1" x="99.06" y="228.6"/>
<instance part="R5" gate="G$1" x="99.06" y="215.9"/>
<instance part="+3V36" gate="G$1" x="132.08" y="220.98" rot="R270"/>
<instance part="D7" gate="G$1" x="241.3" y="259.08" rot="R270"/>
<instance part="R10" gate="G$1" x="228.6" y="259.08"/>
<instance part="D6" gate="G$1" x="241.3" y="269.24" rot="R270"/>
<instance part="R9" gate="G$1" x="228.6" y="269.24"/>
<instance part="D5" gate="G$1" x="241.3" y="279.4" rot="R270"/>
<instance part="R8" gate="G$1" x="228.6" y="279.4"/>
<instance part="+3V38" gate="G$1" x="264.16" y="264.16" rot="R270"/>
<instance part="GND14" gate="1" x="210.82" y="279.4" rot="R270"/>
<instance part="R6" gate="G$1" x="170.18" y="137.16"/>
<instance part="GND10" gate="1" x="185.42" y="129.54"/>
<instance part="GND12" gate="1" x="185.42" y="78.74"/>
<instance part="GND13" gate="1" x="185.42" y="53.34"/>
<instance part="C10" gate="G$1" x="60.96" y="279.4"/>
<instance part="C11" gate="G$1" x="68.58" y="279.4"/>
<instance part="U2" gate="G$1" x="165.1" y="38.1"/>
<instance part="P+2" gate="VCC" x="147.32" y="40.64" rot="R90"/>
<instance part="GND9" gate="1" x="165.1" y="27.94"/>
<instance part="C14" gate="G$1" x="187.96" y="35.56"/>
<instance part="C13" gate="G$1" x="180.34" y="35.56"/>
<instance part="P+3" gate="1" x="200.66" y="40.64" rot="R270"/>
<instance part="GND15" gate="1" x="256.54" y="81.28" rot="R180"/>
<instance part="P+4" gate="1" x="236.22" y="81.28"/>
<instance part="J11" gate="G$1" x="243.84" y="58.42"/>
<instance part="T2" gate="A" x="185.42" y="114.3"/>
<instance part="R7" gate="G$1" x="170.18" y="111.76"/>
<instance part="GND11" gate="1" x="185.42" y="104.14"/>
<instance part="J12" gate="G$1" x="264.16" y="160.02" rot="R180"/>
<instance part="GND16" gate="1" x="259.08" y="226.06" rot="R90"/>
<instance part="J13" gate="G$1" x="276.86" y="160.02" rot="R180"/>
<instance part="J15" gate="G$1" x="289.56" y="160.02" rot="R180"/>
<instance part="J16" gate="G$1" x="302.26" y="160.02" rot="R180"/>
<instance part="J9" gate="G$1" x="233.68" y="195.58" rot="R90"/>
<instance part="J10" gate="G$1" x="233.68" y="142.24" rot="R270"/>
<instance part="J14" gate="G$1" x="279.4" y="71.12"/>
<instance part="J7" gate="G$1" x="215.9" y="137.16" rot="R90"/>
<instance part="J8" gate="G$1" x="215.9" y="111.76" rot="R90"/>
<instance part="J6" gate="G$1" x="99.06" y="294.64"/>
<instance part="GND4" gate="1" x="35.56" y="251.46" rot="R270"/>
<instance part="GND5" gate="1" x="35.56" y="220.98" rot="R270"/>
<instance part="GND17" gate="1" x="289.56" y="91.44" rot="R180"/>
<instance part="+3V39" gate="G$1" x="269.24" y="91.44"/>
<instance part="MP1" gate="G$1" x="127" y="0"/>
</instances>
<busses>
</busses>
<nets>
<net name="RESET" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<pinref part="J3" gate="G$1" pin="!RESET"/>
<wire x1="93.98" y1="25.4" x2="76.2" y2="25.4" width="0.1524" layer="91"/>
<label x="78.74" y="25.4" size="1.778" layer="95"/>
<pinref part="S1" gate="G$1" pin="S"/>
<wire x1="63.5" y1="7.62" x2="93.98" y2="7.62" width="0.1524" layer="91"/>
<wire x1="93.98" y1="7.62" x2="93.98" y2="25.4" width="0.1524" layer="91"/>
<junction x="93.98" y="25.4"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="RESETN"/>
<wire x1="33.02" y1="132.08" x2="20.32" y2="132.08" width="0.1524" layer="91"/>
<label x="20.32" y="132.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="SWO" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="SWO/TDO"/>
<wire x1="76.2" y1="30.48" x2="86.36" y2="30.48" width="0.1524" layer="91"/>
<label x="78.74" y="30.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PB30/SER7-0/SER5-1/TC0-0/SWO"/>
<wire x1="111.76" y1="58.42" x2="124.46" y2="58.42" width="0.1524" layer="91"/>
<label x="114.3" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="SWCLK" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="SWDCLK/TCK"/>
<wire x1="76.2" y1="33.02" x2="86.36" y2="33.02" width="0.1524" layer="91"/>
<label x="78.74" y="33.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA30/SER7-2/SER1-2/TC6-0/SWCLK"/>
<wire x1="111.76" y1="119.38" x2="124.46" y2="119.38" width="0.1524" layer="91"/>
<label x="114.3" y="119.38" size="1.778" layer="95"/>
</segment>
</net>
<net name="SWDIO" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="SWDIO/TMS"/>
<wire x1="76.2" y1="35.56" x2="86.36" y2="35.56" width="0.1524" layer="91"/>
<label x="78.74" y="35.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA31/SER7-3/SER1-3/TC6-1/SWDIO"/>
<wire x1="111.76" y1="116.84" x2="124.46" y2="116.84" width="0.1524" layer="91"/>
<label x="114.3" y="116.84" size="1.778" layer="95"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="GND@3"/>
<wire x1="43.18" y1="33.02" x2="40.64" y2="33.02" width="0.1524" layer="91"/>
<wire x1="40.64" y1="33.02" x2="40.64" y2="30.48" width="0.1524" layer="91"/>
<pinref part="J3" gate="G$1" pin="GND@5"/>
<wire x1="40.64" y1="30.48" x2="43.18" y2="30.48" width="0.1524" layer="91"/>
<wire x1="40.64" y1="30.48" x2="40.64" y2="25.4" width="0.1524" layer="91"/>
<junction x="40.64" y="30.48"/>
<pinref part="J3" gate="G$1" pin="GNDDTCT"/>
<wire x1="40.64" y1="25.4" x2="43.18" y2="25.4" width="0.1524" layer="91"/>
<wire x1="40.64" y1="30.48" x2="38.1" y2="30.48" width="0.1524" layer="91"/>
<pinref part="GND6" gate="1" pin="GND"/>
<pinref part="S1" gate="G$1" pin="P"/>
<wire x1="53.34" y1="7.62" x2="40.64" y2="7.62" width="0.1524" layer="91"/>
<wire x1="40.64" y1="7.62" x2="40.64" y2="25.4" width="0.1524" layer="91"/>
<junction x="40.64" y="25.4"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="PWRPAD"/>
<wire x1="53.34" y1="276.86" x2="60.96" y2="276.86" width="0.1524" layer="91"/>
<pinref part="GND7" gate="1" pin="GND"/>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="60.96" y1="276.86" x2="68.58" y2="276.86" width="0.1524" layer="91"/>
<junction x="60.96" y="276.86"/>
<pinref part="C11" gate="G$1" pin="2"/>
<junction x="68.58" y="276.86"/>
<wire x1="68.58" y1="276.86" x2="93.98" y2="276.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="2"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="12.7" y1="180.34" x2="2.54" y2="180.34" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="2.54" y1="180.34" x2="-7.62" y2="180.34" width="0.1524" layer="91"/>
<junction x="2.54" y="180.34"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="-7.62" y1="180.34" x2="-17.78" y2="180.34" width="0.1524" layer="91"/>
<junction x="-7.62" y="180.34"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="-17.78" y1="180.34" x2="-27.94" y2="180.34" width="0.1524" layer="91"/>
<junction x="-17.78" y="180.34"/>
<wire x1="-27.94" y1="180.34" x2="-35.56" y2="180.34" width="0.1524" layer="91"/>
<junction x="-27.94" y="180.34"/>
<pinref part="GND1" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C7" gate="G$1" pin="2"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="12.7" y1="162.56" x2="2.54" y2="162.56" width="0.1524" layer="91"/>
<wire x1="2.54" y1="162.56" x2="-5.08" y2="162.56" width="0.1524" layer="91"/>
<junction x="2.54" y="162.56"/>
<pinref part="GND2" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="GND@4"/>
<wire x1="33.02" y1="142.24" x2="30.48" y2="142.24" width="0.1524" layer="91"/>
<wire x1="30.48" y1="142.24" x2="30.48" y2="139.7" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="GND@5"/>
<wire x1="30.48" y1="139.7" x2="33.02" y2="139.7" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="GND@3"/>
<wire x1="33.02" y1="144.78" x2="30.48" y2="144.78" width="0.1524" layer="91"/>
<wire x1="30.48" y1="144.78" x2="30.48" y2="142.24" width="0.1524" layer="91"/>
<junction x="30.48" y="142.24"/>
<pinref part="U1" gate="G$1" pin="GND@2"/>
<wire x1="30.48" y1="144.78" x2="30.48" y2="147.32" width="0.1524" layer="91"/>
<wire x1="30.48" y1="147.32" x2="33.02" y2="147.32" width="0.1524" layer="91"/>
<junction x="30.48" y="144.78"/>
<pinref part="U1" gate="G$1" pin="GND@1"/>
<wire x1="30.48" y1="147.32" x2="30.48" y2="149.86" width="0.1524" layer="91"/>
<wire x1="30.48" y1="149.86" x2="33.02" y2="149.86" width="0.1524" layer="91"/>
<junction x="30.48" y="147.32"/>
<wire x1="30.48" y1="149.86" x2="22.86" y2="149.86" width="0.1524" layer="91"/>
<junction x="30.48" y="149.86"/>
<pinref part="GND3" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="154.94" y1="172.72" x2="154.94" y2="170.18" width="0.1524" layer="91"/>
<pinref part="GND8" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="223.52" y1="279.4" x2="213.36" y2="279.4" width="0.1524" layer="91"/>
<label x="213.36" y="279.4" size="1.778" layer="95"/>
<pinref part="GND14" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="T1" gate="A" pin="S"/>
<wire x1="185.42" y1="134.62" x2="185.42" y2="132.08" width="0.1524" layer="91"/>
<pinref part="GND10" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="T3" gate="A" pin="S"/>
<wire x1="185.42" y1="83.82" x2="185.42" y2="81.28" width="0.1524" layer="91"/>
<pinref part="GND12" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="T4" gate="A" pin="S"/>
<wire x1="185.42" y1="58.42" x2="185.42" y2="55.88" width="0.1524" layer="91"/>
<pinref part="GND13" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="GND"/>
<wire x1="165.1" y1="35.56" x2="165.1" y2="33.02" width="0.1524" layer="91"/>
<pinref part="GND9" gate="1" pin="GND"/>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="165.1" y1="33.02" x2="165.1" y2="30.48" width="0.1524" layer="91"/>
<wire x1="165.1" y1="33.02" x2="180.34" y2="33.02" width="0.1524" layer="91"/>
<junction x="165.1" y="33.02"/>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="180.34" y1="33.02" x2="187.96" y2="33.02" width="0.1524" layer="91"/>
<junction x="180.34" y="33.02"/>
</segment>
<segment>
<wire x1="254" y1="71.12" x2="256.54" y2="71.12" width="0.1524" layer="91"/>
<wire x1="256.54" y1="71.12" x2="256.54" y2="68.58" width="0.1524" layer="91"/>
<wire x1="256.54" y1="68.58" x2="256.54" y2="66.04" width="0.1524" layer="91"/>
<wire x1="256.54" y1="66.04" x2="256.54" y2="63.5" width="0.1524" layer="91"/>
<wire x1="256.54" y1="63.5" x2="254" y2="63.5" width="0.1524" layer="91"/>
<wire x1="254" y1="66.04" x2="256.54" y2="66.04" width="0.1524" layer="91"/>
<junction x="256.54" y="66.04"/>
<wire x1="254" y1="68.58" x2="256.54" y2="68.58" width="0.1524" layer="91"/>
<junction x="256.54" y="68.58"/>
<wire x1="256.54" y1="71.12" x2="256.54" y2="78.74" width="0.1524" layer="91"/>
<junction x="256.54" y="71.12"/>
<pinref part="GND15" gate="1" pin="GND"/>
<pinref part="J11" gate="G$1" pin="8"/>
<pinref part="J11" gate="G$1" pin="6"/>
<pinref part="J11" gate="G$1" pin="4"/>
<pinref part="J11" gate="G$1" pin="2"/>
<wire x1="256.54" y1="63.5" x2="256.54" y2="60.96" width="0.1524" layer="91"/>
<junction x="256.54" y="63.5"/>
<pinref part="J11" gate="G$1" pin="10"/>
<wire x1="256.54" y1="60.96" x2="254" y2="60.96" width="0.1524" layer="91"/>
<wire x1="256.54" y1="60.96" x2="256.54" y2="58.42" width="0.1524" layer="91"/>
<junction x="256.54" y="60.96"/>
<pinref part="J11" gate="G$1" pin="12"/>
<wire x1="256.54" y1="58.42" x2="254" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="T2" gate="A" pin="S"/>
<wire x1="185.42" y1="109.22" x2="185.42" y2="106.68" width="0.1524" layer="91"/>
<pinref part="GND11" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="4"/>
<wire x1="63.5" y1="251.46" x2="45.72" y2="251.46" width="0.1524" layer="91"/>
<wire x1="45.72" y1="251.46" x2="45.72" y2="254" width="0.1524" layer="91"/>
<pinref part="J4" gate="G$1" pin="3"/>
<wire x1="45.72" y1="254" x2="63.5" y2="254" width="0.1524" layer="91"/>
<wire x1="45.72" y1="251.46" x2="38.1" y2="251.46" width="0.1524" layer="91"/>
<junction x="45.72" y="251.46"/>
<pinref part="C8" gate="G$1" pin="2"/>
<pinref part="GND4" gate="1" pin="GND"/>
<junction x="38.1" y="251.46"/>
</segment>
<segment>
<pinref part="J5" gate="G$1" pin="4"/>
<wire x1="63.5" y1="220.98" x2="45.72" y2="220.98" width="0.1524" layer="91"/>
<wire x1="45.72" y1="220.98" x2="45.72" y2="223.52" width="0.1524" layer="91"/>
<pinref part="J5" gate="G$1" pin="3"/>
<wire x1="45.72" y1="223.52" x2="63.5" y2="223.52" width="0.1524" layer="91"/>
<wire x1="45.72" y1="220.98" x2="38.1" y2="220.98" width="0.1524" layer="91"/>
<junction x="45.72" y="220.98"/>
<pinref part="C9" gate="G$1" pin="2"/>
<pinref part="GND5" gate="1" pin="GND"/>
<junction x="38.1" y="220.98"/>
</segment>
<segment>
<pinref part="J14" gate="G$1" pin="20"/>
<wire x1="287.02" y1="58.42" x2="289.56" y2="58.42" width="0.1524" layer="91"/>
<wire x1="289.56" y1="58.42" x2="289.56" y2="60.96" width="0.1524" layer="91"/>
<pinref part="J14" gate="G$1" pin="2"/>
<wire x1="289.56" y1="60.96" x2="289.56" y2="63.5" width="0.1524" layer="91"/>
<wire x1="289.56" y1="63.5" x2="289.56" y2="66.04" width="0.1524" layer="91"/>
<wire x1="289.56" y1="66.04" x2="289.56" y2="68.58" width="0.1524" layer="91"/>
<wire x1="289.56" y1="68.58" x2="289.56" y2="71.12" width="0.1524" layer="91"/>
<wire x1="289.56" y1="71.12" x2="289.56" y2="73.66" width="0.1524" layer="91"/>
<wire x1="289.56" y1="73.66" x2="289.56" y2="76.2" width="0.1524" layer="91"/>
<wire x1="289.56" y1="76.2" x2="289.56" y2="78.74" width="0.1524" layer="91"/>
<wire x1="289.56" y1="78.74" x2="289.56" y2="81.28" width="0.1524" layer="91"/>
<wire x1="289.56" y1="81.28" x2="289.56" y2="88.9" width="0.1524" layer="91"/>
<wire x1="287.02" y1="81.28" x2="289.56" y2="81.28" width="0.1524" layer="91"/>
<junction x="289.56" y="81.28"/>
<pinref part="J14" gate="G$1" pin="4"/>
<wire x1="287.02" y1="78.74" x2="289.56" y2="78.74" width="0.1524" layer="91"/>
<junction x="289.56" y="78.74"/>
<pinref part="J14" gate="G$1" pin="6"/>
<wire x1="287.02" y1="76.2" x2="289.56" y2="76.2" width="0.1524" layer="91"/>
<junction x="289.56" y="76.2"/>
<pinref part="J14" gate="G$1" pin="8"/>
<wire x1="287.02" y1="73.66" x2="289.56" y2="73.66" width="0.1524" layer="91"/>
<junction x="289.56" y="73.66"/>
<pinref part="J14" gate="G$1" pin="10"/>
<wire x1="287.02" y1="71.12" x2="289.56" y2="71.12" width="0.1524" layer="91"/>
<junction x="289.56" y="71.12"/>
<pinref part="J14" gate="G$1" pin="12"/>
<wire x1="287.02" y1="68.58" x2="289.56" y2="68.58" width="0.1524" layer="91"/>
<junction x="289.56" y="68.58"/>
<pinref part="J14" gate="G$1" pin="14"/>
<wire x1="287.02" y1="66.04" x2="289.56" y2="66.04" width="0.1524" layer="91"/>
<junction x="289.56" y="66.04"/>
<pinref part="J14" gate="G$1" pin="16"/>
<wire x1="287.02" y1="63.5" x2="289.56" y2="63.5" width="0.1524" layer="91"/>
<junction x="289.56" y="63.5"/>
<pinref part="J14" gate="G$1" pin="18"/>
<wire x1="287.02" y1="60.96" x2="289.56" y2="60.96" width="0.1524" layer="91"/>
<junction x="289.56" y="60.96"/>
<pinref part="GND17" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="S2" gate="G$1" pin="S"/>
<wire x1="243.84" y1="226.06" x2="256.54" y2="226.06" width="0.1524" layer="91"/>
<pinref part="GND16" gate="1" pin="GND"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="VCC"/>
<wire x1="43.18" y1="35.56" x2="38.1" y2="35.56" width="0.1524" layer="91"/>
<wire x1="38.1" y1="35.56" x2="38.1" y2="38.1" width="0.1524" layer="91"/>
<wire x1="38.1" y1="38.1" x2="38.1" y2="40.64" width="0.1524" layer="91"/>
<wire x1="38.1" y1="40.64" x2="93.98" y2="40.64" width="0.1524" layer="91"/>
<wire x1="93.98" y1="40.64" x2="93.98" y2="35.56" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="+3V34" gate="G$1" pin="+3V3"/>
<wire x1="25.4" y1="38.1" x2="38.1" y2="38.1" width="0.1524" layer="91"/>
<junction x="38.1" y="38.1"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="1"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="-27.94" y1="187.96" x2="-17.78" y2="187.96" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="-17.78" y1="187.96" x2="-7.62" y2="187.96" width="0.1524" layer="91"/>
<junction x="-17.78" y="187.96"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="187.96" x2="2.54" y2="187.96" width="0.1524" layer="91"/>
<junction x="-7.62" y="187.96"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="2.54" y1="187.96" x2="12.7" y2="187.96" width="0.1524" layer="91"/>
<junction x="2.54" y="187.96"/>
<pinref part="U1" gate="G$1" pin="VDDIO@1"/>
<wire x1="12.7" y1="187.96" x2="22.86" y2="187.96" width="0.1524" layer="91"/>
<junction x="12.7" y="187.96"/>
<wire x1="22.86" y1="187.96" x2="30.48" y2="187.96" width="0.1524" layer="91"/>
<wire x1="30.48" y1="187.96" x2="33.02" y2="187.96" width="0.1524" layer="91"/>
<wire x1="30.48" y1="187.96" x2="30.48" y2="185.42" width="0.1524" layer="91"/>
<junction x="30.48" y="187.96"/>
<pinref part="U1" gate="G$1" pin="VDDIO@2"/>
<wire x1="30.48" y1="185.42" x2="33.02" y2="185.42" width="0.1524" layer="91"/>
<wire x1="30.48" y1="185.42" x2="30.48" y2="182.88" width="0.1524" layer="91"/>
<junction x="30.48" y="185.42"/>
<pinref part="U1" gate="G$1" pin="VDDIO@3"/>
<wire x1="30.48" y1="182.88" x2="33.02" y2="182.88" width="0.1524" layer="91"/>
<wire x1="30.48" y1="182.88" x2="30.48" y2="180.34" width="0.1524" layer="91"/>
<junction x="30.48" y="182.88"/>
<pinref part="U1" gate="G$1" pin="VDDIO@4"/>
<wire x1="30.48" y1="180.34" x2="33.02" y2="180.34" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VDDANA"/>
<wire x1="33.02" y1="175.26" x2="30.48" y2="175.26" width="0.1524" layer="91"/>
<wire x1="30.48" y1="175.26" x2="30.48" y2="180.34" width="0.1524" layer="91"/>
<junction x="30.48" y="180.34"/>
<pinref part="+3V33" gate="G$1" pin="+3V3"/>
<wire x1="22.86" y1="193.04" x2="22.86" y2="187.96" width="0.1524" layer="91"/>
<junction x="22.86" y="187.96"/>
</segment>
<segment>
<pinref part="L1" gate="G$1" pin="2"/>
<wire x1="180.34" y1="180.34" x2="185.42" y2="180.34" width="0.1524" layer="91"/>
<pinref part="+3V37" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="1"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="63.5" y1="259.08" x2="38.1" y2="259.08" width="0.1524" layer="91"/>
<wire x1="38.1" y1="259.08" x2="30.48" y2="259.08" width="0.1524" layer="91"/>
<junction x="38.1" y="259.08"/>
<wire x1="30.48" y1="259.08" x2="30.48" y2="246.38" width="0.1524" layer="91"/>
<pinref part="J4" gate="G$1" pin="6"/>
<wire x1="30.48" y1="246.38" x2="63.5" y2="246.38" width="0.1524" layer="91"/>
<pinref part="+3V31" gate="G$1" pin="+3V3"/>
<wire x1="25.4" y1="259.08" x2="30.48" y2="259.08" width="0.1524" layer="91"/>
<junction x="30.48" y="259.08"/>
</segment>
<segment>
<wire x1="124.46" y1="259.08" x2="124.46" y2="251.46" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="124.46" y1="251.46" x2="124.46" y2="246.38" width="0.1524" layer="91"/>
<wire x1="116.84" y1="246.38" x2="124.46" y2="246.38" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="116.84" y1="259.08" x2="124.46" y2="259.08" width="0.1524" layer="91"/>
<pinref part="+3V35" gate="G$1" pin="+3V3"/>
<wire x1="129.54" y1="251.46" x2="124.46" y2="251.46" width="0.1524" layer="91"/>
<junction x="124.46" y="251.46"/>
</segment>
<segment>
<pinref part="J5" gate="G$1" pin="1"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="63.5" y1="228.6" x2="38.1" y2="228.6" width="0.1524" layer="91"/>
<wire x1="38.1" y1="228.6" x2="30.48" y2="228.6" width="0.1524" layer="91"/>
<junction x="38.1" y="228.6"/>
<wire x1="30.48" y1="228.6" x2="30.48" y2="215.9" width="0.1524" layer="91"/>
<pinref part="J5" gate="G$1" pin="6"/>
<wire x1="30.48" y1="215.9" x2="63.5" y2="215.9" width="0.1524" layer="91"/>
<pinref part="+3V32" gate="G$1" pin="+3V3"/>
<wire x1="25.4" y1="228.6" x2="30.48" y2="228.6" width="0.1524" layer="91"/>
<junction x="30.48" y="228.6"/>
</segment>
<segment>
<wire x1="124.46" y1="228.6" x2="124.46" y2="220.98" width="0.1524" layer="91"/>
<pinref part="D4" gate="G$1" pin="A"/>
<wire x1="124.46" y1="220.98" x2="124.46" y2="215.9" width="0.1524" layer="91"/>
<wire x1="116.84" y1="215.9" x2="124.46" y2="215.9" width="0.1524" layer="91"/>
<pinref part="D3" gate="G$1" pin="A"/>
<wire x1="116.84" y1="228.6" x2="124.46" y2="228.6" width="0.1524" layer="91"/>
<pinref part="+3V36" gate="G$1" pin="+3V3"/>
<wire x1="129.54" y1="220.98" x2="124.46" y2="220.98" width="0.1524" layer="91"/>
<junction x="124.46" y="220.98"/>
</segment>
<segment>
<pinref part="D5" gate="G$1" pin="A"/>
<wire x1="246.38" y1="279.4" x2="256.54" y2="279.4" width="0.1524" layer="91"/>
<wire x1="256.54" y1="279.4" x2="256.54" y2="269.24" width="0.1524" layer="91"/>
<wire x1="256.54" y1="269.24" x2="256.54" y2="264.16" width="0.1524" layer="91"/>
<wire x1="256.54" y1="264.16" x2="256.54" y2="259.08" width="0.1524" layer="91"/>
<pinref part="D7" gate="G$1" pin="A"/>
<wire x1="246.38" y1="259.08" x2="256.54" y2="259.08" width="0.1524" layer="91"/>
<pinref part="D6" gate="G$1" pin="A"/>
<wire x1="246.38" y1="269.24" x2="256.54" y2="269.24" width="0.1524" layer="91"/>
<junction x="256.54" y="269.24"/>
<wire x1="256.54" y1="264.16" x2="261.62" y2="264.16" width="0.1524" layer="91"/>
<junction x="256.54" y="264.16"/>
<pinref part="+3V38" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="J14" gate="G$1" pin="19"/>
<wire x1="271.78" y1="58.42" x2="269.24" y2="58.42" width="0.1524" layer="91"/>
<wire x1="269.24" y1="58.42" x2="269.24" y2="60.96" width="0.1524" layer="91"/>
<pinref part="J14" gate="G$1" pin="1"/>
<wire x1="269.24" y1="60.96" x2="269.24" y2="63.5" width="0.1524" layer="91"/>
<wire x1="269.24" y1="63.5" x2="269.24" y2="66.04" width="0.1524" layer="91"/>
<wire x1="269.24" y1="66.04" x2="269.24" y2="68.58" width="0.1524" layer="91"/>
<wire x1="269.24" y1="68.58" x2="269.24" y2="71.12" width="0.1524" layer="91"/>
<wire x1="269.24" y1="71.12" x2="269.24" y2="73.66" width="0.1524" layer="91"/>
<wire x1="269.24" y1="73.66" x2="269.24" y2="76.2" width="0.1524" layer="91"/>
<wire x1="269.24" y1="76.2" x2="269.24" y2="78.74" width="0.1524" layer="91"/>
<wire x1="269.24" y1="78.74" x2="269.24" y2="81.28" width="0.1524" layer="91"/>
<wire x1="269.24" y1="81.28" x2="269.24" y2="88.9" width="0.1524" layer="91"/>
<wire x1="271.78" y1="81.28" x2="269.24" y2="81.28" width="0.1524" layer="91"/>
<junction x="269.24" y="81.28"/>
<pinref part="J14" gate="G$1" pin="3"/>
<wire x1="271.78" y1="78.74" x2="269.24" y2="78.74" width="0.1524" layer="91"/>
<junction x="269.24" y="78.74"/>
<pinref part="J14" gate="G$1" pin="5"/>
<wire x1="271.78" y1="76.2" x2="269.24" y2="76.2" width="0.1524" layer="91"/>
<junction x="269.24" y="76.2"/>
<pinref part="J14" gate="G$1" pin="7"/>
<wire x1="271.78" y1="73.66" x2="269.24" y2="73.66" width="0.1524" layer="91"/>
<junction x="269.24" y="73.66"/>
<pinref part="J14" gate="G$1" pin="9"/>
<wire x1="271.78" y1="71.12" x2="269.24" y2="71.12" width="0.1524" layer="91"/>
<junction x="269.24" y="71.12"/>
<pinref part="J14" gate="G$1" pin="11"/>
<wire x1="271.78" y1="68.58" x2="269.24" y2="68.58" width="0.1524" layer="91"/>
<junction x="269.24" y="68.58"/>
<pinref part="J14" gate="G$1" pin="13"/>
<wire x1="271.78" y1="66.04" x2="269.24" y2="66.04" width="0.1524" layer="91"/>
<junction x="269.24" y="66.04"/>
<pinref part="J14" gate="G$1" pin="15"/>
<wire x1="271.78" y1="63.5" x2="269.24" y2="63.5" width="0.1524" layer="91"/>
<junction x="269.24" y="63.5"/>
<pinref part="J14" gate="G$1" pin="17"/>
<wire x1="271.78" y1="60.96" x2="269.24" y2="60.96" width="0.1524" layer="91"/>
<junction x="269.24" y="60.96"/>
<pinref part="+3V39" gate="G$1" pin="+3V3"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="PWRPAD"/>
<wire x1="53.34" y1="284.48" x2="60.96" y2="284.48" width="0.1524" layer="91"/>
<pinref part="P+1" gate="VCC" pin="VCC"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="60.96" y1="284.48" x2="68.58" y2="284.48" width="0.1524" layer="91"/>
<junction x="60.96" y="284.48"/>
<pinref part="C11" gate="G$1" pin="1"/>
<junction x="68.58" y="284.48"/>
<wire x1="68.58" y1="284.48" x2="88.9" y2="284.48" width="0.1524" layer="91"/>
<wire x1="88.9" y1="284.48" x2="109.22" y2="284.48" width="0.1524" layer="91"/>
<wire x1="109.22" y1="284.48" x2="121.92" y2="284.48" width="0.1524" layer="91"/>
<wire x1="91.44" y1="292.1" x2="88.9" y2="292.1" width="0.1524" layer="91"/>
<wire x1="88.9" y1="292.1" x2="88.9" y2="284.48" width="0.1524" layer="91"/>
<junction x="88.9" y="284.48"/>
<wire x1="106.68" y1="292.1" x2="109.22" y2="292.1" width="0.1524" layer="91"/>
<wire x1="109.22" y1="292.1" x2="109.22" y2="284.48" width="0.1524" layer="91"/>
<junction x="109.22" y="284.48"/>
<wire x1="106.68" y1="294.64" x2="109.22" y2="294.64" width="0.1524" layer="91"/>
<wire x1="109.22" y1="294.64" x2="109.22" y2="292.1" width="0.1524" layer="91"/>
<junction x="109.22" y="292.1"/>
<wire x1="106.68" y1="297.18" x2="109.22" y2="297.18" width="0.1524" layer="91"/>
<wire x1="109.22" y1="297.18" x2="109.22" y2="294.64" width="0.1524" layer="91"/>
<junction x="109.22" y="294.64"/>
<wire x1="106.68" y1="299.72" x2="109.22" y2="299.72" width="0.1524" layer="91"/>
<wire x1="109.22" y1="299.72" x2="109.22" y2="297.18" width="0.1524" layer="91"/>
<junction x="109.22" y="297.18"/>
<wire x1="91.44" y1="299.72" x2="88.9" y2="299.72" width="0.1524" layer="91"/>
<wire x1="88.9" y1="299.72" x2="88.9" y2="297.18" width="0.1524" layer="91"/>
<junction x="88.9" y="292.1"/>
<wire x1="88.9" y1="297.18" x2="88.9" y2="294.64" width="0.1524" layer="91"/>
<wire x1="88.9" y1="294.64" x2="88.9" y2="292.1" width="0.1524" layer="91"/>
<wire x1="91.44" y1="297.18" x2="88.9" y2="297.18" width="0.1524" layer="91"/>
<junction x="88.9" y="297.18"/>
<wire x1="91.44" y1="294.64" x2="88.9" y2="294.64" width="0.1524" layer="91"/>
<junction x="88.9" y="294.64"/>
<pinref part="J6" gate="G$1" pin="1"/>
<pinref part="J6" gate="G$1" pin="2"/>
<pinref part="J6" gate="G$1" pin="3"/>
<pinref part="J6" gate="G$1" pin="4"/>
<pinref part="J6" gate="G$1" pin="5"/>
<pinref part="J6" gate="G$1" pin="6"/>
<pinref part="J6" gate="G$1" pin="7"/>
<pinref part="J6" gate="G$1" pin="8"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="IN"/>
<wire x1="157.48" y1="40.64" x2="149.86" y2="40.64" width="0.1524" layer="91"/>
<pinref part="P+2" gate="VCC" pin="VCC"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="VDDCORE"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="33.02" y1="170.18" x2="12.7" y2="170.18" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="12.7" y1="170.18" x2="2.54" y2="170.18" width="0.1524" layer="91"/>
<junction x="12.7" y="170.18"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA03/ANAREF-VREFA/ADC0-1"/>
<wire x1="111.76" y1="180.34" x2="154.94" y2="180.34" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="154.94" y1="180.34" x2="165.1" y2="180.34" width="0.1524" layer="91"/>
<junction x="154.94" y="180.34"/>
<pinref part="L1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="UP0RX" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA01/XOUT32/SER1-1/TC2-1"/>
<wire x1="111.76" y1="185.42" x2="124.46" y2="185.42" width="0.1524" layer="91"/>
<label x="114.3" y="185.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="2"/>
<wire x1="63.5" y1="256.54" x2="50.8" y2="256.54" width="0.1524" layer="91"/>
<label x="50.8" y="256.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="UP0TX" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA00/XIN32/SER1-0/TC2-0"/>
<wire x1="111.76" y1="187.96" x2="124.46" y2="187.96" width="0.1524" layer="91"/>
<label x="114.3" y="187.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="5"/>
<wire x1="63.5" y1="248.92" x2="50.8" y2="248.92" width="0.1524" layer="91"/>
<label x="50.8" y="248.92" size="1.778" layer="95"/>
</segment>
</net>
<net name="UP0STLR" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="93.98" y1="259.08" x2="81.28" y2="259.08" width="0.1524" layer="91"/>
<label x="81.28" y="259.08" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA02/ADC0-1/DAC-0"/>
<wire x1="111.76" y1="182.88" x2="124.46" y2="182.88" width="0.1524" layer="91"/>
<label x="114.3" y="182.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="UP0STLB" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="93.98" y1="246.38" x2="81.28" y2="246.38" width="0.1524" layer="91"/>
<label x="81.28" y="246.38" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PB01/ADC0-13/SER5-3/TC7-1"/>
<wire x1="111.76" y1="106.68" x2="124.46" y2="106.68" width="0.1524" layer="91"/>
<label x="114.3" y="106.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="C"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="109.22" y1="259.08" x2="104.14" y2="259.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="C"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="109.22" y1="246.38" x2="104.14" y2="246.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="C"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="109.22" y1="228.6" x2="104.14" y2="228.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="D4" gate="G$1" pin="C"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="109.22" y1="215.9" x2="104.14" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="2"/>
<pinref part="D5" gate="G$1" pin="C"/>
<wire x1="233.68" y1="279.4" x2="238.76" y2="279.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="2"/>
<pinref part="D6" gate="G$1" pin="C"/>
<wire x1="233.68" y1="269.24" x2="238.76" y2="269.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<pinref part="D7" gate="G$1" pin="C"/>
<wire x1="233.68" y1="259.08" x2="238.76" y2="259.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="STL1G" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="223.52" y1="269.24" x2="213.36" y2="269.24" width="0.1524" layer="91"/>
<label x="213.36" y="269.24" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PB23/XOUT1/SER1-3/SER5-3/TC7-1"/>
<wire x1="111.76" y1="60.96" x2="124.46" y2="60.96" width="0.1524" layer="91"/>
<label x="114.3" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="STL2B" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="223.52" y1="259.08" x2="213.36" y2="259.08" width="0.1524" layer="91"/>
<label x="213.36" y="259.08" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PB22/XIN1/SER1-2/SER5-2/PDEC0-2/TC7-0"/>
<wire x1="111.76" y1="63.5" x2="124.46" y2="63.5" width="0.1524" layer="91"/>
<label x="114.3" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="UP1RX" class="0">
<segment>
<pinref part="J5" gate="G$1" pin="2"/>
<wire x1="63.5" y1="226.06" x2="50.8" y2="226.06" width="0.1524" layer="91"/>
<label x="50.8" y="226.06" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PB03/ADC0/SER5-1/TC6"/>
<wire x1="111.76" y1="101.6" x2="124.46" y2="101.6" width="0.1524" layer="91"/>
<label x="114.3" y="101.6" size="1.778" layer="95"/>
</segment>
</net>
<net name="UP1TX" class="0">
<segment>
<pinref part="J5" gate="G$1" pin="5"/>
<wire x1="63.5" y1="218.44" x2="50.8" y2="218.44" width="0.1524" layer="91"/>
<label x="50.8" y="218.44" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PB02/ADC0-14/SER5-0/TC6-0"/>
<wire x1="111.76" y1="104.14" x2="124.46" y2="104.14" width="0.1524" layer="91"/>
<label x="114.3" y="104.14" size="1.778" layer="95"/>
</segment>
</net>
<net name="UP1STLR" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="93.98" y1="228.6" x2="81.28" y2="228.6" width="0.1524" layer="91"/>
<label x="81.28" y="228.6" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PB00/ADC0-12/SER5-2/TC7-0"/>
<wire x1="111.76" y1="109.22" x2="124.46" y2="109.22" width="0.1524" layer="91"/>
<label x="114.3" y="109.22" size="1.778" layer="95"/>
</segment>
</net>
<net name="UP1STLB" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="93.98" y1="215.9" x2="81.28" y2="215.9" width="0.1524" layer="91"/>
<label x="81.28" y="215.9" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PB31/SER7-1/SER5-0/TC0-1"/>
<wire x1="111.76" y1="55.88" x2="124.46" y2="55.88" width="0.1524" layer="91"/>
<label x="114.3" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<pinref part="T1" gate="A" pin="G"/>
<wire x1="175.26" y1="137.16" x2="180.34" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BF2IN" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="165.1" y1="111.76" x2="152.4" y2="111.76" width="0.1524" layer="91"/>
<label x="152.4" y="111.76" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA22/SER3-0/SER5-1/TC4-0"/>
<wire x1="111.76" y1="132.08" x2="132.08" y2="132.08" width="0.1524" layer="91"/>
<label x="114.3" y="132.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="LF1DRAIN" class="0">
<segment>
<pinref part="T3" gate="A" pin="D"/>
<wire x1="185.42" y1="93.98" x2="185.42" y2="96.52" width="0.1524" layer="91"/>
<wire x1="185.42" y1="96.52" x2="198.12" y2="96.52" width="0.1524" layer="91"/>
<label x="187.96" y="96.52" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="256.54" y1="187.96" x2="243.84" y2="187.96" width="0.1524" layer="91"/>
<label x="243.84" y="187.96" size="1.778" layer="95"/>
<pinref part="J12" gate="G$1" pin="2"/>
</segment>
</net>
<net name="LF2DRAIN" class="0">
<segment>
<pinref part="T4" gate="A" pin="D"/>
<wire x1="185.42" y1="68.58" x2="185.42" y2="71.12" width="0.1524" layer="91"/>
<wire x1="185.42" y1="71.12" x2="198.12" y2="71.12" width="0.1524" layer="91"/>
<label x="187.96" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="256.54" y1="185.42" x2="243.84" y2="185.42" width="0.1524" layer="91"/>
<label x="243.84" y="185.42" size="1.778" layer="95"/>
<pinref part="J12" gate="G$1" pin="3"/>
</segment>
</net>
<net name="LF1IN" class="0">
<segment>
<pinref part="T3" gate="A" pin="G"/>
<wire x1="180.34" y1="86.36" x2="152.4" y2="86.36" width="0.1524" layer="91"/>
<label x="152.4" y="86.36" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA21/SER5-3/SER3-3/TC7-1"/>
<wire x1="111.76" y1="134.62" x2="132.08" y2="134.62" width="0.1524" layer="91"/>
<label x="114.3" y="134.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="LF2IN" class="0">
<segment>
<pinref part="T4" gate="A" pin="G"/>
<wire x1="180.34" y1="60.96" x2="152.4" y2="60.96" width="0.1524" layer="91"/>
<label x="152.4" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA20/SER5-2/SER3-2/TC7-0"/>
<wire x1="111.76" y1="137.16" x2="132.08" y2="137.16" width="0.1524" layer="91"/>
<label x="114.3" y="137.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="OUT1"/>
<wire x1="172.72" y1="40.64" x2="175.26" y2="40.64" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="OUT2"/>
<wire x1="175.26" y1="40.64" x2="180.34" y2="40.64" width="0.1524" layer="91"/>
<wire x1="180.34" y1="40.64" x2="187.96" y2="40.64" width="0.1524" layer="91"/>
<wire x1="187.96" y1="40.64" x2="198.12" y2="40.64" width="0.1524" layer="91"/>
<wire x1="172.72" y1="38.1" x2="175.26" y2="38.1" width="0.1524" layer="91"/>
<wire x1="175.26" y1="38.1" x2="175.26" y2="40.64" width="0.1524" layer="91"/>
<junction x="175.26" y="40.64"/>
<pinref part="C14" gate="G$1" pin="1"/>
<junction x="187.96" y="40.64"/>
<pinref part="C13" gate="G$1" pin="1"/>
<junction x="180.34" y="40.64"/>
<pinref part="P+3" gate="1" pin="+5V"/>
</segment>
<segment>
<wire x1="238.76" y1="71.12" x2="236.22" y2="71.12" width="0.1524" layer="91"/>
<wire x1="236.22" y1="71.12" x2="236.22" y2="68.58" width="0.1524" layer="91"/>
<wire x1="236.22" y1="68.58" x2="236.22" y2="66.04" width="0.1524" layer="91"/>
<wire x1="236.22" y1="66.04" x2="236.22" y2="63.5" width="0.1524" layer="91"/>
<wire x1="236.22" y1="63.5" x2="238.76" y2="63.5" width="0.1524" layer="91"/>
<wire x1="238.76" y1="66.04" x2="236.22" y2="66.04" width="0.1524" layer="91"/>
<junction x="236.22" y="66.04"/>
<wire x1="238.76" y1="68.58" x2="236.22" y2="68.58" width="0.1524" layer="91"/>
<junction x="236.22" y="68.58"/>
<wire x1="236.22" y1="71.12" x2="236.22" y2="78.74" width="0.1524" layer="91"/>
<junction x="236.22" y="71.12"/>
<pinref part="P+4" gate="1" pin="+5V"/>
<pinref part="J11" gate="G$1" pin="7"/>
<pinref part="J11" gate="G$1" pin="5"/>
<pinref part="J11" gate="G$1" pin="3"/>
<pinref part="J11" gate="G$1" pin="1"/>
<wire x1="236.22" y1="63.5" x2="236.22" y2="60.96" width="0.1524" layer="91"/>
<junction x="236.22" y="63.5"/>
<pinref part="J11" gate="G$1" pin="9"/>
<wire x1="236.22" y1="60.96" x2="238.76" y2="60.96" width="0.1524" layer="91"/>
<wire x1="236.22" y1="60.96" x2="236.22" y2="58.42" width="0.1524" layer="91"/>
<junction x="236.22" y="60.96"/>
<pinref part="J11" gate="G$1" pin="11"/>
<wire x1="236.22" y1="58.42" x2="238.76" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="2"/>
<pinref part="T2" gate="A" pin="G"/>
<wire x1="175.26" y1="111.76" x2="180.34" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BF1DRAIN" class="0">
<segment>
<pinref part="T1" gate="A" pin="D"/>
<wire x1="185.42" y1="144.78" x2="185.42" y2="147.32" width="0.1524" layer="91"/>
<label x="187.96" y="147.32" size="1.778" layer="95"/>
<wire x1="185.42" y1="147.32" x2="205.74" y2="147.32" width="0.1524" layer="91"/>
<wire x1="205.74" y1="147.32" x2="210.82" y2="147.32" width="0.1524" layer="91"/>
<wire x1="210.82" y1="147.32" x2="210.82" y2="144.78" width="0.1524" layer="91"/>
<wire x1="210.82" y1="147.32" x2="213.36" y2="147.32" width="0.1524" layer="91"/>
<wire x1="213.36" y1="147.32" x2="213.36" y2="144.78" width="0.1524" layer="91"/>
<junction x="210.82" y="147.32"/>
<wire x1="213.36" y1="147.32" x2="215.9" y2="147.32" width="0.1524" layer="91"/>
<wire x1="215.9" y1="147.32" x2="215.9" y2="144.78" width="0.1524" layer="91"/>
<junction x="213.36" y="147.32"/>
<wire x1="215.9" y1="147.32" x2="218.44" y2="147.32" width="0.1524" layer="91"/>
<wire x1="218.44" y1="147.32" x2="218.44" y2="144.78" width="0.1524" layer="91"/>
<junction x="215.9" y="147.32"/>
<wire x1="205.74" y1="147.32" x2="205.74" y2="127" width="0.1524" layer="91"/>
<junction x="205.74" y="147.32"/>
<wire x1="205.74" y1="127" x2="210.82" y2="127" width="0.1524" layer="91"/>
<wire x1="210.82" y1="127" x2="210.82" y2="129.54" width="0.1524" layer="91"/>
<wire x1="213.36" y1="129.54" x2="213.36" y2="127" width="0.1524" layer="91"/>
<wire x1="213.36" y1="127" x2="210.82" y2="127" width="0.1524" layer="91"/>
<junction x="210.82" y="127"/>
<wire x1="215.9" y1="129.54" x2="215.9" y2="127" width="0.1524" layer="91"/>
<wire x1="215.9" y1="127" x2="213.36" y2="127" width="0.1524" layer="91"/>
<junction x="213.36" y="127"/>
<junction x="215.9" y="127"/>
<wire x1="218.44" y1="127" x2="215.9" y2="127" width="0.1524" layer="91"/>
<wire x1="218.44" y1="129.54" x2="218.44" y2="127" width="0.1524" layer="91"/>
<pinref part="J7" gate="G$1" pin="1"/>
<pinref part="J7" gate="G$1" pin="2"/>
<pinref part="J7" gate="G$1" pin="3"/>
<pinref part="J7" gate="G$1" pin="4"/>
<pinref part="J7" gate="G$1" pin="5"/>
<pinref part="J7" gate="G$1" pin="6"/>
<pinref part="J7" gate="G$1" pin="7"/>
<pinref part="J7" gate="G$1" pin="8"/>
</segment>
</net>
<net name="BF2DRAIN" class="0">
<segment>
<pinref part="T2" gate="A" pin="D"/>
<wire x1="185.42" y1="119.38" x2="185.42" y2="121.92" width="0.1524" layer="91"/>
<label x="187.96" y="121.92" size="1.778" layer="95"/>
<wire x1="185.42" y1="121.92" x2="205.74" y2="121.92" width="0.1524" layer="91"/>
<wire x1="205.74" y1="121.92" x2="210.82" y2="121.92" width="0.1524" layer="91"/>
<wire x1="210.82" y1="121.92" x2="210.82" y2="119.38" width="0.1524" layer="91"/>
<wire x1="210.82" y1="121.92" x2="213.36" y2="121.92" width="0.1524" layer="91"/>
<wire x1="213.36" y1="121.92" x2="213.36" y2="119.38" width="0.1524" layer="91"/>
<junction x="210.82" y="121.92"/>
<wire x1="213.36" y1="121.92" x2="215.9" y2="121.92" width="0.1524" layer="91"/>
<wire x1="215.9" y1="121.92" x2="215.9" y2="119.38" width="0.1524" layer="91"/>
<junction x="213.36" y="121.92"/>
<wire x1="215.9" y1="121.92" x2="218.44" y2="121.92" width="0.1524" layer="91"/>
<wire x1="218.44" y1="121.92" x2="218.44" y2="119.38" width="0.1524" layer="91"/>
<junction x="215.9" y="121.92"/>
<wire x1="205.74" y1="121.92" x2="205.74" y2="101.6" width="0.1524" layer="91"/>
<junction x="205.74" y="121.92"/>
<wire x1="205.74" y1="101.6" x2="210.82" y2="101.6" width="0.1524" layer="91"/>
<wire x1="210.82" y1="101.6" x2="210.82" y2="104.14" width="0.1524" layer="91"/>
<wire x1="210.82" y1="101.6" x2="213.36" y2="101.6" width="0.1524" layer="91"/>
<wire x1="213.36" y1="101.6" x2="213.36" y2="104.14" width="0.1524" layer="91"/>
<junction x="210.82" y="101.6"/>
<wire x1="213.36" y1="101.6" x2="215.9" y2="101.6" width="0.1524" layer="91"/>
<wire x1="215.9" y1="101.6" x2="215.9" y2="104.14" width="0.1524" layer="91"/>
<junction x="213.36" y="101.6"/>
<wire x1="215.9" y1="101.6" x2="218.44" y2="101.6" width="0.1524" layer="91"/>
<wire x1="218.44" y1="101.6" x2="218.44" y2="104.14" width="0.1524" layer="91"/>
<junction x="215.9" y="101.6"/>
<pinref part="J8" gate="G$1" pin="1"/>
<pinref part="J8" gate="G$1" pin="2"/>
<pinref part="J8" gate="G$1" pin="3"/>
<pinref part="J8" gate="G$1" pin="4"/>
<pinref part="J8" gate="G$1" pin="5"/>
<pinref part="J8" gate="G$1" pin="6"/>
<pinref part="J8" gate="G$1" pin="7"/>
<pinref part="J8" gate="G$1" pin="8"/>
</segment>
</net>
<net name="BF1IN" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="165.1" y1="137.16" x2="152.4" y2="137.16" width="0.1524" layer="91"/>
<label x="152.4" y="137.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA23/SER3-1/SER5-0/TC4-1"/>
<wire x1="111.76" y1="129.54" x2="132.08" y2="129.54" width="0.1524" layer="91"/>
<label x="114.3" y="129.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<wire x1="269.24" y1="187.96" x2="281.94" y2="187.96" width="0.1524" layer="91"/>
<pinref part="J13" gate="G$1" pin="2"/>
<pinref part="J15" gate="G$1" pin="2"/>
<wire x1="281.94" y1="187.96" x2="294.64" y2="187.96" width="0.1524" layer="91"/>
<junction x="281.94" y="187.96"/>
<pinref part="J16" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<wire x1="269.24" y1="185.42" x2="281.94" y2="185.42" width="0.1524" layer="91"/>
<pinref part="J13" gate="G$1" pin="3"/>
<pinref part="J15" gate="G$1" pin="3"/>
<wire x1="281.94" y1="185.42" x2="294.64" y2="185.42" width="0.1524" layer="91"/>
<junction x="281.94" y="185.42"/>
<pinref part="J16" gate="G$1" pin="3"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<wire x1="294.64" y1="182.88" x2="281.94" y2="182.88" width="0.1524" layer="91"/>
<pinref part="J13" gate="G$1" pin="4"/>
<pinref part="J15" gate="G$1" pin="4"/>
<wire x1="281.94" y1="182.88" x2="269.24" y2="182.88" width="0.1524" layer="91"/>
<junction x="281.94" y="182.88"/>
<pinref part="J16" gate="G$1" pin="4"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<wire x1="269.24" y1="180.34" x2="281.94" y2="180.34" width="0.1524" layer="91"/>
<pinref part="J13" gate="G$1" pin="5"/>
<pinref part="J15" gate="G$1" pin="5"/>
<wire x1="281.94" y1="180.34" x2="294.64" y2="180.34" width="0.1524" layer="91"/>
<junction x="281.94" y="180.34"/>
<pinref part="J16" gate="G$1" pin="5"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<wire x1="294.64" y1="177.8" x2="281.94" y2="177.8" width="0.1524" layer="91"/>
<pinref part="J13" gate="G$1" pin="6"/>
<pinref part="J15" gate="G$1" pin="6"/>
<wire x1="281.94" y1="177.8" x2="269.24" y2="177.8" width="0.1524" layer="91"/>
<junction x="281.94" y="177.8"/>
<pinref part="J16" gate="G$1" pin="6"/>
</segment>
</net>
<net name="BT1" class="0">
<segment>
<pinref part="S2" gate="G$1" pin="P"/>
<wire x1="233.68" y1="226.06" x2="213.36" y2="226.06" width="0.1524" layer="91"/>
<label x="213.36" y="226.06" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA27/GCLK-1"/>
<wire x1="111.76" y1="121.92" x2="124.46" y2="121.92" width="0.1524" layer="91"/>
<label x="114.3" y="121.92" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="J13" gate="G$1" pin="7"/>
<pinref part="J15" gate="G$1" pin="7"/>
<wire x1="269.24" y1="175.26" x2="281.94" y2="175.26" width="0.1524" layer="91"/>
<pinref part="J16" gate="G$1" pin="7"/>
<wire x1="281.94" y1="175.26" x2="294.64" y2="175.26" width="0.1524" layer="91"/>
<junction x="281.94" y="175.26"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="J16" gate="G$1" pin="8"/>
<pinref part="J15" gate="G$1" pin="8"/>
<wire x1="294.64" y1="172.72" x2="281.94" y2="172.72" width="0.1524" layer="91"/>
<pinref part="J13" gate="G$1" pin="8"/>
<wire x1="281.94" y1="172.72" x2="269.24" y2="172.72" width="0.1524" layer="91"/>
<junction x="281.94" y="172.72"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="J13" gate="G$1" pin="9"/>
<pinref part="J15" gate="G$1" pin="9"/>
<wire x1="269.24" y1="170.18" x2="281.94" y2="170.18" width="0.1524" layer="91"/>
<pinref part="J16" gate="G$1" pin="9"/>
<wire x1="281.94" y1="170.18" x2="294.64" y2="170.18" width="0.1524" layer="91"/>
<junction x="281.94" y="170.18"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="J16" gate="G$1" pin="10"/>
<pinref part="J15" gate="G$1" pin="10"/>
<wire x1="294.64" y1="167.64" x2="281.94" y2="167.64" width="0.1524" layer="91"/>
<pinref part="J13" gate="G$1" pin="10"/>
<wire x1="281.94" y1="167.64" x2="269.24" y2="167.64" width="0.1524" layer="91"/>
<junction x="281.94" y="167.64"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="J13" gate="G$1" pin="11"/>
<pinref part="J15" gate="G$1" pin="11"/>
<wire x1="269.24" y1="165.1" x2="281.94" y2="165.1" width="0.1524" layer="91"/>
<pinref part="J16" gate="G$1" pin="11"/>
<wire x1="281.94" y1="165.1" x2="294.64" y2="165.1" width="0.1524" layer="91"/>
<junction x="281.94" y="165.1"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="J16" gate="G$1" pin="12"/>
<pinref part="J15" gate="G$1" pin="12"/>
<wire x1="294.64" y1="162.56" x2="281.94" y2="162.56" width="0.1524" layer="91"/>
<pinref part="J13" gate="G$1" pin="12"/>
<wire x1="281.94" y1="162.56" x2="269.24" y2="162.56" width="0.1524" layer="91"/>
<junction x="281.94" y="162.56"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="J13" gate="G$1" pin="13"/>
<pinref part="J15" gate="G$1" pin="13"/>
<wire x1="269.24" y1="160.02" x2="281.94" y2="160.02" width="0.1524" layer="91"/>
<pinref part="J16" gate="G$1" pin="13"/>
<wire x1="281.94" y1="160.02" x2="294.64" y2="160.02" width="0.1524" layer="91"/>
<junction x="281.94" y="160.02"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="J16" gate="G$1" pin="14"/>
<pinref part="J15" gate="G$1" pin="14"/>
<wire x1="294.64" y1="157.48" x2="281.94" y2="157.48" width="0.1524" layer="91"/>
<pinref part="J13" gate="G$1" pin="14"/>
<wire x1="281.94" y1="157.48" x2="269.24" y2="157.48" width="0.1524" layer="91"/>
<junction x="281.94" y="157.48"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="J13" gate="G$1" pin="15"/>
<pinref part="J15" gate="G$1" pin="15"/>
<wire x1="269.24" y1="154.94" x2="281.94" y2="154.94" width="0.1524" layer="91"/>
<pinref part="J16" gate="G$1" pin="15"/>
<wire x1="281.94" y1="154.94" x2="294.64" y2="154.94" width="0.1524" layer="91"/>
<junction x="281.94" y="154.94"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="J16" gate="G$1" pin="16"/>
<pinref part="J15" gate="G$1" pin="16"/>
<wire x1="294.64" y1="152.4" x2="281.94" y2="152.4" width="0.1524" layer="91"/>
<pinref part="J13" gate="G$1" pin="16"/>
<wire x1="281.94" y1="152.4" x2="269.24" y2="152.4" width="0.1524" layer="91"/>
<junction x="281.94" y="152.4"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="J13" gate="G$1" pin="17"/>
<pinref part="J15" gate="G$1" pin="17"/>
<wire x1="269.24" y1="149.86" x2="281.94" y2="149.86" width="0.1524" layer="91"/>
<pinref part="J16" gate="G$1" pin="17"/>
<wire x1="281.94" y1="149.86" x2="294.64" y2="149.86" width="0.1524" layer="91"/>
<junction x="281.94" y="149.86"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="J16" gate="G$1" pin="18"/>
<pinref part="J15" gate="G$1" pin="18"/>
<wire x1="294.64" y1="147.32" x2="281.94" y2="147.32" width="0.1524" layer="91"/>
<pinref part="J13" gate="G$1" pin="18"/>
<wire x1="269.24" y1="147.32" x2="281.94" y2="147.32" width="0.1524" layer="91"/>
<junction x="281.94" y="147.32"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="J13" gate="G$1" pin="19"/>
<pinref part="J15" gate="G$1" pin="19"/>
<wire x1="269.24" y1="144.78" x2="281.94" y2="144.78" width="0.1524" layer="91"/>
<pinref part="J16" gate="G$1" pin="19"/>
<wire x1="281.94" y1="144.78" x2="294.64" y2="144.78" width="0.1524" layer="91"/>
<junction x="281.94" y="144.78"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="J16" gate="G$1" pin="20"/>
<pinref part="J15" gate="G$1" pin="20"/>
<wire x1="294.64" y1="142.24" x2="281.94" y2="142.24" width="0.1524" layer="91"/>
<pinref part="J13" gate="G$1" pin="20"/>
<wire x1="281.94" y1="142.24" x2="269.24" y2="142.24" width="0.1524" layer="91"/>
<junction x="281.94" y="142.24"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="J13" gate="G$1" pin="21"/>
<pinref part="J15" gate="G$1" pin="21"/>
<wire x1="269.24" y1="139.7" x2="281.94" y2="139.7" width="0.1524" layer="91"/>
<pinref part="J16" gate="G$1" pin="21"/>
<wire x1="281.94" y1="139.7" x2="294.64" y2="139.7" width="0.1524" layer="91"/>
<junction x="281.94" y="139.7"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="J16" gate="G$1" pin="22"/>
<pinref part="J15" gate="G$1" pin="22"/>
<wire x1="294.64" y1="137.16" x2="281.94" y2="137.16" width="0.1524" layer="91"/>
<pinref part="J13" gate="G$1" pin="22"/>
<wire x1="281.94" y1="137.16" x2="269.24" y2="137.16" width="0.1524" layer="91"/>
<junction x="281.94" y="137.16"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="J13" gate="G$1" pin="23"/>
<pinref part="J15" gate="G$1" pin="23"/>
<wire x1="269.24" y1="134.62" x2="281.94" y2="134.62" width="0.1524" layer="91"/>
<pinref part="J16" gate="G$1" pin="23"/>
<wire x1="281.94" y1="134.62" x2="294.64" y2="134.62" width="0.1524" layer="91"/>
<junction x="281.94" y="134.62"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="J16" gate="G$1" pin="24"/>
<pinref part="J15" gate="G$1" pin="24"/>
<wire x1="294.64" y1="132.08" x2="281.94" y2="132.08" width="0.1524" layer="91"/>
<pinref part="J13" gate="G$1" pin="24"/>
<wire x1="281.94" y1="132.08" x2="269.24" y2="132.08" width="0.1524" layer="91"/>
<junction x="281.94" y="132.08"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="J13" gate="G$1" pin="25"/>
<pinref part="J15" gate="G$1" pin="25"/>
<wire x1="269.24" y1="129.54" x2="281.94" y2="129.54" width="0.1524" layer="91"/>
<pinref part="J16" gate="G$1" pin="25"/>
<wire x1="281.94" y1="129.54" x2="294.64" y2="129.54" width="0.1524" layer="91"/>
<junction x="281.94" y="129.54"/>
</segment>
</net>
<net name="PA16" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA16/SER1-0/SER3-1/TC2-0"/>
<wire x1="111.76" y1="147.32" x2="132.08" y2="147.32" width="0.1524" layer="91"/>
<label x="114.3" y="147.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J12" gate="G$1" pin="7"/>
<wire x1="256.54" y1="175.26" x2="238.76" y2="175.26" width="0.1524" layer="91"/>
<label x="238.76" y="175.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA17" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA17/SER1-1/SER3-0/TC2-1"/>
<wire x1="111.76" y1="144.78" x2="132.08" y2="144.78" width="0.1524" layer="91"/>
<label x="114.3" y="144.78" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J12" gate="G$1" pin="6"/>
<wire x1="256.54" y1="177.8" x2="238.76" y2="177.8" width="0.1524" layer="91"/>
<label x="238.76" y="177.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA18" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA18/SER1-2/SER3-2/TC3-0"/>
<wire x1="111.76" y1="142.24" x2="132.08" y2="142.24" width="0.1524" layer="91"/>
<label x="114.3" y="142.24" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J12" gate="G$1" pin="5"/>
<wire x1="256.54" y1="180.34" x2="238.76" y2="180.34" width="0.1524" layer="91"/>
<label x="238.76" y="180.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA19" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA19/SER1-3/SER3-3/TC3-1"/>
<wire x1="111.76" y1="139.7" x2="132.08" y2="139.7" width="0.1524" layer="91"/>
<label x="114.3" y="139.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J12" gate="G$1" pin="4"/>
<wire x1="256.54" y1="182.88" x2="238.76" y2="182.88" width="0.1524" layer="91"/>
<label x="238.76" y="182.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA24-PD0-DM" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA24/SER3-2/SER5-2/TC5-0/PDEC0-0/USBDM"/>
<wire x1="111.76" y1="127" x2="132.08" y2="127" width="0.1524" layer="91"/>
<label x="114.3" y="127" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J12" gate="G$1" pin="9"/>
<wire x1="256.54" y1="170.18" x2="238.76" y2="170.18" width="0.1524" layer="91"/>
<label x="238.76" y="170.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA25-PD1-DP" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA25/SER3-3/SER5-3/TC5-1/PDEC0-1/USBDP"/>
<wire x1="111.76" y1="124.46" x2="132.08" y2="124.46" width="0.1524" layer="91"/>
<label x="114.3" y="124.46" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J12" gate="G$1" pin="8"/>
<wire x1="256.54" y1="172.72" x2="238.76" y2="172.72" width="0.1524" layer="91"/>
<label x="238.76" y="172.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="PB16" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PB16/SER5-0/TC6-0"/>
<wire x1="111.76" y1="68.58" x2="124.46" y2="68.58" width="0.1524" layer="91"/>
<label x="114.3" y="68.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J12" gate="G$1" pin="10"/>
<wire x1="256.54" y1="167.64" x2="238.76" y2="167.64" width="0.1524" layer="91"/>
<label x="238.76" y="167.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="PB17" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PB17/SER5-1/TC6-1"/>
<wire x1="111.76" y1="66.04" x2="124.46" y2="66.04" width="0.1524" layer="91"/>
<label x="114.3" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J12" gate="G$1" pin="11"/>
<wire x1="256.54" y1="165.1" x2="238.76" y2="165.1" width="0.1524" layer="91"/>
<label x="238.76" y="165.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA10" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA10/ADC0-10/SER0-2/SER2-2/TC1-0/TCC0-2"/>
<wire x1="111.76" y1="162.56" x2="132.08" y2="162.56" width="0.1524" layer="91"/>
<label x="114.3" y="162.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J12" gate="G$1" pin="19"/>
<wire x1="256.54" y1="144.78" x2="238.76" y2="144.78" width="0.1524" layer="91"/>
<label x="238.76" y="144.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA11" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA11/ADC0-11/SER0-3/SER2-3/TC1-1/TCC0-3"/>
<wire x1="111.76" y1="160.02" x2="132.08" y2="160.02" width="0.1524" layer="91"/>
<label x="114.3" y="160.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J12" gate="G$1" pin="18"/>
<wire x1="256.54" y1="147.32" x2="238.76" y2="147.32" width="0.1524" layer="91"/>
<label x="238.76" y="147.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA08" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA08/ADC0-8/ADC1-2/SER0-0/SER2-1/TC0-0/TCC0-0"/>
<wire x1="111.76" y1="167.64" x2="132.08" y2="167.64" width="0.1524" layer="91"/>
<label x="114.3" y="167.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J12" gate="G$1" pin="21"/>
<wire x1="256.54" y1="139.7" x2="238.76" y2="139.7" width="0.1524" layer="91"/>
<label x="238.76" y="139.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA07" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA07/ADC0-7/SER0-3/TC1-1"/>
<wire x1="111.76" y1="170.18" x2="132.08" y2="170.18" width="0.1524" layer="91"/>
<label x="114.3" y="170.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J12" gate="G$1" pin="22"/>
<wire x1="256.54" y1="137.16" x2="238.76" y2="137.16" width="0.1524" layer="91"/>
<label x="238.76" y="137.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA06" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA06/ANAREF-VREFC/ADC0-6/SER0-2/TC1-0"/>
<wire x1="111.76" y1="172.72" x2="132.08" y2="172.72" width="0.1524" layer="91"/>
<label x="114.3" y="172.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J12" gate="G$1" pin="23"/>
<wire x1="256.54" y1="134.62" x2="238.76" y2="134.62" width="0.1524" layer="91"/>
<label x="238.76" y="134.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA09" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA09/ADC0-9/ADC1-3/SER0-1/SER2-0/TC0-1/TCC0-1"/>
<wire x1="111.76" y1="165.1" x2="132.08" y2="165.1" width="0.1524" layer="91"/>
<label x="114.3" y="165.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J12" gate="G$1" pin="20"/>
<wire x1="256.54" y1="142.24" x2="238.76" y2="142.24" width="0.1524" layer="91"/>
<label x="238.76" y="142.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA05" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA05/ADC0-5/DAC-1/SER0-1/TC0-1"/>
<wire x1="111.76" y1="175.26" x2="132.08" y2="175.26" width="0.1524" layer="91"/>
<label x="114.3" y="175.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J12" gate="G$1" pin="24"/>
<wire x1="256.54" y1="132.08" x2="238.76" y2="132.08" width="0.1524" layer="91"/>
<label x="238.76" y="132.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA04" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA04/ANAREF-VREFB/ADC0-4/SER0-0/TC0-0"/>
<wire x1="111.76" y1="177.8" x2="132.08" y2="177.8" width="0.1524" layer="91"/>
<label x="114.3" y="177.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J12" gate="G$1" pin="25"/>
<wire x1="256.54" y1="129.54" x2="238.76" y2="129.54" width="0.1524" layer="91"/>
<label x="238.76" y="129.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA15" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA15/XOUT0/SER2-3/SER4-3/TC3-1"/>
<wire x1="111.76" y1="149.86" x2="132.08" y2="149.86" width="0.1524" layer="91"/>
<label x="114.3" y="149.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J12" gate="G$1" pin="12"/>
<wire x1="256.54" y1="162.56" x2="238.76" y2="162.56" width="0.1524" layer="91"/>
<label x="238.76" y="162.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA14" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA14/XIN0/SER2-2/SER4-2/TC3-0"/>
<wire x1="111.76" y1="152.4" x2="132.08" y2="152.4" width="0.1524" layer="91"/>
<label x="114.3" y="152.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J12" gate="G$1" pin="13"/>
<wire x1="256.54" y1="160.02" x2="238.76" y2="160.02" width="0.1524" layer="91"/>
<label x="238.76" y="160.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA13" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA13/SER2-1/SER4-0/TC2-1/TCC0-7"/>
<wire x1="111.76" y1="154.94" x2="132.08" y2="154.94" width="0.1524" layer="91"/>
<label x="114.3" y="154.94" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J12" gate="G$1" pin="14"/>
<wire x1="256.54" y1="157.48" x2="238.76" y2="157.48" width="0.1524" layer="91"/>
<label x="238.76" y="157.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="PA12" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA12/SER2-0/SER4-1/TC2-0/TCC0-6"/>
<wire x1="111.76" y1="157.48" x2="132.08" y2="157.48" width="0.1524" layer="91"/>
<label x="114.3" y="157.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J12" gate="G$1" pin="15"/>
<wire x1="256.54" y1="154.94" x2="238.76" y2="154.94" width="0.1524" layer="91"/>
<label x="238.76" y="154.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="PB08" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PB08/ADC0-2/ADC1-9/SER4-0/TC4-0"/>
<wire x1="111.76" y1="88.9" x2="124.46" y2="88.9" width="0.1524" layer="91"/>
<label x="114.3" y="88.9" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J12" gate="G$1" pin="16"/>
<wire x1="256.54" y1="152.4" x2="238.76" y2="152.4" width="0.1524" layer="91"/>
<label x="238.76" y="152.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="PB09" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PB09/ADC0-3/ADC1-1/SER4-1/TC4-1"/>
<wire x1="111.76" y1="86.36" x2="124.46" y2="86.36" width="0.1524" layer="91"/>
<label x="114.3" y="86.36" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J12" gate="G$1" pin="17"/>
<wire x1="256.54" y1="149.86" x2="238.76" y2="149.86" width="0.1524" layer="91"/>
<label x="238.76" y="149.86" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
