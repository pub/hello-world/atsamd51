# SAMD51J19A Arduino-Compatible Bootloader

**fair warning**
>Use of the arduino bootloader *does* assume that your circuit has a USB port available, as well as a 32.678kHz clock on the RTC Xout / Xin pins. During cycles, the bootloader (assuming it is on a Feather M4) will toggle 'D13' (PA23) as well as send Neopixel Data to (?).


## Setup

To get started, make sure you have a bootloader binary (probably a `.bin`, `.elf`, or `.hex` file).
There's one in this directory, but you can always download the latest build of the [Adafruit
bootloader](https://github.com/adafruit/uf2-samdx1/releases) instead. The Adafruit Feather M4 uses
the SAMD51J19A, so grab that one for the current generation squidworks module. If you want to get to
know your bootloader very well, click
[here](https://learn.adafruit.com/adafruit-feather-m4-express-atsamd51/uf2-bootloader-details). (In
particular, note that it's 16k.)

We need to write this file into the micro's memory. I have been using the `Serial Wire Debug`
interface, which is enabled by default on the D51 hardware. `SWCLK` is on `PA30` and `SWDIO` is on
`PA31`. JTAG is also available, but requires an enable pin pulled low somewhere *I think*. An Atmel
ICE debugger should be able to handle either.

From here on out there are two options for burning the bootloader to your board (documented here
anyway): OpenOCD, and Atmel Studio.


## OpenOCD

To get set up with OpenOCD, see the docs
[here](https://gitlab.cba.mit.edu/pub/hello-world/tools/tree/master/openocd).

Once that's done, you should just need to navigate to this directory and run `openocd`. The
configuration file here has all the relevant commands inside it. If all goes well you'll see
something like this, and then `openocd` will immediately exit.

```
erik@strandbeest:~/Code/atsamd51/bootloader$ openocd
Open On-Chip Debugger 0.10.0+dev-00973-g80f1a92b (2019-12-01-17:19)
Licensed under GNU GPL v2
For bug reports, read
	http://openocd.org/doc/doxygen/bugs.html
Info : auto-selecting first available session transport "swd". To override use 'transport select <transport>'.
Info : CMSIS-DAP: SWD  Supported
Info : CMSIS-DAP: JTAG Supported
Info : CMSIS-DAP: FW Version = 1.0
Info : CMSIS-DAP: Serial# = J41800095841
Info : CMSIS-DAP: Interface Initialised (SWD)
Info : SWCLK/TCK = 1 SWDIO/TMS = 1 TDI = 1 TDO = 1 nTRST = 0 nRESET = 1
Info : CMSIS-DAP: Interface ready
Info : clock speed 2000 kHz
Info : SWD DPIDR 0x2ba01477
Info : at91samd51j19.cpu: hardware has 6 breakpoints, 4 watchpoints
Info : Listening on port 3333 for gdb connections
target halted due to debug-request, current mode: Thread
xPSR: 0x01000000 pc: 0x0000033c msp: 0x2000c020
target halted due to debug-request, current mode: Thread
xPSR: 0x01000000 pc: 0x0000033c msp: 0x2000c020
** Programming Started **
Info : SAM MCU: SAMD51J19A (512KB Flash, 192KB RAM)
** Programming Finished **
** Verify Started **
** Verified OK **
shutdown command invoked
Info : Listening on port 6666 for tcl connections
Info : Listening on port 4444 for telnet connections
```

Note that once the bootloader is on your micro, you can't program it as discussed in
[blink-openocd](../blink-openocd). (It's harmless to try it; you'll just get an error.) This happens
because the bootloader is protected. To disable this protection, run `openocd -f
unprotect_bootloader.cfg`. Then the next time you program it, the bootloader will be overwritten.


## Atmel Studio

To speak SWD, I just use the Atmel-Ice programmer, and Atmel Studio 7 (a windows application, sorry). Any programmer that speaks SWD should be capable of doing this. In Atmel Studio, go to `Tools -> Device Programming`, and select the Atmel-Ice Tool, the ATSAMD51xxxx device you'd like to program (the bootloader is written for the ATSAMD51J19A), and the SWD interface. You should be able to read the device signature. If this works, your SWD connection is all super-gucci (as they say). You can navigate to `Memories` - and write that bootloader in. OK. The device should now enumerate over USB as an Adafruit Feather M4 / FeatherBoot (or something similar).

If you're interested, you can check out Adafruit's documentation of this process
[here](https://learn.adafruit.com/how-to-program-samd-bootloaders/programming-the-bootloader-with-atmel-studio).
