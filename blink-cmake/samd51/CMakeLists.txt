#---------------------------------------------------------------------------------------------------
# This file packages ASF code into a library.

# By default, cmake would make a static library and package the object files into an archive. This
# doesn't work since the linker then throws away the startup code, since it's not called by
# anything. (It actually calls main.) So instead we use an object library, which tells cmake to
# leave the contents as bare object files and directly compile them into any target that needs them.
add_library(asf OBJECT
    startup/startup_samd51.c
    startup/system_samd51.c
)

# Pull in the shared compiler flags from the toolchain file. (Aren't interface libraries nice?)
target_link_libraries(asf shared_settings)

# Every target that links against asf needs to know where the ASF headers are.
target_include_directories(asf PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}/CMSIS/Include
    ${CMAKE_CURRENT_SOURCE_DIR}/include
)

# Use the custom linker script provided with ASF.
target_link_options(asf PUBLIC
    -T${CMAKE_CURRENT_SOURCE_DIR}/startup/samd51j19a_flash.ld
    --specs=nano.specs
    -Wl,--gc-sections
)
