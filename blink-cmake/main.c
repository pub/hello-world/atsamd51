#include "sam.h"

int main (void) {
    int i;

    // on a squidworks module board, the red LED is 17, and the green is 19
    REG_PORT_DIR0 = (1u << 17) | (1u << 19);

    // setting these high turns the LEDs off
    REG_PORT_OUTSET0 = (1u << 17) | (1u << 19);

    while(1) {
        REG_PORT_OUTSET0 = (1u << 17) | (1u << 19);
        for (i=0; i<1000000; i++) {
            __asm("nop");
        }

        REG_PORT_OUTCLR0 = (1u << 17) | (1u << 19);
        for (i=0; i<1000000; i++) {
            __asm("nop");
        }
    }
}
