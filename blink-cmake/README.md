# Building with CMake

This directory contains a `cmake` project for SAMD51, using `arm-none-eabi-gcc`.

To compile, first make sure you have `cmake` version 3.13 or newer installed. You may need to
compile from source if your package manager is out of date.

Next, create a build directory: `mkdir build`. All build artifacts will go here, so you won't end up
with a bunch of `.o` or `.elf` files hanging out next to your source code.

Tell `cmake` to configure a build environment in your new directory: `cd build` then `cmake ..`.
This tells `cmake` to generate a `Makefile` for you.

From here on out you can just use the `Makefile` like normal: `make` builds everything, or `make
<target>` builds a particular thing. The `Makefile` that `cmake` generates is even smart enough to
run the configuration step again automatically if it detects that you've modified any `cmake` config
files.

Note that I haven't yet added targets that flash your board. The executable built by `cmake` is
`build/hello_world`; for now you need to load this manually.
