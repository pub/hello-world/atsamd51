#---------------------------------------------------------------------------------------------------
# This file tells cmake how to build for samd51 with arm-none-eabi-gcc.

# These commands tell cmake that we're cross compiling to ARM with arm-none-eabi.
set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_PROCESSOR arm)
set(CMAKE_CROSSCOMPILING 1)
set(CMAKE_C_COMPILER "arm-none-eabi-gcc")
set(CMAKE_CXX_COMPILER "arm-none-eabi-g++")
set(CMAKE_AR "arm-none-eabi-ar")
set(CMAKE_RANLIB "arm-none-eabi-ranlib")

# Clear default compiler and linker flags.
set(CMAKE_C_FLAGS "")
set(CMAKE_C_LINK_FLAGS "")

# This should eventually live somewhere else, since we'll be using J19s and J20s.
set(MCU_DEFINE "__SAMD51J19A__")

# Search for programs in the build host directories.
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# Search for libraries and headers in the target directories.
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)


# This creates an interface library that holds common compiler and link flags. An interface library
# is one that has no actual targets to build; it's a virtual library that we can use to manage
# dependencies.
add_library(shared_settings INTERFACE)

# These flags are derived from Adafruit projects. They might not all be necessary.
list(APPEND common_flags
    -mthumb
    -mabi=aapcs-linux
    -mcpu=cortex-m4
    -mfpu=fpv4-sp-d16
    -mfloat-abi=softfp
    -mlong-calls
    -DSAMD51
)

target_compile_options(shared_settings INTERFACE
    ${common_flags}
    -D${MCU_DEFINE}
    -ffunction-sections
    -Wall
)
target_link_options(shared_settings INTERFACE ${common_flags})
